IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspCheckCardDocumentNumber' AND type = 'P')
    DROP PROCEDURE dbo.uspCheckCardDocumentNumber
GO
Create Procedure dbo.uspCheckCardDocumentNumber
		@DocumentNumber bigint,
		@AccountNumber bigint output,
		@AccountName varchar(80) output,
		@Status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
	select @Status = 0
	select @ErrorMessage = ''
	select @AccountName = ''
	select @AccountNumber = 0
	select @AccountNumber = AccountNumber, @AccountName = SPAPPSPROD.dbo.udfFormatAccountName(AccountNumber, 'ALL')  from SPDSPROD.dbo.T01_TransactionMaster 
     where Documentnumber = @DocumentNumber
	if (@AccountName is null) select @AccountName = ''
	if (@AccountNumber is null) select @AccountNumber = 0
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspCardTypeOfCard, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH
