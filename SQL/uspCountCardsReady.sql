use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspCountCardsReady' AND type = 'P')
    DROP PROCEDURE dbo.uspCountCardsReady
GO
--------------------------------------------
-- Replaced with version 2
--------------------------------------------
Create Procedure dbo.uspCountCardsReady
		@iCount int output,
		@status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
select @status = 0
select @ErrorMessage = ''
select @iCount = count(*) from SPAPPSDEV.dbo.HonorMemorialCards1 where Status = 'Y' and AccountNumber > 0 and 
CardDocumentNumber in (select CardDocumentNumber from SPAPPSDEV.dbo.HonorMemorialCards2 where CardDocumentNumber = CardDocumentNumber)
if (@iCount is null) select @iCount = 0
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspCountCardsReady, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH
