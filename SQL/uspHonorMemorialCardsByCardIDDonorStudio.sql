use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspHonorMemorialCardsByCardIDDonorStudio' AND type = 'P')
    DROP PROCEDURE dbo.uspHonorMemorialCardsByCardIDDonorStudio
GO
Create Procedure dbo.uspHonorMemorialCardsByCardIDDonorStudio
		@CardID bigint,
		@Status int output,
		@ErrorMessage varchar(3000) output
as
set nocount on
BEGIN TRY
declare @SQL varchar(2048)
declare @Fields varchar(2048)
select @SQL = ''
select @status = 0
select @ErrorMessage = ''
--
-- Make sure all is loaded.
--
--exec dbo.uspLoadHonorMemorialCards	@status output,	@ErrorMessage output
--if (@status <> 0) return
--
--
-- Fields
--
select @Fields = 
	'SP02.RecordId as CardDocumentNumber, ' +
	'SP02.AccountNumber, ' +
	'SPAPPSPROD.[dbo].[udfFormatAccountName] (SP02.AccountNumber, ''ALL'') as AccountName, ' + 
	'isnull(SP02.DocumentNumber, 0) as DocumentNumber, ' +
	'rtrim(isnull(SP02.FromName, '''')) as FromName, ' +
	'rtrim(isnull(SP02.Memorial, '''')) as MemorialName, ' +
	'convert(varchar(20), T01.Date, 101) as TransactionDate, ' +
	'SP02.CardType as LetterCode , ' +
	'case when SP02.PrintStatus=''X'' then ''Delete'' 
	      when SP02.PrintStatus = ''D'' then ''Donor Ministry'' 
	      when SP02.PrintStatus = ''I'' then ''Entry In Progress''
	      when SP02.PrintStatus = ''H'' then ''Hold''  
	      when SP02.PrintStatus =''N'' then ''No Print'' 
	      when SP02.PrintStatus = ''Y'' then ''Ready'' 
	      when SP02.PrintStatus = ''P'' then ''Printed'' 
		  when SP02.PrintStatus = ''T'' then ''Test'' 
	      else SP02.PrintStatus end as status, ' +
	'PrintGroup, ' +
	'rtrim(isnull(SP02.ToName, '''')) as ToName, ' +
	'rtrim(isnull(SP02.Address1, '''')) as ToAddress1, ' +
	'rtrim(isnull(SP02.Address2, '''')) as ToAddress2, ' +
	'rtrim(isnull(SP02.Address3, '''')) as ToAddress3, ' +
	'rtrim(isnull(SP02.City, '''')) as ToCity, ' +
	'rtrim(isnull(SP02.State, '''')) as ToState, ' +
	'rtrim(isnull(SP02.ZipPostal, '''')) as ToZip, ' +
	'rtrim(isnull(SP02.Country, '''')) as ToCountry, ' +
	'0 as NumberOfGifts, ' +
	'''DonorStudio'' as WebPageName, ' +
	'dbo.usfGetCardDate(SP02.RecordId, ''C'', ''A'') as AddDate, ' +
	'dbo.usfGetCardUser(SP02.RecordId, ''C'', ''C'') as ChangeUser, ' +
	'dbo.usfGetCardDate(SP02.RecordId, ''C'', ''C'') as ChangeDate, ' +
	'case when ReturnToSender = 1  then ''Yes'' when ReturnToSender = 0 then ''No'' else ''No'' end as ReturnToSender, ' +
    'dbo.usfGetCardGiftsDonorStudio(SP02.RecordID) as Gifts, ' +
    'dbo.usfGetCardUser(SP02.RecordId, ''C'', ''A'') as AddUser, ' +
	''''' as NewStatus '
--
-- Main query
--
set @SQL = 'select ' + @Fields + ' from SPDSPROD.dbo.SP02_CardMaster SP02, SPDSPROD.dbo.T01_TransactionMaster T01 where '
set @SQL = @SQL + ' SP02.documentnumber = T01.documentNumber '
--
-- Card ID
--
if (@CardID > 0)
	set @SQL = @SQL + 'and SP02.RecordID = ' + rtrim(ltrim(convert(varchar(15), @CardID)))
--
-- Order
--
select @SQL = @SQL + 'order by T01.Date, SP02.AccountNumber, SP02.RecordID '
--select @SQL
exec (@SQL)
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspHonorMemorialCardsByCardIDDonorStudio, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) + ' ' + @SQL
	
END CATCH

-- select * from SPAPPSPROD.dbo.HonorMemorialCards1 
