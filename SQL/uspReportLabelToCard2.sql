USE SPAPPSPROD
GO

IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspReportLabelToCard2' AND type = 'P')
    DROP PROCEDURE dbo.uspReportLabelToCard2
GO
Create Procedure dbo.uspReportLabelToCard2
		@PrintGroup int
as
set nocount on
--BEGIN TRY
--
--select distinct 0 as 'RowNumber', AccountNumber, [Type],
--		[FileName],   dbo.udfFormatAccountName(AccountNumber, 'ALL')  as SenderName, + replace(dbo.udfFormatAccountPrimeAddr(AccountNumber, 'ALL'), 'USA, USA,', 'USA') as SenderAddress, 
--	   'From ' + rtrim(FromName) + ' to ' + rtrim(ToName) as Receiver, 0 as RowColor
--	   from dbo.HonorCardFormatting where PrintGroup = @PrintGroup and [Type] in (3, 4, 7, 8, 11, 12, 15, 16)
--		order by [Type], accountnumber
--
--END TRY
--BEGIN CATCH
--	select 0 as AccountNumber, 0 as Type, '' as FileName, 'Error' as Sender, 'Error' as Receiver
--
--END CATCH
BEGIN TRY
select distinct ROW_NUMBER() OVER(ORDER BY [Type], accountnumber) AS 'RowNumber', AccountNumber, [Type], SPACE(80) AS Folder,
		[FileName],   dbo.udfFormatAccountName(AccountNumber, 'ALL')  as SenderName, + replace(replace(dbo.udfFormatAccountPrimeAddr(AccountNumber, 'ALL'), ', United States of America', ''), ', USA,', '') as SenderAddress, 
	   'From ' + rtrim(FromName) + ' to ' + rtrim(ToName) as Receiver, 0 as RowColor
	   into #a from dbo.HonorCardFormatting where PrintGroup = @PrintGroup and [Type] in (3, 4, 7, 8, 11, 12, 15, 16,
																					   103, 104, 107, 108, 111, 112, 115, 116,
																					   203, 204, 207, 208, 211, 212, 215, 216,
																					   303, 304, 307, 308, 311, 312, 315, 316)
		order by [Type], accountnumber

update #a set FileName = rtrim(ltrim(FileName))
UPDATE #a SET Folder = '000 - Catalog Cards' WHERE type between 0 AND 99
UPDATE #a SET Folder = '100 - Acknowledgement Cards' WHERE type between 100 AND 199
UPDATE #a SET Folder = '200 - OHOP Cards' WHERE type between 200 AND 299
UPDATE #a SET Folder = '300 - Mother''s Day Cards' WHERE type between 300 AND 399

declare @RowNumber int
declare @AccountNumber int
declare @RowColor int
declare @LastAccountNumber int
select @LastAccountNumber = 0
select @RowColor = 1
DECLARE a_cursor CURSOR FAST_FORWARD FOR
select RowNumber, AccountNumber from #a order by rownumber
OPEN a_cursor
WHILE (1=1)
BEGIN
	FETCH NEXT FROM a_cursor into @RowNumber, @AccountNumber
	if (@@FETCH_STATUS != 0) break
	if (@AccountNumber != @LastAccountNumber)
	BEGIN
		if (@RowColor = 1) 
			select @RowColor = 0
		else 
			select @RowColor = 1
	END
	update #a set RowColor = @RowColor where RowNumber = @RowNumber
	select @LastAccountNumber = @AccountNumber
END
CLOSE a_cursor
DEALLOCATE a_cursor
select * from #a order by rownumber
END TRY
BEGIN CATCH
	select 0 as RowNumber, 0 as AccountNumber, 0 as Type, '' AS folder,  '' as FileName, 'Error' as Sender, 'Error' as Receiver, 0 as RowColor

END CATCH


