USE [SPDSPROD]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  trigger [dbo].[SP02_CardMaster_insert]
	on [dbo].[SP02_CardMaster] for insert
	as
      Begin
		begin try
			DECLARE @recordID bigint
			DECLARE insert_cursor CURSOR FAST_FORWARD FOR
			select RecordID from inserted
			OPEN insert_cursor
			WHILE (1 = 1) 
			BEGIN
				FETCH NEXT FROM insert_cursor into @recordID
				if (@@FETCH_STATUS != 0) break
				exec SPAPPSPROD.dbo.uspHonorCardInSyncQueue @recordID, 0
			END
			CLOSE insert_cursor
			DEALLOCATE insert_cursor
		End try
		begin catch
			declare @i int
			select @i = 1
		end catch
		set NOCOUNT OFF
end
