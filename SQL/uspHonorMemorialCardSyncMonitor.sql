use SPAPPSTRAIN
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspHonorMemorialCardSyncMonitor' AND type = 'P')
    DROP PROCEDURE uspHonorMemorialCardSyncMonitor
GO
CREATE Procedure uspHonorMemorialCardSyncMonitor
as
set nocount on
while datepart(hh, getdate()) between 5 and 23
	BEGIN
	--
	-- Sync committed and backorder.
	--
	while exists (select CardID from SPAPPSTRAIN.dbo.HonorMemorialSyncFromDS with (nolock) where CardID = 0)
	BEGIN
		exec SPAPPSTRAIN.dbo.uspHonorCardSync
		--
		-- Resync if there was a mishap
		--
		update SPAPPSTRAIN.dbo.HonorMemorialSyncFromDS set cardid = 0 where cardid not in
		(select carddocumentnumber from  SPAPPSTRAIN.dbo.HonorMemorialCards1 where carddocumentnumber = cardid)

		update dbo.HonorMemorialSyncFromDS set cardid = 0 from dbo.HonorMemorialSyncFromDS DS, SPDSTRAIN.dbo.SP01_CardDetails SP01
		where
		DS.CardItemRecordID = SP01.RecordID and
		cardid not in
		(select carddocumentnumber from  dbo.HonorMemorialCards2 HM where HM.carddocumentnumber = DS.cardid and
		SP01.ProjectCode = HM.Project)

		WAITFOR DELAY '00:00:01'
	END
	--
	-- Wait a few.
	--
	WAITFOR DELAY '00:00:05'
	print 'again...'
END
