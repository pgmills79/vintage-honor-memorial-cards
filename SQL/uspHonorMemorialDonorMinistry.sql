use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspHonorMemorialDonorMinistry' AND type = 'P')
    DROP PROCEDURE dbo.uspHonorMemorialDonorMinistry
GO
Create Procedure dbo.uspHonorMemorialDonorMinistry
		@BeginDate datetime,
	    @EndDate datetime
as
set nocount on
BEGIN TRY
declare @db as datetime
declare @de as datetime
declare @iCount as int

select @db = dbo.udfMinDateTime(@BeginDate)
select @de = dbo.udfMaxDateTime(@EndDate)
--
-- Get honor card information for donor ministry.
--
select	C1.CardDocumentNumber, 
		AccountNumber, 
		rtrim(ltrim(FromName)) as FromName, 
		rtrim(ltrim(MemorialName)) as MemorialName, 
		TransactionDate, 
		rtrim(ltrim(LetterCode)) as LetterCode, 
		rtrim(ltrim(ToName)) as ToAddress1, 
		rtrim(ltrim(ToAddress1)) as ToAddress2, 
		rtrim(ltrim(ToAddress2)) as ToAddress3, 
		rtrim(ltrim(ToAddress3)) as ToAddress4, 
		ltrim(rtrim(ToCity)) + ' ' +  ltrim(rtrim(ToState)) + ' ' + ltrim(rtrim(ToZip))  as ToAddress5, 
case
when ToCountry='USA' then ''
else rtrim(ltrim(ToCountry))
end as ToAddress6, rtrim(ltrim(Project)) as Project, space(80) as ProjectDescription into #a from SPAPPSPROD.dbo.HonorMemorialCards1 C1, SPAPPSPROD.dbo.HonorMemorialCards2 C2 where
C1.CardDocumentNumber = C2.CardDocumentNumber and
C1.Status = 'D' and
TransactionDate between @db and @de
--
-- Null thing, you make my heart sing.
--
update #a set ToAddress1 = '' where ToAddress1 is null
update #a set ToAddress2 = '' where ToAddress2 is null
update #a set ToAddress3 = '' where ToAddress3 is null
update #a set ToAddress4 = '' where ToAddress4 is null
update #a set ToAddress5 = '' where ToAddress5 is null
update #a set ToAddress6 = '' where ToAddress6 is null
update #a set FromName = '' where FromName is null
update #a set MemorialName = '' where MemorialName is null
update #a set LetterCode = '' where LetterCode is null
update #a set Project = '' where Project is null
-- 
-- Shift address to top.
--
select @iCount = 6
While (@iCount > 0)
BEGIN
	update #a set ToAddress1 = ToAddress2, 
                  ToAddress2 = ToAddress3,
				  ToAddress3 = ToAddress4,
				  ToAddress4 = ToAddress5,
				  ToAddress5 = ToAddress6,
				  ToAddress6 = '' 
		where ToAddress1 = ''
	update #a set ToAddress2 = ToAddress3,
				  ToAddress3 = ToAddress4,
				  ToAddress4 = ToAddress5,
				  ToAddress5 = ToAddress6,
				  ToAddress6 = '' 
		where ToAddress2 = ''	
	update #a set ToAddress3 = ToAddress4,
				  ToAddress4 = ToAddress5,
				  ToAddress5 = ToAddress6,
				  ToAddress6 = '' 
		where ToAddress3 = ''
	update #a set ToAddress4 = ToAddress5,
				  ToAddress5 = ToAddress6,
				  ToAddress6 = '' 
		where ToAddress4 = ''
	update #a set ToAddress5 = ToAddress6,
				  ToAddress6 = '' 
		where ToAddress5 = ''

	select @iCount = @iCount - 1
END
--
-- Add int product description.
--
update #a set projectdescription = ''
update #a set Projectdescription = Y01.Description from #a a, SPDSPROD.dbo.Y01_ProjectMaster Y01 where
a.project = Y01.projectcode
--
-- Return
--
select * from #a order by transactiondate, cardDocumentNumber, project
drop table #a
END TRY
BEGIN CATCH
		select substring('Error in  = ' + ERROR_PROCEDURE() + ', procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255)
END CATCH
