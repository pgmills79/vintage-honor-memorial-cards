IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspGetGiftsOfCard' AND type = 'P')
    DROP PROCEDURE dbo.uspGetGiftsOfCard
GO
Create Procedure dbo.uspGetGiftsOfCard
		@CardID int,
		@Status int output,
		@ErrorMessage varchar(3000) output
as
set nocount on
BEGIN TRY
	select @status = 0
	select @ErrorMessage = 0
	select project from HonorMemorialCards2 where carddocumentnumber = @CardID order by project
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspGetGiftsOfCard, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH

-- select * from SPAPPSPROD.dbo.HonorMemorialCards1 
