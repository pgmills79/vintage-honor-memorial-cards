IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspReassignLetterCodePriority' AND type = 'P')
    DROP PROCEDURE dbo.uspReassignLetterCodePriority
GO
Create Procedure dbo.uspReassignLetterCodePriority
as
BEGIN TRY
set nocount on
INSERT INTO [SPAPPSDEV].[dbo].[ReassignedLetterCodePriority]
           ([ResponseID]
           ,[T12RecordID]
           ,[DocumentNumber]
           ,[AccountNumber]
           ,[OldLetterCode]
           ,[ReassignedLetterCode]
           ,[Processed])
select distinct T12.ResponseID,
		   T12.RecordID,
           T01.DocumentNumber,
           T01.AccountNumber,
           T07.LetterCode,
           '',
           0 from
SPDSDEV.dbo.T01_TransactionMaster T01, SPDSDEV.dbo.T07_TransactionResponseMaster T07, SPDSDEV.dbo.T12_TransactionResponseDetails T12 where
T01.documentnumber = t07.documentnumber and
T07.responseid = T12.responseid and
T12.communicationtype = 'RECEIPT' and
T12.Status in ('H', 'Y') and lettercode in ('SS', 'S5') and
T12.RecordID not in
(select T12RecordID from [SPAPPSDEV].[dbo].[ReassignedLetterCodePriority] LCP where
 LCP.T12RecordID = T12.RecordID)
 
 
declare @ResponseID bigint
declare @T12RecordID bigint
declare @DocumentNumber bigint
declare @AccountNumber bigint
declare @OldLetterCode varchar(10)
declare @NewLetterCode varchar(10)
declare @d bigint
declare @IsMemorial int
declare @IsHonor int
declare @NewName int
declare @FiveOrMore int

DECLARE response_cursor CURSOR FOR
select T12RecordID, ResponseID, DocumentNumber, AccountNumber, OldLetterCode from [SPAPPSDEV].[dbo].[ReassignedLetterCodePriority] where processed = 0
OPEN response_cursor
WHILE (1 = 1) 
BEGIN
	FETCH NEXT FROM response_cursor into @T12RecordID, @ResponseID, @DocumentNumber, @AccountNumber, @OldLetterCode
	if (@@FETCH_STATUS != 0) break
	
	select @d = SPAPPSDEV.dbo.udfMemorialCardGift(@DocumentNumber)
	select @IsMemorial = 0
	if (@d > 0) select @IsMemorial = 1

	select @d = SPAPPSDEV.dbo.udfHonorCardGift(@DocumentNumber)
	select @IsHonor = 0
	if (@d > 0) select @IsHonor = 1
	
	select @d = SPAPPSDEV.dbo.udfNewNameTransaction(@DocumentNumber)
	select @NewName = 0
	if (@d > 0) select @NewName = 1
	
	select @FiveOrMore = 0	
	if ((select count(*) from SPDSDEV.dbo.T04_GiftDetails where DocumentNumber = @DocumentNumber) >= 5)
		select @FiveOrMore = 1

	--
	-- Figure the new letter code
	--
	select @NewLetterCode = @OldLetterCode
	if ((@NewName = 1) and ((@IsMemorial = 1) or (@IsHonor = 1)))
	BEGIN
		select @NewLetterCode = 'NGR'		-- New Name
	END
	else
	BEGIN
		if (@IsMemorial = 1)
		BEGIN
			if (@FiveOrMore = 1)
			BEGIN
				select @NewLetterCode = 'CM5'	-- Memorial five or more
			END
			else
			BEGIN
				select @NewLetterCode = 'CM'	-- Memorial 
			END
		END
		if (@IsHonor = 1)
		BEGIN
			if (@FiveOrMore = 1)
			BEGIN
				select @NewLetterCode = 'CH5'	-- Honor five or more
			END
			else
			BEGIN
				select @NewLetterCode = 'CH'	-- Honor
			END
		END
	END
	--
	-- Update records
	--
	--print convert(Varchar(10), @AccountNumber) + ' ' + @OldLetterCode + ' ' + @NewLetterCode
	BEGIN TRANSACTION
	if (@NewLetterCode <> @OldLetterCode)
	BEGIN
		update SPDSDEV.dbo.T07_TransactionResponseMaster set LetterCode = @NewLetterCode where ResponseID = @ResponseID
	END
	update [SPAPPSDEV].[dbo].[ReassignedLetterCodePriority] set ReassignedLetterCode = @NewLetterCode, Processed = 1 where
			T12RecordID =  @T12RecordID
	COMMIT TRANSACTION
END
CLOSE response_cursor
DEALLOCATE response_cursor
END TRY
BEGIN CATCH
	if (@@TranCount > 0) rollback transaction
END CATCH
