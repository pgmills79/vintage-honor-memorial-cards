use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspPullHonorCardsToPrintQueue' AND type = 'P')
    DROP PROCEDURE uspPullHonorCardsToPrintQueue
GO
Create Procedure uspPullHonorCardsToPrintQueue
as
set nocount on
BEGIN TRY
--------------------------------------------------------------------------------------------------------------
--  Clean up their null thing.
--------------------------------------------------------------------------------------------------------------
update SPDSPROD.dbo.SP02_CardMaster 
set PrintStatus = hc1.status from
  HonorMemorialCards1 HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
 HC1.DSRecordID = SP02.RecordId and
 SP02.PrintStatus is null and
 HC1.Status is not null
update SPDSPROD.dbo.SP02_CardMaster set PrintStatus = 'Y' where PrintStatus is null
update HonorMemorialCards1 set Status = '' where Status is NULL

 --------------------------------------------------------------------------------------------------------------
--  Move the cards over.
--------------------------------------------------------------------------------------------------------------
INSERT INTO [SPAPPSPROD].[dbo].[HonorMemorialCards1]
           ([AccountNumber]
           ,[DocumentNumber]
           ,[FromName]
           ,[MemorialName]
           ,[TransactionDate]
           ,[LetterCode]
           ,[Status]
           ,[Occurrance]
           ,[PrintGroup]
           ,[ToName]
           ,[ToAddress1]
           ,[ToAddress2]
           ,[ToAddress3]
           ,[ToCity]
           ,[ToState]
           ,[ToZip]
           ,[ToCountry]
           ,[NumberOfGifts]
           ,[WebPageName]
           ,[AddDate]
           ,[ChangeUser]
           ,[ChangeDate]
           ,[ReturnToSender]
           ,[DSRecordID])
     select 
           T01.AccountNumber, 
           T01.DocumentNumber,
           left(FromName, 50), 
           left(Memorial, 50),
           T01.[Date],
           left(CardType, 10),
           left(PrintStatus, 1),
           0,
           0,
           left(ToName, 50), 
           left(Address1, 70), 
           left(Address2, 70), 
           left(Address3, 70),
           left(City, 40), 
           left([State], 10), 
           left(ZipPostal, 15), 
           left(Country, 10),
           0,
           'DonorStudio',
           GETDATE(),
           'DONORSTUDIO',
           null,
           case when ReturnToSender = 1 then 'Y' else 'N' end,
           SP02.RecordId from SPDSPROD.dbo.SP02_CardMaster SP02 WITH (NOLOCK), SPDSPROD.dbo.T01_TransactionMaster T01 WITH (NOLOCK)
           where 
           T01.DocumentNumber = SP02.DocumentNumber and
           SP02.RecordId not in
           (Select DSRecordID from [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1 WITH (NOLOCK) where HC1.DSRecordID = SP02.RecordId)
--------------------------------------------------------------------------------------------------------------
--  Move the items over.
--
--  NOTE:  No longer compare items for update because it doesn't recognize case changes.  In other words,
-- a to name of "DAVE" and "Dave" is the same thing in SQL with our settings.
--
--------------------------------------------------------------------------------------------------------------
declare @CardID bigint
declare @Project varchar(10)
declare @ItemRecordID bigint
DECLARE ItemCursor CURSOR FOR
Select 
           HC1.CardDocumentNumber,
           SP01.ProjectCode,
           SP01.Recordid
		from SPDSPROD.dbo.SP01_CardDetails SP01 WITH (NOLOCK), SPDSPROD.dbo.SP02_CardMaster SP02 WITH (NOLOCK), [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1  WITH (NOLOCK) WHERE
		SP01.CardRecordId = SP02.RecordId and
		SP02.RecordId = HC1.DSRecordID and
		SP01.Recordid not in 
	    (Select DSRecordID from [SPAPPSPROD].[dbo].[HonorMemorialCards2] HC2  WITH (NOLOCK) WHERE HC2.DSRecordID = SP01.RecordId and HC2.Project = SP01.ProjectCode) and
	    HC1.CardDocumentNumber not in
	    (Select CardDocumentNumber from [SPAPPSPROD].[dbo].[HonorMemorialCards2] HC2b  WITH (NOLOCK) WHERE HC2b.CardDocumentNumber = HC1.CardDocumentNumber and HC2b.Project = SP01.ProjectCode)
OPEN ItemCursor
WHILE (1 = 1)
BEGIN
      FETCH NEXT FROM ItemCursor into @CardID, @Project, @ItemRecordID
      if (@@FETCH_STATUS != 0) break
		if not exists (select * from [SPAPPSPROD].[dbo].[HonorMemorialCards2]    WITH (NOLOCK) WHERE CardDocumentNumber = @CardID and Project = @Project)
		BEGIN
			INSERT INTO [SPAPPSPROD].[dbo].[HonorMemorialCards2]
			   ([CardDocumentNumber]
			   ,[Project]
			   ,[AddDate]
			   ,[DSRecordID])
			values
				(@CardID,
				 @Project,
				 GETDATE(),
				 @ItemRecordID)
		END
END
CLOSE ItemCursor
DEALLOCATE ItemCursor
--------------------------------------------------------------------------------------------------------------
--  Take out items that were deleted.
--------------------------------------------------------------------------------------------------------------
 
delete  [SPAPPSPROD].[dbo].[HonorMemorialCards2] from [SPAPPSPROD].[dbo].[HonorMemorialCards2] where DSRecordID not in
(Select RecordID from SPDSPROD.dbo.SP01_CardDetails where Recordid = DSRecordID)

delete from [SPAPPSPROD].[dbo].[HonorMemorialCards1] WHERE DSRecordID not in
(Select RecordID from SPDSPROD.dbo.SP02_CardMaster where Recordid = DSRecordID)
--------------------------------------------------------------------------------------------------------------
--  Make sure we have the latest data.
--------------------------------------------------------------------------------------------------------------
update dbo.HonorMemorialCards1 set AccountNumber = A11.AccountNumber from SPDSPROD.dbo.A11_AccountMergeHeaders A11   WITH (NOLOCK) , dbo.HonorMemorialCards1 HC1   WITH (NOLOCK)  where
A11.MergedAccountNumber = HC1.AccountNumber
--
update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
AccountNumber = SP02.AccountNumber 
from [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
HC1.AccountNumber <> SP02.AccountNumber

update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
DocumentNumber = SP02.DocumentNumber 
from [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
HC1.DocumentNumber <> SP02.DocumentNumber

update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
[Status] = upper(left(SP02.PrintStatus, 1))
from  [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
HC1.[Status] <> left(SP02.PrintStatus, 1)
--
update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
FromName = left(SP02.FromName, 50)
from  [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
--HC1.FromName <> left(SP02.FromName, 50) and 
HC1.Status <> 'P'
--
update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
MemorialName = left(SP02.Memorial, 50)
from  [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
--HC1.MemorialName <> left(SP02.Memorial, 50) and 
HC1.Status <> 'P'
--
update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
LetterCode = upper(left(SP02.CardType, 10))
from  [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
--HC1.LetterCode <> left(SP02.CardType, 10) and 
HC1.Status <> 'P'
--
update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
ToName = left(SP02.ToName, 50)
from  [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
--HC1.ToName <> left(SP02.ToName, 50) and 
HC1.Status <> 'P'
--
update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
ToAddress1 = left(SP02.Address1, 70)
from  [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
--HC1.ToAddress1 <> left(SP02.Address1, 70) and 
HC1.Status <> 'P'
--
update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
ToAddress2 = left(SP02.Address2, 70)
from  [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
--HC1.ToAddress2 <> left(SP02.Address2, 70) and 
HC1.Status <> 'P'
--
update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
ToAddress3 = left(SP02.Address3, 70)
from  [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
--HC1.ToAddress3 <> left(SP02.Address3, 70) and 
HC1.Status <> 'P'
--
update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
ToCity = left(SP02.City, 40)
from  [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
--HC1.ToCity <> left(SP02.City, 40) and 
HC1.Status <> 'P'
--
update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
ToState = left(SP02.State, 10)
from  [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
--HC1.ToState <> left(SP02.State, 10) and 
HC1.Status <> 'P'
--
update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
ToZip = left(SP02.ZipPostal, 15)
from  [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
--HC1.ToZip <> left(SP02.ZipPostal, 15) and 
HC1.Status <> 'P'
--
update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
ToCountry = left(SP02.Country, 10)
from  [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
--HC1.ToCountry <> left(SP02.Country, 10) and 
HC1.Status <> 'P'
--
update [SPAPPSPROD].[dbo].[HonorMemorialCards1] set 
ReturnToSender = case when SP02.ReturnToSender = 1 then 'Y' else 'N' end
from  [SPAPPSPROD].[dbo].[HonorMemorialCards1] HC1, SPDSPROD.dbo.SP02_CardMaster SP02 where
HC1.DSRecordID = SP02.RecordId and
HC1.ReturnToSender <> case when SP02.ReturnToSender = 1 then 'Y' else 'N' end and HC1.Status <> 'P'
--
END TRY
BEGIN CATCH
	IF (SELECT CURSOR_STATUS('local','sync_cursor')) >=0 
	BEGIN
		DEALLOCATE sync_cursor
	END
	declare @ErrorMessage varchar(255)
	select @ErrorMessage = substring('Error in  = uspPullHonorCardsToPrintQueue, procedure line ' + 
												   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
												   ERROR_MESSAGE(), 1, 255)
END CATCH
