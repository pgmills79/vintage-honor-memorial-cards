
declare @proj1 varchar(20)
declare @proj2 varchar(20)
declare @proj3 varchar(20)
declare @doc int
declare @acno int
declare @cardnumber float
declare @ErrorText char(80)
declare @count int
declare @numcard int
DECLARE card_cursor CURSOR FOR
select Project from SPAPPSPROD.dbo.ChristmasCatalogDescriptions

OPEN card_cursor
WHILE (1 = 1) 
BEGIN
	select @proj1 = ''
	select @proj2 = ''
	select @proj3 = ''
	FETCH NEXT FROM Card_Cursor
	into @proj1
	if (@@FETCH_STATUS != 0) break
	if (@@FETCH_STATUS = 0) FETCH NEXT FROM Card_Cursor	into @proj2
	if (@@FETCH_STATUS = 0) FETCH NEXT FROM Card_Cursor	into @proj3
	select @doc = 0
	select @acno = 0
	select top 1 @acno = t1z103acno, @doc = t1z103dcno from
			#x where g2z103proj = @proj1
	select @proj1, @proj2, @proj3, @acno, @doc
	exec  SPAPPSPROD.dbo.uspStartCard
					 @doc,
					 '',
					 'HCC',
					 'Ryan Eanes',
					 '',
					 '345 86th Street, Apt. 506',
					 '',
					 '',
					 'Brooklyn',
					 'NY',
					 '11209',
					 'DBSFix',
					 @CardNumber output,
					 @ErrorText output
	select @CardNumber, @ErrorText
	exec SPAPPSPROD.dbo.uspApertaCardItems
					 @CardNumber,
					 @proj1,
					 @Count output,
					 @ErrorText output
	select @numcard=1
	if (@proj2 != '')
	BEGIN
		exec SPAPPSPROD.dbo.uspApertaCardItems
						 @CardNumber,
						 @proj2,
						 @Count output,
						 @ErrorText output
		select @numcard=2
	END
	if (@proj3 != '')
	BEGIN
		exec SPAPPSPROD.dbo.uspApertaCardItems
						 @CardNumber,
						 @proj3,
						 @Count output,
						 @ErrorText output
		select @numcard=3
	END

	select @ErrorText
	exec SPAPPSPROD.dbo.uspEndCard
					 @CardNumber,
					 @numcard,
					 @ErrorText output
	select @ErrorText, @CardNumber
END
CLOSE card_cursor
DEALLOCATE card_cursor



--	select distinct t1z103acno, t1z103dcno, g2z103proj into #x from
--			testdta.fz103t1, testdta.fz103g2, SPAPPSPROD.dbo.ChristmasCatalogDescriptions where
--			t1z103dcno = g2z103dcno and
--			t1z103trdt > 108000 and
--			g2z103proj = Project


update SPAPPSPROD.dbo.HonorMemorialCards1 set Status = 'Y' where  Status = 'H'
--update testdta.f55hmc1 set Status = 'P' where  Status = 'Y'

select * from SPAPPSPROD.dbo.ChristmasCatalogDescriptions