use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspAddCardAudit' AND type = 'P')
    DROP PROCEDURE dbo.uspAddCardAudit
GO
Create Procedure dbo.uspAddCardAudit
		@RecordID bigint,
		@UserName varchar(80),
		@CardOrItem char(1),
		@AddOrChange char(1),
		@Status int output,
		@ErrorMessage varchar(256) output
as
set nocount on
BEGIN TRY
select @Status = 0
select @ErrorMessage = ''
declare @SessionRecordID bigint = 0
------------------------------------------------------------------------------------------------------------------
-- Find existing record.
------------------------------------------------------------------------------------------------------------------
select @SessionRecordID = MIN(recordid) from SPDSPROD.dbo.X29_AccessorSessions
where REPLACE(REPLACE(sourceid, 'SAMARITAN\', ''),'BG\', '') = REPLACE(REPLACE(@UserName, 'SAMARITAN\', ''),'BG\', '')
------------------------------------------------------------------------------------------------------------------
-- If session record does not exist, create one.
------------------------------------------------------------------------------------------------------------------
if ((@SessionRecordID is null) or (@SessionRecordID = 0))
BEGIN
	declare @SessionKey uniqueidentifier;
	select @SessionKey = NEWID()
	declare @StartDate datetime;
	select @StartDate = GETDATE()
	INSERT INTO [SPDSPROD].[dbo].[X29_AccessorSessions]
			   ([AccessorSource]
			   ,[SourceId]
			   ,[Active]
			   ,[SessionKey]
			   ,[DatabaseId]
			   ,[StartDateTime]
			   ,[EndDateTime])
		 VALUES
			   ('I',
			   Replace(replace(@UserName, 'SAMARITAN\', ''), 'BG\', ''),
			   1,
			   @SessionKey,
			   'SPDSPROD',
			   @StartDate,
			   @StartDate)
		select @SessionRecordID = SCOPE_IDENTITY()
END
------------------------------------------------------------------------------------------------------------------
-- If session record does not exist, create one.
------------------------------------------------------------------------------------------------------------------
declare @TableID varchar(10)
declare @Action varchar(10)

if (@CardOrItem = 'C') select @TableID = 'SP02_' else select @TableID = 'SP01_'
if (@AddOrChange = 'A') select @Action = 'INSERT' else select @Action = 'UPDATE'
--select @CardOrItem, @AddOrChange, @TableID, @Action
INSERT INTO [SPDSPROD].[dbo].[X30_AuditInformation]
           ([TableId]
           ,[TableRecordId]
           ,[Action]
           ,[ActionDateTime]
           ,[SessionRecordId])
     VALUES
           (@TableID,
           @RecordID,
           @Action,
           GETDATE(),
           @SessionRecordID)
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspUpdateHonorMemorialCardsDonorStudios, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) + ' '
	
END CATCH
