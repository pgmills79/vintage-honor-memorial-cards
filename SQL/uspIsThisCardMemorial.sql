use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspIsThisCardMemorial' AND type = 'P')
    DROP PROCEDURE dbo.uspIsThisCardMemorial
GO
Create Procedure dbo.uspIsThisCardMemorial
	    @LetterCode VARCHAR(10),
		@IsMemorial INT OUTPUT,
		@Status int output,
		@ErrorMessage varchar(256) output
as
set nocount on
BEGIN TRY
	SET @status = 0
	SET @ErrorMessage = 0
	SET @IsMemorial = 0

	IF EXISTS (SELECT * FROM SPDSPROD.dbo.X04cCodeValueCodeAttributes WHERE type = 'SPCARDTYPE' AND value = @LetterCode AND AttributeType = 'MEMORIAL' AND AttributeValue = 'Y')
	BEGIN
		SET @IsMemorial = 1
	END
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspIsThisCardMemorial, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH

-- select * from SPAPPSPROD.dbo.HonorMemorialCards1 
