IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspGetDocumentProducts' AND type = 'P')
    DROP PROCEDURE dbo.uspGetDocumentProducts
GO
Create Procedure dbo.uspGetDocumentProducts
		@AccountNumber bigint,
		@Status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
	declare @LetterCode varchar(10)
	select @status = 0
	select @ErrorMessage = 0
	declare @AccountTable as table
	(
		Account bigint null
	)
insert into @AccountTable
(Account)
values
(@AccountNumber)

insert into @AccountTable
(Account)
select (FamilyID) from SPDSPROD.dbo.A01_AccountMaster where AccountNumber = @AccountNumber and FamilyID > 0

insert into @AccountTable 
(Account)
select (AccountNumber) from SPDSPROD.dbo.A01_AccountMaster A01, @AccountTable at where
A01.FamilyID = at.Account

select top 200 T01.AccountNumber, SPAPPSPROD.dbo.udfFormatAccountName (T01.AccountNumber, 'ALL') as AccountName, T01.DocumentNumber, convert(varchar(10), T01.date, 101) as TransactionDate, T04.ProjectCode, Y01.Description as ProjectDescription
from SPDSPROD.dbo.A01_AccountMaster A01, SPDSPROD.dbo.T01_TransactionMaster T01, SPDSPROD.dbo.T04_GiftDetails T04, SPDSPROD.dbo.Y01_ProjectMaster Y01 where
A01.accountnumber = T01.accountnumber and
T04.documentnumber = T01.documentnumber and
A01.AccountNumber in (select account from @AccountTable where account = A01.accountnumber) and
T04.projectcode = Y01.projectcode
order by T01.date desc, T01.documentnumber desc
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspCardTypeOfCard, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH

-- select * from SPAPPSPROD.dbo.HonorMemorialCards1 
