use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspGetListHonorMemorialCards' AND type = 'P')
    DROP PROCEDURE dbo.uspGetListHonorMemorialCards
GO
Create Procedure dbo.uspGetListHonorMemorialCards
		@Status int output,
		@ErrorMessage varchar(256) output
as
set nocount on
BEGIN TRY
	select @status = 0
	select @ErrorMessage = 0
SELECT [Value] AS LetterCode, Value + '-' + description AS Description FROM spdsprod.dbo.X04_CodeValues WHERE type = 'SPCARDTYPE' ORDER BY value
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspGetListHonorMemorialCards, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH

-- select * from SPAPPSPROD.dbo.HonorMemorialCards1 
