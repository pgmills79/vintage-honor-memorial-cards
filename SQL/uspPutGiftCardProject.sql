IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspPutGiftsOfCard' AND type = 'P')
    DROP PROCEDURE dbo.uspPutGiftsOfCard
GO
Create Procedure dbo.uspPutGiftsOfCard
		@CardID int,
		@Project varchar(10),
		@Guid varchar(200) output,
		@Status int output,
		@ErrorMessage varchar(3000) output
as
set nocount on
BEGIN TRY
	declare @DateTime datetime
	select @DateTime = getdate()
	select @status = 0
	select @ErrorMessage = 0
	if (@Guid = '') select @Guid = newid()
	if (@Project = '') return;
	if not exists (select Project from SPAPPSPROD.dbo.ChristmasCatalogDescriptions where Project = @Project) return;

	if not exists (select project from SPAPPSPROD.dbo.HonorMemorialItemTemp where
				   [Guid] = @Guid and
				   [CardID] = @Cardid and
				   [Project] = @Project)
	BEGIN
		insert into SPAPPSPROD.dbo.HonorMemorialItemTemp 
		([Guid], CardID, Project, CurrentDate)
		values
		(@Guid, @CardID, @Project, @DateTime)
	END
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspPutGiftsOfCard, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH
