use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspResetFormattedCardData' AND type = 'P')
    DROP PROCEDURE dbo.uspResetFormattedCardData
GO

Create Procedure dbo.uspResetFormattedCardData
		@PrintGroup int,
		@status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
select @status = 0
select @ErrorMessage = ''
update SPAPPSPROD.dbo.HonorMemorialCards1 set Status = 'Y' where PrintGroup = @PrintGroup and status <> 'T'
update SPDSPROD.dbo.SP02_CardMaster 
set printstatus = HC1.Status from  SPAPPSPROD.dbo.HonorMemorialCards1 hc1, SPDSPROD.dbo.SP02_CardMaster SP02 where
hc1.dsrecordid = SP02.recordid and
HC1.Printgroup = @PrintGroup
update SPAPPSPROD.dbo.HonorMemorialCards1 set PrintGroup = 0 where PrintGroup = @PrintGroup
delete from dbo.HonorCardFormatting where PrintGroup = @PrintGroup
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspResetFormattedCardData, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH

