use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspGetTypeOfCardDonorStudio' AND type = 'P')
    DROP PROCEDURE dbo.uspGetTypeOfCardDonorStudio
GO
Create Procedure [dbo].[uspGetTypeOfCardDonorStudio]
		@CardID int,
		@CardType char(1) output,
		@InMemorialOf varchar(50) output,
		@ReturnToSender char(1) output,
		@Status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
	select @status = 0
	select @ErrorMessage = 0
	select @CardType = left(CardType, 1) from SPDSPROD.DBO.SP02_CardMaster where RecordId = @CardID
	select @InmemorialOf = Memorial from SPDSPROD.DBO.SP02_CardMaster where RecordId = @CardID
	select @ReturnToSender = case when ReturnToSender = 1 then 'Y' else 0 END from SPDSPROD.DBO.SP02_CardMaster where RecordId = @CardID
	if (@CardType not in ('H', 'M')) select @CardType = 'H'
	if (@InMemorialOf is null) select @InMemorialOf = ''
	if (@ReturnTOSender not in ('Y', 'N')) select @ReturnToSender = 'Y'
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspGetTypeOfCardDonorStudio, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH

-- select * from SPDSPROD.DBO.SP02_CardMaster 
