USE [SPAPPSPROD]
GO
/****** Object:  StoredProcedure [dbo].[uspFormatCardData4]    Script Date: 7/1/2016 10:59:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------
--  7/1/2016 S. Burton
--    Added code to pull cards <= Through Date.  Added NOLOCKs.
------------------------------------------------------------------------
ALTER Procedure [dbo].[uspFormatCardData4]
		@PullType char(1), -- Y for ready T for test
		@WhichCards INT,	-- Bit 1 Include HCC, MCC; Bit 2 Include HAC, MAC; Bit 3 Include OHC, OMC; Bit 4 include MOTH
		@ThroughDate DATETIME,  -- Only select cards having trans date <= this date.
		@PrintGroup int output,
		@status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
select @status = 0
select @ErrorMessage = ''
select @PrintGroup = 0
declare @LastCardID [int]
declare @CardID [int]
declare @DocumentNumber [int] 
declare @AccountNumber [int] 
declare @FromName [varchar](50) 
declare @MemorialName [varchar](50) 
declare @Letter [varchar](10)
declare @Type [int] 
declare @DateGenerated [datetime] 
declare @ToName [varchar](50) 
declare @ToAddress1 [varchar](70) 
declare @ToAddress2 [varchar](70) 
declare @ToAddress3 [varchar](70) 
declare @ToCity [varchar](40) 
declare @ToState [varchar](10) 
declare @ToZip [varchar](15) 
declare @ToCountry [varchar](10) 
declare @Project [varchar](10) 
declare @ShortDescription [varchar](100) 
declare @LongDescription [varchar](400) 
declare @Project01 [varchar](10) 
declare @ShortDescription01 [varchar](100) 
declare @LongDescription01 [varchar](400) 
declare @Project02 [varchar](10) 
declare @ShortDescription02 [varchar](100) 
declare @LongDescription02 [varchar](400) 
declare @Project03 [varchar](10) 
declare @ShortDescription03 [varchar](100) 
declare @LongDescription03 [varchar](400) 
declare @Project04 [varchar](10) 
declare @ShortDescription04 [varchar](100) 
declare @LongDescription04 [varchar](400) 
declare @Project05 [varchar](10) 
declare @ShortDescription05 [varchar](100) 
declare @LongDescription05 [varchar](400) 
declare @Project06 [varchar](10) 
declare @ShortDescription06 [varchar](100) 
declare @LongDescription06 [varchar](400) 
declare @Project07 [varchar](10) 
declare @ShortDescription07 [varchar](100) 
declare @LongDescription07 [varchar](400) 
declare @Project08 [varchar](10) 
declare @ShortDescription08 [varchar](100) 
declare @LongDescription08 [varchar](400) 
declare @Project09 [varchar](10) 
declare @ShortDescription09 [varchar](100) 
declare @LongDescription09 [varchar](400) 
declare @Project10 [varchar](10) 
declare @ShortDescription10 [varchar](100) 
declare @LongDescription10 [varchar](400) 
declare @GiftNumber int
declare @FullDescription int
declare @RTS char(1)
declare @On306 int
declare @ReturnToSender int
declare @FileName varchar(50)

DECLARE @MaxDate DATETIME

SET @MaxDate = dbo.udfMaxDateTime(@ThroughDate);

--
--  Bring cards over from Donor Studio
--
exec SPAPPSPROD.dbo.uspPullHonorCardsToPrintQueue
--
-- Make sure imports are done and the accoutns are assigned.
--
update SPAPPSPROD.dbo.HonorMemorialCards1 set AccountNumber = 0 where AccountNumber is null
update SPAPPSPROD.dbo.HonorMemorialCards1 set AccountNumber = T01.AccountNumber from SPAPPSPROD.dbo.HonorMemorialCards1 C1 (NOLOCK), SPDSPROD.dbo.T01_TransactionMaster T01 (NOLOCK) WHERE
C1.DocumentNumber = T01.DocumentNumber and C1.Status = @PullType
--
-- If there are records, assign the print group that we will be using throughout this process.
--
if exists (select * from SPAPPSPROD.dbo.HonorMemorialCards1 (NOLOCK) where Status = @PullType and AccountNumber > 0 AND
																	((@WhichCards IN (1,3,5,7,9,11,13,15) AND LetterCode IN ('HCC', 'MCC')) OR
																	(@WhichCards IN (2,3,6,7,10,11,14,15) AND LetterCode IN ('HAC', 'MAC')) OR
																	(@WhichCards IN (4,5,6,7,12,13,14,15) AND LetterCode IN ('OHC', 'OMC')) OR
																	(@WhichCards IN (8,9,10,11,12,13,14,15) AND LetterCode IN ('MOTH'))) AND
																	TransactionDate < @MaxDate)
BEGIN
	exec dbo.uspGetNextPrintGroup @PrintGroup output
	update SPAPPSPROD.dbo.HonorMemorialCards1 set PrintGroup = @PrintGroup, Status = 'I' from SPAPPSPROD.dbo.HonorMemorialCards1 (NOLOCK) where
           Status = @PullType and AccountNumber > 0 AND
		   ((@WhichCards IN (1,3,5,7,9,11,13,15) AND LetterCode IN ('HCC', 'MCC')) OR
			(@WhichCards IN (2,3,6,7,10,11,14,15) AND LetterCode IN ('HAC', 'MAC')) OR
			(@WhichCards IN (4,5,6,7,12,13,14,15) AND LetterCode IN ('OHC', 'OMC')) OR
			(@WhichCards IN (8,9,10,11,12,13,14,15) AND LetterCode IN ('MOTH'))) AND
			TransactionDate < @MaxDate
END
delete from dbo.HonorCardFormatting where printgroup = @PrintGroup
select @GiftNumber = 0
DECLARE card_cursor CURSOR FOR
select 
C1.CardDocumentNumber, AccountNumber, DocumentNumber, FromName, MemorialName, LetterCode, ToName, ToAddress1, ToAddress2, ToAddress3, ToCity, ToState, ToZip, ToCountry, Project, ReturnToSender
from SPAPPSPROD.dbo.HonorMemorialCards1 C1 (NOLOCK), SPAPPSPROD.dbo.HonorMemorialCards2 C2 (NOLOCK) WHERE
         C1.CardDocumentNumber = C2.CardDocumentNumber and Status = 'I' and PrintGroup = @PrintGroup
		 order by C1.CardDocumentNumber
select @LastCardID = 0
OPEN card_cursor
WHILE (1 = 1) 
BEGIN
	FETCH NEXT FROM card_cursor into 
		@CardID, @AccountNumber, @DocumentNumber, @FromName, @MemorialName, @Letter, @ToName, @ToAddress1, @ToAddress2, @ToAddress3, @ToCity, @ToState, @ToZip, @ToCountry, @Project, @RTS
	if (@@FETCH_STATUS != 0) break
	--
	--  OK, we started a new card
	--
	if (@CardID <> @LastCardID)
	BEGIN
		select @GiftNumber = 1
		select @Project01 = Null
		select @ShortDescription01 = Null
		select @LongDescription01 = Null
		select @Project02 = Null
		select @ShortDescription02 = Null
		select @LongDescription02 = Null
		select @Project02 = Null
		select @ShortDescription03 = Null
		select @LongDescription03 = Null
		select @Project02 = Null
		select @ShortDescription04 = Null
		select @LongDescription04 = Null
		select @Project02 = Null
		select @ShortDescription05 = Null
		select @LongDescription05 = Null
		select @Project02 = Null
		select @ShortDescription06 = Null
		select @LongDescription06 = Null
		select @Project02 = Null
		select @ShortDescription07 = Null
		select @LongDescription07 = Null
		select @Project02 = Null
		select @ShortDescription08 = Null
		select @LongDescription08 = Null
		select @Project02 = Null
		select @ShortDescription09 = Null
		select @LongDescription09 = Null
		select @Project02 = Null
		select @ShortDescription10 = Null
		select @LongDescription10 = Null
	END
	--
	-- Get project description
	--
	select @ShortDescription = Null
	select @LongDescription = Null
	if (@Project = '') select @Project = Null
	if (@Project is not null)
	BEGIN
		select @ShortDescription = Title, @LongDescription = Body from SPAPPSPROD.dbo.ChristmasCatalogDescriptions (NOLOCK)
			where Project = @Project
		if (@ShortDescription is null) select @ShortDescription = 'Unknown = ' + @Project
		if (@LongDescription is null) select @LongDescription = 'Unknown = ' + @Project
		select @Project = replace(@Project, '"', '''')
		select @ShortDescription = replace(@ShortDescription, '"', '''')
		select @LongDescription = replace(@LongDescription, '"', '''')
	END
	--
	--  Assign to the correct gift.
	--
	if (@GiftNumber = 1)
	BEGIN
		select @Project01 = @Project
		select @ShortDescription01 = @ShortDescription
		select @LongDescription01 = @LongDescription
	END
	if (@GiftNumber = 2)
	BEGIN
		select @Project02 = @Project
		select @ShortDescription02 = @ShortDescription
		select @LongDescription02 = @LongDescription
	END
	if (@GiftNumber = 3)
	BEGIN
		select @Project03 = @Project
		select @ShortDescription03 = @ShortDescription
		select @LongDescription03 = @LongDescription
	END
	if (@GiftNumber = 4)
	BEGIN
		select @Project04 = @Project
		select @ShortDescription04 = @ShortDescription
		select @LongDescription04 = @LongDescription
	END
	if (@GiftNumber = 5)
	BEGIN
		select @Project05 = @Project
		select @ShortDescription05 = @ShortDescription
		select @LongDescription05 = @LongDescription
	END
	if (@GiftNumber = 6)
	BEGIN
		select @Project06 = @Project
		select @ShortDescription06 = @ShortDescription
		select @LongDescription06 = @LongDescription
	END
	if (@GiftNumber = 7)
	BEGIN
		select @Project07 = @Project
		select @ShortDescription07 = @ShortDescription
		select @LongDescription07 = @LongDescription
	END
	if (@GiftNumber = 8)
	BEGIN
		select @Project08 = @Project
		select @ShortDescription08 = @ShortDescription
		select @LongDescription08 = @LongDescription
	END
	if (@GiftNumber = 9)
	BEGIN
		select @Project09 = @Project
		select @ShortDescription09 = @ShortDescription
		select @LongDescription09 = @LongDescription
	END
	if (@GiftNumber = 10)
	BEGIN
		select @Project10 = @Project
		select @ShortDescription10 = @ShortDescription
		select @LongDescription10 = @LongDescription
	END
	--
	-- Figure out the type
	--
	select @FullDescription = 0
	select @On306 = 0
	select @ReturnToSender = 0
	if (@RTS = @PullType) select @ReturnToSender = 1

	if exists (select * from SPDSPROD.dbo.A01cAccountCodes (NOLOCK) where CodeType = 'SECURE' and CodeValue = '306' and AccountNumber = @AccountNumber) select @On306 = 1
	if (@GiftNumber > 3) select @FullDescription = 1
	select @Type = 0
	if (@FullDescription = 1) select @Type = @Type + 1
	if (@ReturnToSender = 1) select @TYpe = @Type + 2
	if (@On306 = 1) select @Type = @Type + 4
	if (@Letter IN ('MCC', 'MAC', 'OMC')) select @Type = @Type + 8
	select @Type = @Type + 1
	select @FileName = ''
	if (@Type = 1) select @FileName = '01-Honor-Not306-NotReturn-3OrUnder'
	if (@Type = 2) select @FileName = '02-Honor-Not306-NotReturn-Over3'
	if (@Type = 3) select @FileName = '03-Honor-Not306-Return-3OrUnder'
	if (@Type = 4) select @FileName = '04-Honor-Not306-Return-Over3'
	if (@Type = 5) select @FileName = '05-Honor-306-NotReturn-3OrUnder'
	if (@Type = 6) select @FileName = '06-Honor-306-NotReturn-Over3'
	if (@Type = 7) select @FileName = '07-Honor-306-Return-3OrUnder'
	if (@Type = 8) select @FileName = '08-Honor-306-Return-Over3'
	if (@Type = 9) select @FileName = '09-Memorial-Not306-NotReturn-3OrUnder'
	if (@Type = 10) select @FileName = '10-Memorial-Not306-NotReturn-Over3'
	if (@Type = 11) select @FileName = '11-Memorial-Not306-Return-3OrUnder'
	if (@Type = 12) select @FileName = '12-Memorial-Not306-Return-Over3'
	if (@Type = 13) select @FileName = '13-Memorial-306-NotReturn-3OrUnder'
	if (@Type = 14) select @FileName = '14-Memorial-306-NotReturn-Over3'
	if (@Type = 15) select @FileName = '15-Memorial-306-Return-3OrUnder'
	if (@Type = 16) select @FileName = '16-Memorial-306-Return-Over3'
	IF (@Letter IN ('HAC', 'MAC')) SET @Type = @Type + 100
	IF (@Letter IN ('OHC', 'OMC')) SET @Type = @Type + 200
	IF (@Letter IN ('MOTH')) SET @Type = @Type + 300
	--
	-- Clean up data
	--
	select @FromName = replace(@FromName, '"', '''')
	select @MemorialName = replace(@MemorialName, '"', '''')	
	select @ToName = replace(@ToName, '"', '''')
	select @ToAddress1 = replace(@ToAddress1, '"', '''') 
	select @ToAddress2 = replace(@ToAddress2, '"', '''') 
	select @ToAddress3 = replace(@ToAddress3, '"', '''') 
	select @ToCity = replace(@ToCity, '"', '''')
	select @ToState = replace(@ToState, '"', '''')
	select @ToZip = replace(@ToZip, '"', '''')
	select @ToCountry = replace(@ToCountry, '"', '''')
	select @ToCountry = replace(@ToCountry, 'USA', '')
	--
	-- Put in current amount.  If more than one gift, may change so we will delete the old one and add the new information.
	--
	delete from dbo.HonorCardFormatting where printgroup = @PrintGroup and cardid = @CardID
	insert into dbo.HonorCardFormatting
	(PrintGroup, CardID, DocumentNumber, AccountNumber, [FileName], FromName, MemorialName, [Type], DateGenerated, ToName, ToAddress1, ToAddress2, ToAddress3, ToCity, ToState, ToZip, ToCountry, StdAddress1, StdAddress2, StdAddress3, StdCity, StdState, StdZip, StdCountry, StdError, Project01, ShortDescription01, LongDescription01, Project02, ShortDescription02, LongDescription02, Project03, ShortDescription03, LongDescription03, Project04, ShortDescription04, LongDescription04, Project05, ShortDescription05, LongDescription05, Project06, ShortDescription06, LongDescription06, Project07, ShortDescription07, LongDescription07, Project08, ShortDescription08, LongDescription08, Project09, ShortDescription09, LongDescription09, Project10, ShortDescription10, LongDescription10)
	values
	(@PrintGroup, @CardID, @DocumentNumber, @AccountNumber, @FileName, @FromName, @MemorialName, @Type, getdate(), @ToName, @ToAddress1, @ToAddress2, @ToAddress3, @ToCity, @ToState, @ToZip, @ToCountry, '', '', '', '', '', '', '', '', @Project01, @ShortDescription01, @LongDescription01, @Project02, @ShortDescription02, @LongDescription02, @Project03, @ShortDescription03, @LongDescription03, @Project04, @ShortDescription04, @LongDescription04, @Project05, @ShortDescription05, @LongDescription05, @Project06, @ShortDescription06, @LongDescription06, @Project07, @ShortDescription07, @LongDescription07, @Project08, @ShortDescription08, @LongDescription08, @Project09, @ShortDescription09, @LongDescription09, @Project10, @ShortDescription10, @LongDescription10)
	select @LastCardID = @CardID
	--
	-- Next gift
	--
	select @GiftNumber = @GiftNumber + 1
END
CLOSE card_cursor
DEALLOCATE card_cursor
--
--  Set as printed.
--
IF (@PrintGroup > 0)
BEGIN
	IF (@PullType= 'Y') update SPAPPSPROD.dbo.HonorMemorialCards1 set Status = 'P' where PrintGroup = @PrintGroup
	IF (@PullType = 'T') update SPAPPSPROD.dbo.HonorMemorialCards1 set Status = 'T' where PrintGroup = @PrintGroup
	UPDATE SPDSPROD.dbo.SP02_CardMaster 
	SET printstatus = HC1.Status from  SPAPPSPROD.dbo.HonorMemorialCards1 hc1 (NOLOCK), SPDSPROD.dbo.SP02_CardMaster SP02 where
	hc1.dsrecordid = SP02.recordid and
	HC1.Printgroup = @PrintGroup
END

END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspFormatCardData4, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH