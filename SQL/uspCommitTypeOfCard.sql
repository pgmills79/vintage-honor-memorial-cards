IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspCommitTypeOfCard' AND type = 'P')
    DROP PROCEDURE dbo.uspCommitTypeOfCard
GO
Create Procedure dbo.uspCommitTypeOfCard
		@CardID int,
		@CardType char(1),
		@InMemorialOf varchar(50),
		@UserName varchar(50),
		@ReturnToSender char(1),
		@Status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
	declare @LetterCode varchar(10)
	select @status = 0
	select @ErrorMessage = 0
	select @LetterCode = 'HCC'
	if (@CardType = 'M') select @LetterCode = 'MCC'
	update SPAPPSPROD.DBO.Honormemorialcards1 
	set lettercode = @LetterCode,
		MemorialName = @InMemorialOf,
		ReturnToSender = @ReturnToSender,
		ChangeUser = @UserName,
		ChangeDate = getdate()
	where CardDocumentNumber = @CardID
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspCardTypeOfCard, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH

-- select * from SPAPPSPROD.dbo.HonorMemorialCards1 
