use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspCountCardsReady3' AND type = 'P')
    DROP PROCEDURE dbo.uspCountCardsReady3
GO
Create Procedure dbo.uspCountCardsReady3
		@PullType char(1),
		@iCountRegular int output,
		@iCountAcknowledge int output,
		@iCountOHOP int output,
		@iCountMOTH int output,
		@status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
select @status = 0
select @ErrorMessage = ''
exec uspPullHonorCardsToPrintQueue
---
--  Regular
--
select @iCountRegular = count(*) from SPAPPSPROD.dbo.HonorMemorialCards1 where Status = @PullType and AccountNumber > 0 and 
CardDocumentNumber in (select CardDocumentNumber from SPAPPSPROD.dbo.HonorMemorialCards2 where CardDocumentNumber = CardDocumentNumber) AND
LetterCode IN ('HCC', 'MCC')
---
--  Acknowledgement
--
select @iCountAcknowledge = count(*) from SPAPPSPROD.dbo.HonorMemorialCards1 where Status = @PullType and AccountNumber > 0 and 
CardDocumentNumber in (select CardDocumentNumber from SPAPPSPROD.dbo.HonorMemorialCards2 where CardDocumentNumber = CardDocumentNumber) AND
LetterCode IN ('HAC', 'MAC')
---
--  OHOP
--
select @iCountOHOP = count(*) from SPAPPSPROD.dbo.HonorMemorialCards1 where Status = @PullType and AccountNumber > 0 and 
CardDocumentNumber in (select CardDocumentNumber from SPAPPSPROD.dbo.HonorMemorialCards2 where CardDocumentNumber = CardDocumentNumber) AND
LetterCode IN ('OHC', 'OMC')
---
--  Mother's Day
--
select @iCountMOTH = count(*) from SPAPPSPROD.dbo.HonorMemorialCards1 where Status = @PullType and AccountNumber > 0 and 
CardDocumentNumber in (select CardDocumentNumber from SPAPPSPROD.dbo.HonorMemorialCards2 where CardDocumentNumber = CardDocumentNumber) AND
LetterCode IN ('MOTH')

IF (@iCountRegular is null) select @iCountRegular = 0
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspCountCardsReady3, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH
