use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspGetListOfValidCardProjects' AND type = 'P')
    DROP PROCEDURE dbo.uspGetListOfValidCardProjects
GO
Create Procedure dbo.uspGetListOfValidCardProjects
		@Status int output,
		@ErrorMessage varchar(256) output
as
set nocount on
BEGIN TRY
	select @status = 0
	select @ErrorMessage = 0
	select '' as Project, '' As Description
	union
	select  rtrim(ltrim(project)) as Project, rtrim(ltrim(Project + ' ' + isnull(SingleWord, ''))) as Description from SPAPPSPROD.dbo.ChristmasCatalogDescriptions order by project
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspGetListOfValidCardProjects, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH

-- select * from SPAPPSPROD.dbo.HonorMemorialCards1 
