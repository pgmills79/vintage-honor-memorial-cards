IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspGetReturnToSenderLabelData' AND type = 'P')
    DROP PROCEDURE dbo.uspGetReturnToSenderLabelData
GO
Create Procedure dbo.uspGetReturnToSenderLabelData
		@PrintGroup int,
		@status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
declare @iCount int

select @status = 0
select @ErrorMessage = ''
select distinct [PrintGroup], AccountNumber, [Type], [FileName],
	   space(80) as Line1,
	   space(80) as Line2, 
	   space(80) as Line3, 
	   space(80) as Line4, 
	   space(80) as Line5,
	   space(80) as Line6
into #x from SPAPPSPROD.dbo.HonorCardFormatting where PrintGroup = @PrintGroup 
--
-- Populate the fields
--
update #x set line1 = organizationname  from #x x, SPDSPROD.dbo.A01_AccountMaster A01 where
x.accountnumber = A01.AccountNumber and
accounttype in ('O', 'F')
update #x set line1 = COALESCE(Title, '') + ' ' + COALESCE(FirstName, '') + ' ' + COALESCE(LastName, '') + ' ' + COALESCE(suffix, '') from #x x, SPDSPROD.dbo.A01_AccountMaster A01 where
x.accountnumber = A01.AccountNumber and
accounttype not in ('O', 'F')
update #x set line2 = AddressLine1, 
			  line3 = AddressLine2,  
			  line4 = AddressLine3,  
			  line5 = AddressLine4,
			  line6 = COALESCE(City, '') + ' ' + COALESCE([state], '') + ' ' + COALESCE(ZipPostal, '')
from #x x, 
		SPDSPROD.dbo.A01_AccountMaster A01,
		SPDSPROD.dbo.A02_AccountAddresses A02,
		SPDSPROD.dbo.A03_AddressMaster A03 where
x.accountnumber = A01.AccountNumber and
A01.accountnumber = A02.accountnumber and
A02.Addressid = A03.AddressID and
A02.active = 1 and
A02.useasprimary = 1

update #x set	line1 = ltrim(rtrim(replace(line1, '  ', ' '))),
				line2 = ltrim(rtrim(line2)),
				line3 = ltrim(rtrim(line3)),
				line4 = ltrim(rtrim(line4)),
				line5 = ltrim(rtrim(line5)),
				line6 = ltrim(rtrim(line6))

update #x set line1 = '' where line1 is null
update #x set line2 = '' where line2 is null
update #x set line3 = '' where line3 is null
update #x set line4 = '' where line4 is null
update #x set line5 = '' where line5 is null
update #x set line6 = '' where line6 is null
select @iCount = 6
while (@iCount > 0)
BEGIN
	update #x set line1 = line2, line2=line3, line3=line4, line4 = line5, line5 = line6, line6 = '' where line1 = ''
	update #x set line2=line3, line3=line4, line4 = line5, line5 = line6, line6 = '' where line2 = ''
	update #x set line3=line4, line4 = line5, line5  = line6, line6 = '' where line3 = ''
	update #x set line4 = line5, line5  = line6, line6 = '' where line4 = ''
	select @iCount = @iCount - 1
END
select * from #x order by printgroup, [type], accountnumber
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspGetReturnToSenderLabelData, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH

