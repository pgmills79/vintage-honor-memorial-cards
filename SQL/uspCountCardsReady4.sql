USE [SPAPPSPROD]
GO
/****** Object:  StoredProcedure [dbo].[uspCountCardsReady4]    Script Date: 7/1/2016 10:10:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------
-- 07/01/2016 S. Burton
--   Added code to select by through-date.  Added NOLOCKs.
------------------------------------------------------------

ALTER Procedure [dbo].[uspCountCardsReady4]
		@PullType char(1),
		@ThroughDate DATETIME,
		@iCountRegular int output,
		@iCountAcknowledge int output,
		@iCountOHOP int output,
		@iCountMOTH int output,
		@status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
select @status = 0
select @ErrorMessage = ''

DECLARE @MaxDate	DATETIME;

SET @MaxDate = dbo.udfMaxDateTime(@ThroughDate);

exec uspPullHonorCardsToPrintQueue
---
--  Regular
--
select @iCountRegular = count(*) from SPAPPSPROD.dbo.HonorMemorialCards1 (NOLOCK) WHERE Status = @PullType and AccountNumber > 0 AND 
TransactionDate < @MaxDate AND
CardDocumentNumber in (select CardDocumentNumber from SPAPPSPROD.dbo.HonorMemorialCards2 (NOLOCK) where CardDocumentNumber = CardDocumentNumber) AND
LetterCode IN ('HCC', 'MCC')
---
--  Acknowledgement
--
select @iCountAcknowledge = count(*) from SPAPPSPROD.dbo.HonorMemorialCards1 (NOLOCK) where Status = @PullType and AccountNumber > 0 AND 
TransactionDate < @MaxDate AND
CardDocumentNumber in (select CardDocumentNumber from SPAPPSPROD.dbo.HonorMemorialCards2 (NOLOCK) WHERE CardDocumentNumber = CardDocumentNumber) AND
LetterCode IN ('HAC', 'MAC')
---
--  OHOP
--
select @iCountOHOP = count(*) from SPAPPSPROD.dbo.HonorMemorialCards1 (NOLOCK) where Status = @PullType and AccountNumber > 0 AND
TransactionDate < @MaxDate AND
CardDocumentNumber in (select CardDocumentNumber from SPAPPSPROD.dbo.HonorMemorialCards2 (NOLOCK) where CardDocumentNumber = CardDocumentNumber) AND
LetterCode IN ('OHC', 'OMC')
---
--  Mother's Day
--
select @iCountMOTH = count(*) from SPAPPSPROD.dbo.HonorMemorialCards1 (NOLOCK) where Status = @PullType and AccountNumber > 0 AND 
TransactionDate < @MaxDate AND
CardDocumentNumber in (select CardDocumentNumber from SPAPPSPROD.dbo.HonorMemorialCards2 (NOLOCK) where CardDocumentNumber = CardDocumentNumber) AND
LetterCode IN ('MOTH')

IF (@iCountRegular is null) select @iCountRegular = 0
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspCountCardsReady4, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH
