use SPAPPSPROD
go
drop function usfMatchCardWithTransaction
go
CREATE function usfMatchCardWithTransaction(@CardID bigint, @AccountNumber bigint, @TransDate datetime)
returns Bigint
begin
declare @ReturnDocumentNumber bigint
declare @xDocumentNumber bigint
declare @yDocumentNumber bigint
declare @xAccountNumber bigint
	select @ReturnDocumentNumber = 0
	DECLARE CardCursor CURSOR FOR
	select AccountNumber, DocumentNumber from dbo.HonorMemorialCards1 h1 where 
	carddocumentnumber = @CardID 
	OPEN CardCursor
	WHILE (1 = 1)
	BEGIN
		  FETCH NEXT FROM CardCursor into @xAccountNumber, @xDocumentNumber
		  if (@@FETCH_STATUS != 0) break
		if exists (select * from SPDSPROD.dbo.T01_TransactionMaster where accountnumber = @xAccountNumber and documentnumber = @xDocumentNumber)
		BEGIN
			select @ReturnDocumentNumber = @xDocumentNumber
		END
	END
	--------------------------------------------------------------------------------------------------------------
	--  Try to find within range
	--------------------------------------------------------------------------------------------------------------
	if (@ReturnDocumentNumber = 0)
	BEGIN
		DECLARE @ProjectCode varchar(10)
		DECLARE @Exists int
		DECLARE TransCursor CURSOR FOR
			select DocumentNumber from SPDSPROD.dbo.T01_TransactionMaster where accountnumber = @AccountNumber and datepart(yy, [date]) = datepart(yy, @TransDate)
			OPEN TransCursor
			WHILE (1 = 1)
			BEGIN
				  FETCH NEXT FROM TransCursor into @yDocumentNumber
				  if (@@FETCH_STATUS != 0) break
				  DECLARE itemcursor CURSOR for
				  select Project from HOnorMemorialCards2 where CardDocumentNumber = @CardID
				  OPEN itemcursor
				  select @Exists = 1
				  WHILE (1=1)
				  BEGIN
					FETCH NEXT FROM itemcursor into @ProjectCode
					if (@@FETCH_STATUS != 0) break
					if not exists (select * from SPDSPROD.dbo.T04_GiftDetails where documentnumber = @yDocumentNumber and projectcode = @ProjectCode)
					BEGIN
						select @Exists = 0
						break
					END
				  END
				  if (@Exists = 1)
				  BEGIN
					select @ReturnDocumentNumber = @yDocumentNumber
					break
				  END
				  CLOSE itemcursor
				  DEALLOCATE itemcursor
			END
			CLOSE TransCursor
			DEALLOCATE TransCursor
    END
	--------------------------------------------------------------------------------------------------------------
	--  Try to find out of date
	--------------------------------------------------------------------------------------------------------------
	if (@ReturnDocumentNumber = 0)
	BEGIN
		DECLARE TransCursor CURSOR FOR
			select DocumentNumber from SPDSPROD.dbo.T01_TransactionMaster where accountnumber = @AccountNumber --and datepart(yy, [date]) = datepart(yy, @TransDate)
			OPEN TransCursor
			WHILE (1 = 1)
			BEGIN
				  FETCH NEXT FROM TransCursor into @yDocumentNumber
				  if (@@FETCH_STATUS != 0) break
				  DECLARE itemcursor CURSOR for
				  select Project from HOnorMemorialCards2 where CardDocumentNumber = @CardID
				  OPEN itemcursor
				  select @Exists = 1
				  WHILE (1=1)
				  BEGIN
					FETCH NEXT FROM itemcursor into @ProjectCode
					if (@@FETCH_STATUS != 0) break
					if not exists (select * from SPDSPROD.dbo.T04_GiftDetails where documentnumber = @yDocumentNumber and projectcode = @ProjectCode)
					BEGIN
						select @Exists = 0
						break
					END
				  END
				  if (@Exists = 1)
				  BEGIN
					select @ReturnDocumentNumber = @yDocumentNumber
					break
				  END
				  CLOSE itemcursor
				  DEALLOCATE itemcursor
			END
			CLOSE TransCursor
			DEALLOCATE TransCursor
    END
	CLOSE CardCursor
	DEALLOCATE CardCursor
	--------------------------------------------------------------------------------------------------------------
	--  Take any doc within date
	--------------------------------------------------------------------------------------------------------------
	if (@ReturnDocumentNumber = 0)
	BEGIN
		select @ReturnDocumentNumber = max(T01.DocumentNumber) from SPDSPROD.dbo.T01_TransactionMaster T01, SPDSPROD.dbo.T04_GiftDetails T04 where
		T01.documentnumber = T04.DocumentNumber and
		T01.AccountNumber = @AccountNumber and
		datepart(yy, [date]) = datepart(yy, @TransDate)
	END
	if (@ReturnDocumentNumber is null) select @ReturnDocumentNumber = 0
	--------------------------------------------------------------------------------------------------------------
	--  Take any doc any time.
	--------------------------------------------------------------------------------------------------------------
	if (@ReturnDocumentNumber = 0)
	BEGIN
		select @ReturnDocumentNumber = max(T01.DocumentNumber) from SPDSPROD.dbo.T01_TransactionMaster T01, SPDSPROD.dbo.T04_GiftDetails T04 where
		T01.documentnumber = T04.DocumentNumber and
		T01.AccountNumber = @AccountNumber 
	END
	if (@ReturnDocumentNumber is null) select @ReturnDocumentNumber = 0
	--------------------------------------------------------------------------------------------------------------
	--  Take any Family Doc.
	--------------------------------------------------------------------------------------------------------------
	if (@ReturnDocumentNumber = 0)
	BEGIN
		select @ReturnDocumentNumber = max(T01.DocumentNumber) from SPDSPROD.dbo.A01_AccountMaster A01, SPDSPROD.dbo.T01_TransactionMaster T01, SPDSPROD.dbo.T04_GiftDetails T04 where
		T01.documentnumber = T04.DocumentNumber and
		T01.AccountNumber = A01.AccountNumber and
		A01.FamilyID = @AccountNumber
		if (@ReturnDocumentNumber is null) select @ReturnDocumentNumber = 0
	END
	
	if (@ReturnDocumentNumber = 0)
	BEGIN
	 select  @ReturnDocumentNumber =max(T01.DocumentNumber) from SPDSPROD.dbo.A01_AccountMaster A01, SPDSPROD.dbo.T01_TransactionMaster T01, SPDSPROD.dbo.T04_GiftDetails T04 where
		T01.documentnumber = T04.DocumentNumber and
		T01.AccountNumber = A01.FamilyId and
		A01.AccountNumber = @AccountNumber
		if (@ReturnDocumentNumber is null) select @ReturnDocumentNumber = 0
	END
	return(@ReturnDocumentNumber)
end

