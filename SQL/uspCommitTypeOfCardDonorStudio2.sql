use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspCommitTypeOfCardDonorStudio2' AND type = 'P')
    DROP PROCEDURE dbo.uspCommitTypeOfCardDonorStudio2
GO
Create Procedure dbo.uspCommitTypeOfCardDonorStudio2
		@CardID int,
		@CardType char(10),
		@InMemorialOf varchar(50),
		@UserName varchar(50),
		@ReturnToSender char(1),
		@Status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
	select @status = 0
	select @ErrorMessage = 0
----------------------------------------------------------------------------------------------------
-- Update cart type.
----------------------------------------------------------------------------------------------------
	update SPDSPROD.dbo.SP02_CardMaster 
	set CardType = @CardType,
		Memorial = @InMemorialOf,
		ReturnToSender = case when @ReturnToSender = 'Y' then 1 else 0 END
--		ChangeUser = @UserName,
--		ChangeDate = getdate()
	where RecordId = @CardID
----------------------------------------------------------------------------------------------------
-- Add audit
----------------------------------------------------------------------------------------------------
	exec dbo.uspAddCardAudit
				@CardID,
				@UserName,
				'C',
				'C',
				@Status output,
				@ErrorMessage output

END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspCardTypeOfCard, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH

-- select * from SPDSPROD.dbo.SP02_CardMaster 
