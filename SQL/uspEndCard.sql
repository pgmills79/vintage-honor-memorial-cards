-- =============================================
-- Author:		Dave Sable
-- Create date: 8/13/2007
-- Description:	Give address information for
--              honor/memorial card.
--
-- 12/5/2007 - Corrected problem where end card is called twice.
-- =============================================
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspEndCard' AND type = 'P')
    DROP PROCEDURE dbo.uspEndCard
GO
CREATE PROCEDURE dbo.uspEndCard
				 @CardNumber bigint,
				 @Count int,
                 @ErrorText char(80) output
AS
BEGIN
SET NOCOUNT ON
declare @CardID int
declare @Message char(255)
declare @Done as int
declare @DocumentNumber as float
declare @ErrorFlag int
declare @CountedCards int
--
--  Init
--
select @ErrorText = ''
select @CardID = @CardNumber
select @Done = 0
if (@Count is null) set @Count = 0
--
--  If already processed, just quit.
--
if not exists (select * from SPAPPSPROD.dbo.HonorMemorialCards1Temp where CardDocumentNumber = @CardNumber) and 
	   exists (select * from SPAPPSPROD.dbo.HonorMemorialCards1 where CardDocumentNumber = @CardNumber) and
       (@Done = 0)
BEGIN
	select @ErrorText = ''
	select @Message = 'Called end routine twice'
	exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'W', @Message
	select @Done = 1
END
--
--  Check to see if transaction and account exists.
--
if not exists (select * from SPAPPSPROD.dbo.HonorMemorialCards1Temp where CardDocumentNumber = @CardNumber) and 
    (@Done = 0)
BEGIN
	select @ErrorText = 'Invalid Card number.'
	select @Message = 'Invalid Card Number'
	exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'E', @Message
	select @Done = 1
END
--
--  Make sure the count is correct.
--
select @CountedCards = count(*) from SPAPPSPROD.dbo.HonorMemorialCards2Temp where CardDocumentNumber = @CardNumber
if (@CountedCards <> @Count) and (@Done = 0)
BEGIN
	select @ErrorText = 'Number of gifts do not match.'
	select @Message = 'Number of gifts do not match.'
	exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'E', @Message
	select @Done = 1
END
--
--  Update count and status field.
--
if (@Done = 0)
BEGIN
		update SPAPPSPROD.dbo.HonorMemorialCards1Temp set NumberOfGifts = @CountedCards where CardDocumentNumber = @CardNumber
		if (@Count > 8) update SPAPPSPROD.dbo.HonorMemorialCards1Temp set Status = 'D' where CardDocumentNumber = @CardNumber
		if (@Count <= 8) update SPAPPSPROD.dbo.HonorMemorialCards1Temp set Status = 'H' where CardDocumentNumber = @CardNumber	
	    update SPAPPSPROD.dbo.HonorMemorialCards1Temp set Status = 'D' from SPAPPSPROD.dbo.HonorMemorialCards1Temp c1, SPAPPSPROD.dbo.HonorMemorialCards2Temp  c2 where
			c1.CardDocumentNumber = c2.CardDocumentNumber and
            Project not in (select Project from SPAPPSPROD.dbo.ChristmasCatalogDescriptions where
                                Project = Project) and c1.CardDocumentNumber = @CardNumber
END
--
--  Load to perminant table.
--
if (@Done = 0)
BEGIN
select @ErrorFlag = 0
BEGIN TRY
    BEGIN TRANSACTION
	-- Make sure perminant table works
	delete from SPAPPSPROD.dbo.HonorMemorialCards1 where CardDocumentNumber = @CardNumber
	delete from SPAPPSPROD.dbo.HonorMemorialCards2 where CardDocumentNumber = @CardNumber
	-- Move data from temporary to perminant.
	insert into SPAPPSPROD.dbo.HonorMemorialCards1
	(      CardDocumentNumber, AccountNumber, DocumentNumber, FromName, MemorialName, TransactionDate, LetterCode, [Status], Occurrance, PrintGroup, ToName, ToAddress1, ToAddress2, ToAddress3, ToCity, ToState, ToZip, ToCountry, NumberOfGifts, WebPageName, AddDate, ChangeUser, ChangeDate, ReturnToSender)
	select CardDocumentNumber, AccountNumber, DocumentNumber, FromName, MemorialName, TransactionDate, LetterCode, [Status], Occurrance, PrintGroup, ToName, ToAddress1, ToAddress2, ToAddress3, ToCity, ToState, ToZip, ToCountry, NumberOfGifts, WebPageName, AddDate, ChangeUser, ChangeDate, ReturnToSender 
	from SPAPPSPROD.dbo.HonorMemorialCards1Temp
		where CardDocumentNumber = @CardNumber
	insert into SPAPPSPROD.dbo.HonorMemorialCards2
	(CardDocumentNumber, Project, AddDate)
	select CardDocumentNumber, Project, AddDate from SPAPPSPROD.dbo.HonorMemorialCards2Temp
		where CardDocumentNumber = @CardNumber
	-- Delete from temporary.
	delete from SPAPPSPROD.dbo.HonorMemorialCards1Temp where CardDocumentNumber = @CardNumber
	delete from SPAPPSPROD.dbo.HonorMemorialCards2Temp where CardDocumentNumber = @CardNumber
    COMMIT TRANSACTION;
END TRY
BEGIN CATCH
    -- Rollback any active or uncommittable transactions before
    -- inserting information in the ErrorLog
    IF XACT_STATE() <> 0
    BEGIN
        ROLLBACK TRANSACTION
		select @Message = 'Rolling back transaction'
		exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'W', @Message
    END
	select @ErrorFlag = 1
END CATCH; 
END
--
--  Flag error if necessary
--
if (@ErrorFlag = 0) and (@Done = 0)
BEGIN
	select @Message = 'Card complete.'
	exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'I', @Message
END
else
BEGIN
	if (@@Error <> 0)
	BEGIN
		select @Message = 'Completion error - ' + convert(char(20), @@Error)
		exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'I', @Message
	END
END
set nocount off
END
go
