use SPAPPSPROD
go
update SPDSPROD.dbo.SP02_CardMaster set Address4 = Address4
update SPDSPROD.dbo.SP01_CardDetails set ProjectCode = ProjectCode
exec uspHonorCardSync

update dbo.HonorMemorialCards1 set DSRecordID = CardMasterRecordID  from dbo.HonorMemorialCards1 hc1, HonorMemorialSyncFromDS sync
where hc1.CardDocumentNumber = sync.cardid and
CardItemRecordID = 0 


update dbo.HonorMemorialCards2 set DSRecordID = dsi.Recordid 
from dbo.HonorMemorialCards1 s1, SPDSPROD.dbo.SP02_CardMaster dsc,
			  dbo.HonorMemorialCards2 s2, SPDSPROD.dbo.SP01_CardDetails dsi where
dsc.RecordId = s1.DSRecordID and
s1.CardDocumentNumber = s2.CardDocumentNumber and
dsc.RecordId = dsi.CardRecordId and
s2.Project = dsi.ProjectCode  
---------------------------------------------
--
--
--  Turn off triggers here
--
--
---------------------------------------------
--
-- Remove those with invalid document numbers.
--
delete from dbo.HonorMemorialCards1 where AccountNumber = 0

delete dbo.HonorMemorialCards2 from dbo.HonorMemorialCards2 h2 where CardDocumentNumber not in
(Select CardDocumentNumber from dbo.HonorMemorialCards1  h1 where h1.CardDocumentNumber = h2.CardDocumentNumber)

update dbo.HonorMemorialCards1 set DocumentNumber = 0  from dbo.HonorMemorialCards1 HC1 where DocumentNumber not in
(Select DocumentNumber from SPDSPROD.dbo.T01_TransactionMaster T01 where T01.documentnumber = HC1.DocumentNumber) and
DocumentNumber > 0
update  dbo.HonorMemorialCards1 set status = 'X' where AccountNumber = 0 and DocumentNumber = 0
--
--  Get the latest Account Number
--
update dbo.HonorMemorialCards1 set AccountNumber = A11.AccountNumber from SPDSPROD.dbo.A11_AccountMergeHeaders A11, dbo.HonorMemorialCards1 HC1 where
A11.MergedAccountNumber = HC1.AccountNumber
--
-- Make sure family account
--
Update HonorMemorialCards1 set AccountNumber = FamilyId from HonorMemorialCards1 h1, SPDSPROD.dbo.A01_AccountMaster A01 where
A01.AccountNumber = h1.AccountNumber and
A01.FamilyId <> A01.AccountNumber and
A01.FamilyId > 0
--
-- Find a matching document number.
--
Update HonorMemorialCards1 set DocumentNumber =  dbo.usfMatchCardWithTransaction(CardDocumentNumber, AccountNumber, TransactionDate) 
--
--  Import into Donor Studio - cards
--
declare @CardID bigint
declare @RecordID bigint
declare @Status int
declare @ErrorMessage varchar(255)
DECLARE CardCursor CURSOR FOR
select CardDocumentNumber from HonorMemorialCards1 where documentnumber > 0 and dsrecordid is null
OPEN CardCursor
WHILE (1 = 1)
BEGIN
      FETCH NEXT FROM CardCursor into @CardID
      if (@@FETCH_STATUS != 0) break
		BEGIN TRANSACTION
		INSERT INTO [SPDSPROD].[dbo].[SP02_CardMaster]
			   ([DocumentNumber]
			   ,[AccountNumber]
			   ,[CardType]
			   ,[FromName]
			   ,[ToName]
			   ,[Memorial]
			   ,[Address1]
			   ,[Address2]
			   ,[Address3]
			   ,[Address4]
			   ,[City]
			   ,[State]
			   ,[ZipPostal]
			   ,[Country]
			   ,[CardStatus]
			   ,[PrintStatus]
			   ,[PrintDate]
			   ,[ReprintDate]
			   ,[PrintGroup]
			   ,[LetterType]
			   ,[ReturnToSender])
		 select 
			   DocumentNumber,
			   AccountNumber,
			   LetterCode,
			   FromName,
			   ToName,
			   MemorialName, 
			   ToAddress1, 
			   ToAddress2, 
			   ToAddress3,
			   '', 
			   ToCity, 
			   ToState, 
			   ToZip,
			   ToCountry, 
			   Null,
			   [Status],
			   Null,
			   Null,
			   Null,
			   Null,
			   case when ReturnToSender = 'Y' then 1 else 0 end
		from HonorMemorialCards1 where CardDocumentNumber = @CardID
		select @RecordID = SCOPE_IDENTITY()
		update HonorMemorialCards1 set DSRecordID = @RecordID where CardDocumentNumber = @CardID
		COMMIT TRANSACTION
		print 'Record ID = ' + convert(varchar(10), @RecordID)
		exec dbo.uspAddCardAudit
				@RecordID,
				'IMPORT',
				'C',
				'A',
				@Status output,
				@ErrorMessage output
END
CLOSE CardCursor
DEALLOCATE CardCursor
--
--  Import into Donor Studio - items
--
declare @CardID2 bigint
declare @RecordID2 bigint
declare @Status2 int
declare @ErrorMessage2 varchar(255)
declare @CardRecordID bigint
declare @Project varchar(10)
DECLARE CardCursor CURSOR FOR
select h2.CardDocumentNumber, h2.Project, h1.dsrecordid from HonorMemorialCards2 h2, HonorMemorialCards1 h1 where 
h1.documentnumber > 0 and h2.dsrecordid is null and h1.dsrecordid  is not null and
h1.CardDocumentNumber = h2.CardDocumentNumber
OPEN CardCursor
WHILE (1 = 1)
BEGIN
      FETCH NEXT FROM CardCursor into @CardID2, @Project, @CardRecordID
      if (@@FETCH_STATUS != 0) break
		BEGIN TRANSACTION
			INSERT INTO [SPDSPROD].[dbo].[SP01_CardDetails]
				([CardRecordId]
				,[ProjectCode])
			VALUES
				(@CardRecordID,
				@Project)
		select @RecordID2 = SCOPE_IDENTITY()
		
		update HonorMemorialCards2 set DSRecordID = @RecordID2 where CardDocumentNumber = @CardID2 and Project = @Project
		COMMIT TRANSACTION
		print 'Record ID = ' + convert(varchar(10), @RecordID2)
		exec dbo.uspAddCardAudit
				@RecordID2,
				'IMPORT',
				'I',
				'A',
				@Status2 output,
				@ErrorMessage2 output
END
CLOSE CardCursor
DEALLOCATE CardCursor
--------------------------------------------------------------------------------------------------------------------------
--
--
--
--	Make HonorMemorialCards1 an identity column.
--
--
--
--------------------------------------------------------------------------------------------------------------------------
select * from HonorMemorialCards1 where FromName like '%Sable%'