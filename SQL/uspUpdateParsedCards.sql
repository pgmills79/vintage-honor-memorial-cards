IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspUpdateParsedCards' AND type = 'P')
    DROP PROCEDURE dbo.uspUpdateParsedCards
GO
Create Procedure dbo.uspUpdateParsedCards
		@CardID int,
		@PrintGroup int,
		@StdAddress1 varchar(70),
		@StdAddress2 varchar(70),
		@StdAddress3 varchar(70),
		@StdCity     varchar(40),
		@StdState varchar(10),
		@StdZip varchar(15),
		@StdCountry varchar(50),
		@ACE_ERROR varchar(10),
		@status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
declare @icount int

select @status = 0
select @ErrorMessage = ''
if (@StdAddress1  is null) select @StdAddress1  = ''
if (@StdAddress2 is null) select @StdAddress2 = ''
if (@StdAddress3 is null) select @StdAddress3 = ''
if (@StdCity is null) select @StdCity = ''
if (@StdState is null) select @StdState = ''
if (@StdZip is null) select @StdZip = ''
if (@StdCountry is null) select @StdCountry = ''
if (@ACE_ERROR  is null) select @ACE_ERROR  = ''

--
-- Move address to top
--
select @icount = 3
while (@icount > 0)
BEGIN
	if (@StdAddress1 = '')
	BEGIN
		select @StdAddress1 = @StdAddress2
		select @StdAddress2 = @StdAddress3
		select @StdAddress3 = ''
	END		
	if (@StdAddress2 = '')
	BEGIN
		select @StdAddress2 = @StdAddress3
		select @StdAddress3 = ''
	END		
	select @icount = @icount - 1
END

--
--  Fill in with original address if failed to standardize.
--
if ((@StdAddress1 = '') or (@StdZip = ''))
BEGIN
	select	@StdAddress1 = ToAddress1,
			@StdAddress2 = ToAddress2,
			@StdAddress3 = ToAddress3,
			@StdCity = ToCity,
			@StdState = ToState,
			@StdZip = ToZip,
			@StdCountry = ToCountry
            from HonorCardFormatting
			where CardID = @CardID and PrintGroup = @PrintGroup
END
--
-- Update
--
update HonorCardFormatting set 
				StdAddress1 = left(rtrim(@StdAddress1), 70),
				StdAddress2 = left(rtrim(@StdAddress2), 70),
				StdAddress3 = left(rtrim(@StdAddress3), 70) ,
				StdCity = left(rtrim(@StdCity), 50),   
				StdState = left(rtrim(@StdState), 50),
				StdZip = left(rtrim(@StdZip), 50),
				StdCountry = left(rtrim(@StdCountry), 50),
				StdError = left(rtrim(@ACE_ERROR), 5)  
	where CardID = @CardID and PrintGroup = @PrintGroup
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspUpdateParsedCards, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255)
END CATCH
