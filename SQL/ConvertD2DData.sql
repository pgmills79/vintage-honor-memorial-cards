--delete from [HonorMemorialCards1]
--delete from [HonorMemorialCards2]
INSERT INTO [SPAPPSPROD].[dbo].[HonorMemorialCards1]
           ([CardDocumentNumber]
           ,[AccountNumber]
           ,[DocumentNumber]
           ,[FromName]
           ,[MemorialName]
           ,[TransactionDate]
           ,[LetterCode]
           ,[Status]
           ,[Occurrance]
           ,[PrintGroup]
           ,[ToName]
           ,[ToAddress1]
           ,[ToAddress2]
           ,[ToAddress3]
           ,[ToCity]
           ,[ToState]
           ,[ToZip]
           ,[ToCountry]
           ,[NumberOfGifts]
           ,[WebPageName]
           ,[AddDate]
           ,[ChangeUser]
           ,[ChangeDate]
           ,[ReturnToSender])
select 
C155HMDCNO, C1Z103ACNO, C1Z103DCNO, C155CCQFM, C155CCQMN, convert(datetime, dbo.udfConvJdeDateToChar(C1Z103TRDT)), 
C1Z103LTR, C1Z103STAT, C1Z103OCCR, C1Z103PRGP, C155CCQTO, C1Z103ADR1, 
C1Z103ADR2, C1Z103ADR3, C1Z103CITY, C1Z103ST, C1Z103ZIP, C1Z103CNTY, 
C1Z103GFNO, C155WPN, dbo.udfConvJdeDateAndTimeToDatetime (C1Z103ADDT, C1Z103ADTM), C1Z103CHUS, 
dbo.udfConvJdeDateAndTimeToDatetime (C1Z103CHDT, C1Z103CHTM), C155CCRTC
from f55hmc1 

update HonorMemorialCards1 set FromName= Null where FromName = ''
update HonorMemorialCards1 set MemorialName= Null where MemorialName = ''
update HonorMemorialCards1 set ToName= Null where ToName = ''
update HonorMemorialCards1 set ToAddress1= Null where ToAddress1 = ''
update HonorMemorialCards1 set ToAddress2= Null where ToAddress2 = ''
update HonorMemorialCards1 set ToAddress3= Null where ToAddress3 = ''
update HonorMemorialCards1 set ToCity= Null where ToCity = ''
update HonorMemorialCards1 set ToState= Null where ToState = ''
update HonorMemorialCards1 set ToZip= Null where ToZip = ''
update HonorMemorialCards1 set ToCountry= Null where ToCountry = ''
update HonorMemorialCards1 set WebPageName= Null where WebPageName = ''
update HonorMemorialCards1 set ChangeUser= Null where ChangeUser = ''
update HonorMemorialCards1 set ChangeDate= Null where ChangeDate < '01/01/1900 00:00'

INSERT INTO [SPAPPSPROD].[dbo].[HonorMemorialCards2]
           ([CardDocumentNumber]
           ,[Project]
           ,[AddDate])
select C255HMDCNO, C2Z103PROJ, dbo.udfConvJdeDateAndTimeToDatetime (C2Z103ADDT, C2Z103ADTM) from f55hmc2


INSERT INTO [SPAPPSPROD].[dbo].[ChristmasCatalogDescriptions]
           ([Project]
           ,[Title]
           ,[Body])
select CCZ103PROJ, CC55CCDESC, CC55CCDES2  from f55ccdes where ccz103proj <> ''

Update SPDSPROD.dbo.X31_NextNumberBusinessData set [Number] = (select max(carddocumentnumber)+1 from honormemorialcards1)
where [Type] = 'SP_HMCARD'


drop table f55hmc1
drop table f55hmc2
drop table f55ccdes