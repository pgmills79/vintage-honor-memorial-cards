use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspCommitGiftCardProjectDonorStudio' AND type = 'P')
    DROP PROCEDURE dbo.uspCommitGiftCardProjectDonorStudio
GO
Create Procedure dbo.uspCommitGiftCardProjectDonorStudio
		@CardID int,
		@Guid varchar(200),
		@GiftLine varchar(2048) output,
		@Status int output,
		@ErrorMessage varchar(256) output
as
set nocount on
BEGIN TRY
	select @GiftLine = ''
	select @Status = 0
	select @ErrorMessage = ''
	declare @CardID2 bigint
	declare @Project varchar(10)
	declare @RecordID bigint
	
--------------------------------------------------------------------------------------------------------------
--  Add each item.
--------------------------------------------------------------------------------------------------------------

	DECLARE ItemCursor CURSOR FOR
	select CardID, Project from SPAPPSPROD.dbo.HonorMemorialItemTemp t where
				   [Guid] = @Guid and
				   [CardID] = @Cardid and
				   [Project] not in (select ProjectCode from SPDSPROD.dbo.SP01_CardDetails HMC2 where
                                     HMC2.CardRecordId = t.CardID and
                                     HMC2.ProjectCode = t.Project)
	OPEN ItemCursor
	WHILE (1 = 1)
	BEGIN
		  FETCH NEXT FROM ItemCursor into @CardID2, @Project
		  if (@@FETCH_STATUS != 0) break
		  insert into SPDSPROD.dbo.SP01_CardDetails
		  (CardRecordId, ProjectCode)
		  values
		  (@CardID2, @Project)
		  select @RecordID = SCOPE_IDENTITY()
		  exec dbo.uspAddCardAudit  @RecordID,
									'HMVIEW',
									'I',
									'A',
									@Status output,
									@ErrorMessage output

	END
	CLOSE ItemCursor
	DEALLOCATE ItemCursor
--------------------------------------------------------------------------------------------------------------
--  Remove auditing record.
--------------------------------------------------------------------------------------------------------------
	delete SPDSPROD.dbo.SP01_CardDetails from SPDSPROD.dbo.SP01_CardDetails HMC2,
	       SPDSPROD.dbo.X30_AuditInformation X30
			where ProjectCode not in
				(select [Project] from SPAPPSPROD.dbo.HonorMemorialItemTemp t where
						HMC2.CardRecordId = t.CardID and
                        HMC2.ProjectCode = t.Project and
						t.[Guid] = @Guid and
						t.[CardID] = @Cardid) and
						CardRecordId = @CardID and
			X30.TableId = 'SP01' and
			X30.TableRecordId = HMC2.Recordid
--------------------------------------------------------------------------------------------------------------
--  Remove missing items.
--------------------------------------------------------------------------------------------------------------
	delete SPDSPROD.dbo.SP01_CardDetails from SPDSPROD.dbo.SP01_CardDetails HMC2 where ProjectCode not in
				(select [Project] from SPAPPSPROD.dbo.HonorMemorialItemTemp t where
						HMC2.CardRecordId = t.CardID and
                        HMC2.ProjectCode = t.Project and
						t.[Guid] = @Guid and
						t.[CardID] = @Cardid) and
			CardRecordId = @CardID

	select @GiftLine = SPAPPSPROD.dbo.usfGetCardGiftsDonorStudio(@CardID)
	delete from SPAPPSPROD.dbo.HonorMemorialItemTemp where [Guid] = @Guid
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspCommitGiftCardProjectDonorStudio, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH
