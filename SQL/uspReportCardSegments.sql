use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspReportCardSegments' AND type = 'P')
    DROP PROCEDURE dbo.uspReportCardSegments
GO
Create Procedure dbo.uspReportCardSegments
		@PrintGroup int
as
set nocount on
BEGIN TRY
select [FileName] + space(80) as FileName, count(*) as NumberInFile
	   into #x from dbo.HonorCardFormatting where PrintGroup = @PrintGroup 
	   group by [FileName]
	   order by [FileName]
update #x set FileName = rtrim(ltrim(FileName))

insert into #x values ('01-Honor-Not306-NotReturn-3OrUnder', 0)
insert into #x values ('02-Honor-Not306-NotReturn-Over3', 0)
insert into #x values ('03-Honor-Not306-Return-3OrUnder', 0)
insert into #x values ('04-Honor-Not306-Return-Over3', 0)
insert into #x values ('05-Honor-306-NotReturn-3OrUnder', 0)
insert into #x values ('06-Honor-306-NotReturn-Over3', 0)
insert into #x values ('07-Honor-306-Return-3OrUnder', 0)
insert into #x values ('08-Honor-306-Return-Over3', 0)
insert into #x values ('09-Memorial-Not306-NotReturn-3OrUnder', 0)
insert into #x values ('10-Memorial-Not306-NotReturn-Over3', 0)
insert into #x values ('11-Memorial-Not306-Return-3OrUnder', 0)
insert into #x values ('12-Memorial-Not306-Return-Over3', 0)
insert into #x values ('13-Memorial-306-NotReturn-3OrUnder', 0)
insert into #x values ('14-Memorial-306-NotReturn-Over3', 0)
insert into #x values ('15-Memorial-306-Return-3OrUnder', 0)
insert into #x values ('16-Memorial-306-Return-Over3', 0)

select [FileName] + case 
                    when (replace([FileName], '-Return-', '') <> [FileName]) then ' ---Label'
                    else ''
                    end as FileName,
sum(NumberInFile) as NumberInFile from #x
group by [FileName] + case 
                    when (replace([FileName], '-Return-', '') <> [FileName]) then ' ---Label'
                    else ''
                    end
order by [FileName] + case 
                    when (replace([FileName], '-Return-', '') <> [FileName]) then ' ---Label'
                    else ''
                    end
END TRY
BEGIN CATCH
	select 'Error' as FileName, 0 as NumberInFile

END CATCH
