IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspReportLabelToCard' AND type = 'P')
    DROP PROCEDURE dbo.uspReportLabelToCard
GO
Create Procedure dbo.uspReportLabelToCard
		@PrintGroup int
as
set nocount on
--BEGIN TRY
--
--select distinct 0 as 'RowNumber', AccountNumber, [Type],
--		[FileName],   dbo.udfFormatAccountName(AccountNumber, 'ALL')  as SenderName, + replace(dbo.udfFormatAccountPrimeAddr(AccountNumber, 'ALL'), 'USA, USA,', 'USA') as SenderAddress, 
--	   'From ' + rtrim(FromName) + ' to ' + rtrim(ToName) as Receiver, 0 as RowColor
--	   from dbo.HonorCardFormatting where PrintGroup = @PrintGroup and [Type] in (3, 4, 7, 8, 11, 12, 15, 16)
--		order by [Type], accountnumber
--
--END TRY
--BEGIN CATCH
--	select 0 as AccountNumber, 0 as Type, '' as FileName, 'Error' as Sender, 'Error' as Receiver
--
--END CATCH
BEGIN TRY
select distinct ROW_NUMBER() OVER(ORDER BY [Type], accountnumber) AS 'RowNumber', AccountNumber, [Type],
		[FileName],   dbo.udfFormatAccountName(AccountNumber, 'ALL')  as SenderName, + replace(replace(dbo.udfFormatAccountPrimeAddr(AccountNumber, 'ALL'), ', United States of America', ''), ', USA,', '') as SenderAddress, 
	   'From ' + rtrim(FromName) + ' to ' + rtrim(ToName) as Receiver, 0 as RowColor
	   into #a from dbo.HonorCardFormatting where PrintGroup = @PrintGroup and [Type] in (3, 4, 7, 8, 11, 12, 15, 16)
		order by [Type], accountnumber

declare @RowNumber int
declare @AccountNumber int
declare @RowColor int
declare @LastAccountNumber int
select @LastAccountNumber = 0
select @RowColor = 1
DECLARE a_cursor CURSOR FAST_FORWARD FOR
select RowNumber, AccountNumber from #a order by rownumber
OPEN a_cursor
WHILE (1=1)
BEGIN
	FETCH NEXT FROM a_cursor into @RowNumber, @AccountNumber
	if (@@FETCH_STATUS != 0) break
	if (@AccountNumber != @LastAccountNumber)
	BEGIN
		if (@RowColor = 1) 
			select @RowColor = 0
		else 
			select @RowColor = 1
	END
	update #a set RowColor = @RowColor where RowNumber = @RowNumber
	select @LastAccountNumber = @AccountNumber
END
CLOSE a_cursor
DEALLOCATE a_cursor
select * from #a order by rownumber
END TRY
BEGIN CATCH
	select 0 as RowNumber, 0 as AccountNumber, 0 as Type, '' as FileName, 'Error' as Sender, 'Error' as Receiver, 0 as RowColor

END CATCH


