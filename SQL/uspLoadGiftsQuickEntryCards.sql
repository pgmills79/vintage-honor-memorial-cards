-- =============================================
-- Author:		Dave Sable
-- Create date: 9/27/2007
-- Description:	Load one card into print table.
-- =============================================
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspLoadGiftsQuickEntryCards' AND type = 'P')
    DROP PROCEDURE dbo.uspLoadGiftsQuickEntryCards
GO
CREATE PROCEDURE dbo.uspLoadGiftsQuickEntryCards
				 @DonationNumber float
AS
BEGIN
set nocount on
declare @CardNumber int

DECLARE gift_cursor CURSOR FOR
select cd55crdno from proddta.f55de03a where cd55ccqdn = @DonationNumber

OPEN gift_cursor 

-- Perform the first fetch.
FETCH NEXT FROM gift_cursor into @CardNumber

-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
WHILE @@FETCH_STATUS = 0
BEGIN
   exec SPAPPSPROD.dbo.uspLoadQuickEntryCard @CardNumber
   FETCH NEXT FROM gift_cursor into @CardNumber
END

CLOSE gift_cursor
DEALLOCATE gift_cursor



set nocount off
END

