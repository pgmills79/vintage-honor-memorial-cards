-- =============================================
-- Author:		Dave Sable
-- Create date: 8/13/2007
-- Description:	Give address information for
--              honor/memorial card.
-- =============================================
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspStartCardVersion2' AND type = 'P')
    DROP PROCEDURE dbo.uspStartCardVersion2
GO
CREATE PROCEDURE dbo.uspStartCardVersion2
				 @DocumentNumber float,
				 @FromName char(50),
			     @LetterCode char(10),
				 @ToName char(50),
				 @DeceasedName char(50),
                 @AddressLine1 char(70),
                 @AddressLine2 char(70),
                 @AddressLine3 char(70),
                 @City char(40),
                 @State char(10),
                 @Zip char(15),
			     @ReturnToSender char(1),
                 @ScreenName char(30),
                 @CardNumber float output,
                 @ErrorText char(80) output
AS
BEGIN
SET NOCOUNT ON
declare @CardID int
declare @Occurrance int
declare @AccountID float
declare @DeterminedFromName char(70)
declare @Message char(255)
--
--  Init
--
if @FromName is null select @FromName = ''
if @LetterCode is null select @LetterCode = ''
if @ToName is null select @ToName = ''
if @DeceasedName is null select @DeceasedName = ''
if @AddressLine1 is null select @AddressLine1 = ''
if @AddressLine2 is null select @AddressLine2 = ''
if @AddressLine3 is null select @AddressLine3 = ''
if @City is null select @City = ''
if @State is null select @State = ''
if @Zip is null select @Zip = ''
if @ScreenName is null select @ScreenName = ''
select @ErrorText = ''
select @CardNumber = 0
--
--  Get account ID and formatted name.
--
select @AccountID = T01.AccountNumber,
	   @DeterminedFromName = case A01.AccountType
							 when 'O' then rtrim(A01.OrganizationName)
                             when 'F'then rtrim(A01.OrganizationName)
                             else rtrim(A01.Title) + ' ' + rtrim(A01.FirstName) + ' ' + rtrim(A01.MiddleName) + ' ' + rtrim(A01.LastName) + ' ' + rtrim(A01.Suffix) end
from SPDSPROD.dbo.T01_TransactionMaster T01, SPDSPROD.dbo.A01_AccountMaster A01 where
A01.AccountNumber = T01.AccountNumber and
T01.DocumentNumber = @DocumentNumber
--***********************
-- Post Trustcommerce, we can no longer expect account to exist when card is placed in system.
--***********************
--
--  Check to see if transaction and account exists.
--
----if (@@RowCount = 0)
----BEGIN
----	select @ErrorText = 'Transaction or account does not exist.'
----	select @Message = 'Transaction or account does not exist: '  + substring(rtrim(ltrim(convert(char(80), convert(int, @DocumentNumber)))), 1, 10) +
----					'|' + rtrim(ltrim(@FromName)) +
----					'|' + rtrim(ltrim(@LetterCode)) +
----					'|' + rtrim(ltrim(@ToName)) +
----					'|' + rtrim(ltrim(@DeceasedName)) +
----					'|' + rtrim(ltrim(@AddressLine1)) +
----					'|' + rtrim(ltrim(@AddressLine2)) +
----					'|' + rtrim(ltrim(@AddressLine3)) +
----					'|' + rtrim(ltrim(@City)) +
----					'|' + rtrim(ltrim(@State)) +
----					'|' + rtrim(ltrim(@Zip)) +
----					'|' + rtrim(ltrim(@ScreenName))
----
----	exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'W', @Message
----END
--ELSE
--BEGIN
	if @LetterCode not in ('HCC', 'MCC')
	BEGIN
		select @ErrorText = substring('Invalid letter code - ' + @LetterCode, 1, 80)
		select @Message = 'Invalid letter code - ' + rtrim(@LetterCode) +
					'|' + rtrim(ltrim(@FromName)) +
					'|' + rtrim(ltrim(@LetterCode)) +
					'|' + rtrim(ltrim(@ToName)) +
					'|' + rtrim(ltrim(@DeceasedName)) +
					'|' + rtrim(ltrim(@AddressLine1)) +
					'|' + rtrim(ltrim(@AddressLine2)) +
					'|' + rtrim(ltrim(@AddressLine3)) +
					'|' + rtrim(ltrim(@City)) +
					'|' + rtrim(ltrim(@State)) +
					'|' + rtrim(ltrim(@Zip)) +
					'|' + rtrim(ltrim(@ScreenName))

		exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'E', @Message
	END
	else
	BEGIN
		--
		-- Choose from name to use.
		--
		if (@FromName = '') 
		BEGIN
			select @DeterminedFromName = replace(@DeterminedFromName, '  ', ' ')
			select @DeterminedFromName = replace(@DeterminedFromName, '  ', ' ')
			select @FromName  = replace(@DeterminedFromName, '  ', ' ')
		END
		--
		-- Get next card ID
		--
		exec  uspGetNextHonorCardNumber @CardID OUTPUT
		select @CardNumber = @CardID
		--
		-- Figure occurrance
		--
		set @Occurrance = (@CardID % 5) + 1
		--
		--  Add to temp table.
		--
		insert into SPAPPSPROD.dbo.HonorMemorialCards1Temp
		(CardDocumentNumber, AccountNumber, DocumentNumber, FromName, MemorialName, TransactionDate, LetterCode, Status, Occurrance, PrintGroup, ToName, ToAddress1, ToAddress2, ToAddress3, ToCity, ToState, ToZip, ToCountry, NumberOfGifts, WebPageName, AddDate, ChangeUser, ChangeDate, ReturnToSender)
		values
		(@CardID, @AccountID, @DocumentNumber, @FromName, @DeceasedName, getdate(), @LetterCode, '?', @Occurrance, 0, @ToName, @AddressLine1, @AddressLine2, @AddressLine3, @City, @State, @Zip, 'USA', 0, @ScreenName, getdate(), '', Null, @ReturnToSender)
		select @Message = 'Added card information.' +		
					'|' + rtrim(ltrim(@FromName)) +
					'|' + rtrim(ltrim(@LetterCode)) +
					'|' + rtrim(ltrim(@ToName)) +
					'|' + rtrim(ltrim(@DeceasedName)) +
					'|' + rtrim(ltrim(@AddressLine1)) +
					'|' + rtrim(ltrim(@AddressLine2)) +
					'|' + rtrim(ltrim(@AddressLine3)) +
					'|' + rtrim(ltrim(@City)) +
					'|' + rtrim(ltrim(@State)) +
					'|' + rtrim(ltrim(@Zip)) +
					'|' + rtrim(ltrim(@ScreenName))
		exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'I',@Message
	END
--END
set nocount off
END
go
