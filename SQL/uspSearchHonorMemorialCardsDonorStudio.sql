use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspSearchHonorMemorialCardsDonorStudio' AND type = 'P')
    DROP PROCEDURE dbo.uspSearchHonorMemorialCardsDonorStudio
GO
Create Procedure dbo.uspSearchHonorMemorialCardsDonorStudio
		@AccountNumber bigint,
		@BeginDate datetime,
		@EndDate datetime,
		@PrintStatus char(1),
		@Group int,
		@Of int,
		@FromName varchar(20),
		@ToName varchar(20),
		@MemorialName varchar(20),
		@Address1 varchar(20),
		@City varchar(20),
		@Zip varchar(10),
		@RowsReturned int output,
		@MaxLimitWarning int output,
		@Status int output,
		@ErrorMessage varchar(3000) output
as
set nocount on
BEGIN TRY
declare @iCount int
declare @SQL varchar(4096)
declare @Fields varchar(4096)
declare @MaxToReturn int
select @MaxToReturn = 1000  -- <-- Change here for maximum number of rows returned.
select @SQL = ''
select @status = 0
select @ErrorMessage = ''
select @RowsReturned = 0
select @MaxLimitWarning = 0
SET @FromName = REPLACE(@FromName, '''', '''''')
SET @ToName = REPLACE(@ToName, '''', '''''')
SET @MemorialName = REPLACE(@MemorialName, '''', '''''')
SET @Address1 = REPLACE(@Address1, '''', '''''')
SET @City = REPLACE(@City, '''', '''''')
SET @Zip = REPLACE(@Zip, '''', '''''')
--
-- Make sure account number is set correctly
--
update SPDSPROD.dbo.SP02_CardMaster set accountnumber = T01.AccountNumber  
from SPDSPROD.dbo.SP02_CardMaster HC1, SPDSPROD.dbo.T01_TransactionMaster T01
where T01.DocumentNumber = HC1.DocumentNumber and
HC1.AccountNumber is null
--
-- Make sure accounts are accurate.
--
update SPDSPROD.dbo.SP02_CardMaster set accountnumber = A11.AccountNumber from SPDSPROD.dbo.SP02_CardMaster HC1, SPDSPROD.dbo.A11_AccountMergeHeaders A11
where A11.mergedaccountnumber = HC1.AccountNumber
--
-- Make sure all is loaded.
--
--exec dbo.uspLoadHonorMemorialCards	@status output,	@ErrorMessage output
--if (@status <> 0) return
--
if @AccountNumber is null set @AccountNumber = 0
if @BeginDate is null set @BeginDate = '01/01/1900 00:00'
if @EndDate is null set @EndDate = getdate()
select @BeginDate = SPAPPSPROD.dbo.udfMinDateTime(@BeginDate)
select @EndDate = SPAPPSPROD.dbo.udfMaxDateTime(@EndDate)
if @PrintStatus is null set @PrintStatus = ''
if @Group is null set @Group = 0
if @Of is null set @Of = 0
if @FromName is null set @FromName = ''
if @ToName is null set @ToName = ''
if @PrintStatus is null set @PrintStatus = ''
if @Group is null set @Group = 0
if @Of is null set @Of = 0
if @FromName is null set @FromName = ''
if @ToName is null set @ToName = ''
if @MemorialName is null set @MemorialName = ''
if @Address1 is null set @Address1 = ''
if @City is null set @City = ''
if @Zip is null set @Zip = ''
set @PrintStatus = rtrim(ltrim(@PrintStatus))
set @FromName = replace(rtrim(ltrim(@FromName)), '*', '%')
set @ToName = replace(rtrim(ltrim(@ToName)), '*', '%')
set @MemorialName = replace(rtrim(ltrim(@MemorialName)), '*', '%')
set @Address1 = replace(rtrim(ltrim(@Address1)), '*', '%')
set @City = replace(rtrim(ltrim(@City)), '*', '%')
set @Zip = replace(rtrim(ltrim(@Zip)), '*', '%')
--
-- Fields
--
select @Fields = 
	'SP02.RecordId as CardDocumentNumber, ' +
	'SP02.AccountNumber, ' +
	'SPAPPSPROD.[dbo].[udfFormatAccountName] (SP02.AccountNumber, ''ALL'') as AccountName, ' + 
	'isnull(SP02.DocumentNumber, 0) as DocumentNumber, ' +
	'rtrim(isnull(SP02.FromName, '''')) as FromName, ' +
	'rtrim(isnull(SP02.Memorial, '''')) as MemorialName, ' +
	'convert(varchar(20), T01.Date, 101) as TransactionDate, ' +
	'SP02.CardType as LetterCode , ' +
	'case when SP02.PrintStatus=''X'' then ''Delete'' 
	      when SP02.PrintStatus = ''D'' then ''Donor Ministry'' 
	      when SP02.PrintStatus = ''I'' then ''Entry In Progress''
	      when SP02.PrintStatus = ''H'' then ''Hold''  
	      when SP02.PrintStatus =''N'' then ''No Print'' 
	      when SP02.PrintStatus = ''Y'' then ''Ready'' 
	      when SP02.PrintStatus = ''P'' then ''Printed'' 
		  when SP02.PrintStatus = ''T'' then ''Test'' 
	      else SP02.PrintStatus end as status, ' +
	'PrintGroup, ' +
	'rtrim(isnull(SP02.ToName, '''')) as ToName, ' +
	'rtrim(isnull(SP02.Address1, '''')) as ToAddress1, ' +
	'rtrim(isnull(SP02.Address2, '''')) as ToAddress2, ' +
	'rtrim(isnull(SP02.Address3, '''')) as ToAddress3, ' +
	'rtrim(isnull(SP02.City, '''')) as ToCity, ' +
	'rtrim(isnull(SP02.State, '''')) as ToState, ' +
	'rtrim(isnull(SP02.ZipPostal, '''')) as ToZip, ' +
	'rtrim(isnull(SP02.Country, '''')) as ToCountry, ' +
	'0 as NumberOfGifts, ' +
	'''DonorStudio'' as WebPageName, ' +
	'dbo.usfGetCardDate(SP02.RecordId, ''C'', ''A'') as AddDate, ' +
	'dbo.usfGetCardUser(SP02.RecordId, ''C'', ''C'') as ChangeUser, ' +
	'dbo.usfGetCardDate(SP02.RecordId, ''C'', ''C'') as ChangeDate, ' +
	'case when ReturnToSender = 1  then ''Yes'' when ReturnToSender = 0 then ''No'' else ''No'' end as ReturnToSender, ' +
    'dbo.usfGetCardGiftsDonorStudio(SP02.RecordID) as Gifts, ' +
    'dbo.usfGetCardUser(SP02.RecordId, ''C'', ''A'') as AddUser, ' +
	''''' as NewStatus '
--
-- Main query
--
set @SQL = 'select top ' + convert(varchar(10), @MaxToReturn) + ' ' + @Fields + ' from SPDSPROD.dbo.SP02_CardMaster SP02, SPDSPROD.dbo.T01_TransactionMaster T01 where '
set @SQL = @SQL + ' SP02.documentnumber = T01.documentNumber '
--
-- Transaction Date
--
set @SQL = @SQL + ' and T01.Date between ''' + convert(varchar(20), @BeginDate) + ''' and ''' + convert(varchar(20), @EndDate) + ''' '
--
-- Do not pick up deleted items
--
set @SQL = @SQL + 'and SP02.PrintStatus <> ''Z'''
--
-- Account ID
--
if (@AccountNumber > 0)
	set @SQL = @SQL + 'and SP02.AccountNumber = ' + rtrim(ltrim(convert(varchar(15), @AccountNumber)))
--
-- Print Status
--
if ((@PrintStatus <> '') and (@PrintStatus <> 'A'))
	set @SQL = @SQL + 'and SP02.PrintStatus = ''' + @PrintStatus + ''' '
--
-- Filter
--
if ((@Group > 0) and (@Of > 0))
	set @SQL = @SQL + ' and (SP02.RecordID % ' + convert(varchar(10), @Group) + ') = ' + convert(varchar(10), @Of) + ' '
--
-- Name address like fields.
--
if (@FromName <> '')
	set @SQL = @SQL + ' and SP02.FromName like ''%' + @FromName + '%'' '
if (@ToName <> '')
	set @SQL = @SQL + ' and SP02.ToName like ''%' + @ToName + '%'' '
if (@MemorialName <> '')
	set @SQL = @SQL + ' and SP02.Memorial like ''%' + @MemorialName + '%'' '
if (@Address1 <> '')
	set @SQL = @SQL + ' and SP02.Address1 like ''%' + @Address1 + '%'' '
if (@City <> '')
	set @SQL = @SQL + ' and SP02.City like ''%' + @City + '%'' '
if (@Zip <> '')
	set @SQL = @SQL + ' and SP02.ZipPostal like ''%' + @Zip + '%'' '
--
-- Order
--
select @SQL = @SQL + 'order by T01.Date, SP02.AccountNumber, SP02.RecordID '

--INSERT INTO davetest (message)
--VALUES (left(@SQL,4096))
--SELECT * FROM davetest
exec (@SQL)
select @RowsReturned = @@rowcount
select @ErrorMessage = @SQL
if (@RowsReturned >= @MaxToReturn) select @MaxLimitWarning = 1
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspSearchHonorMemorialCardsDonorStudio, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) + ' ' + @SQL
	
END CATCH

-- select * from SPAPPSPROD.dbo.HonorMemorialCards1 
