﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyCode;

namespace ViewHonorMemorialCards
{
    /// <summary>
    /// Interaction logic for wndEnvironment.xaml
    /// </summary>
    public partial class wndEnvironment : Window
    {
        public delegate void EnvironmentFormExit(bool bCancelled);
        public event EnvironmentFormExit EnvironmentFormCompleteEvent;
        public void EnvironmentFormIsCompleteSubscribe(EnvironmentFormExit eventHandler)
        {
            EnvironmentFormCompleteEvent += new EnvironmentFormExit(eventHandler);
        }


        public wndEnvironment()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            switch (clsEnvironment.DSEnv)
            {
                case clsEnvironment.DSEnvironment.Development:
                    rbDevelopment.IsChecked = true;
                    break;

                case clsEnvironment.DSEnvironment.Production:
                    rbProduction.IsChecked = true;
                    break;

                case clsEnvironment.DSEnvironment.Training:
                    rbTraining.IsChecked = true;
                    break;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            EnvironmentFormCompleteEvent(true);
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            bool bTraining;
            bool bProduction;
            bool bDevelopment;

            bTraining = (bool)(rbTraining.IsChecked == null ? false : rbTraining.IsChecked);
            bProduction = (bool)(rbProduction.IsChecked == null ? false : rbProduction.IsChecked);
            bDevelopment = (bool)(rbDevelopment.IsChecked == null ? false : rbDevelopment.IsChecked);
            if (bTraining) clsEnvironment.DSEnv = clsEnvironment.DSEnvironment.Training;
            if (bProduction) clsEnvironment.DSEnv = clsEnvironment.DSEnvironment.Production;
            if (bDevelopment) clsEnvironment.DSEnv = clsEnvironment.DSEnvironment.Development;
            EnvironmentFormCompleteEvent(false);
        }
    }
}
