﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyStuff
{
    public class clsNewCard : EventArgs
    {
        public int iCardID { get; set; }
        public bool bCancelled { get; set; }
        public event EventFormIsComplete CardFormCompleteEvent;
        //
        //  Allows main form to subscribe to this event.
        //
        public void CardFormIsCompleteSubscribe(EventFormIsComplete eventHandler)
        {
            CardFormCompleteEvent += new EventFormIsComplete(eventHandler);
        }
        //
        //  Called by second form to raise the event.
        //
        public void CardSignalFormIsComplete()
        {
            CardFormCompleteEvent();
        }

    }
    public delegate void CardEventFormIsComplete();
}
