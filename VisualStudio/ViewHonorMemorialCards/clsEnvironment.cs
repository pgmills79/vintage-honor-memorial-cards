﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyCode;
using System.Security.Principal;

namespace MyCode
{
    static class clsEnvironment
    {
        public enum DSEnvironment
        {
            Production = 1,
            Development = 2,
            Training = 3
        }
        private static DSEnvironment DSENV = DSEnvironment.Production;
        public static DSEnvironment DSEnv
        {
            get { return DSENV; }
            set { DSENV = value; }
        }
        public static void GetuserName(out string sUserName, out string sGroupName)
        {
            int i;
            string sName;
            WindowsPrincipal wp = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
            sName = wp.Identity.Name;
            sUserName = "";
            sGroupName = "";
            string[] saWords = sName.Split('\\');
            i = 0;
            foreach (string sWord in saWords)
            {
                i++;
                if (i == 1) sGroupName = sWord;
                if (i == 2) sUserName = sWord;
                if (i > 2) break;
            }
        }
    }
}
