﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CardStatus
{
    static class clsStatus
    {
        public static string DescriptionToStatusCode(this string sDescription)
        {
            string sStatusCode = "";
            switch (sDescription)
            {
                case "Delete":
                    sStatusCode = "X";
                    break;

                case "Donor Ministry":
                    sStatusCode = "D";
                    break;

                case "Entry In Progress":
                    sStatusCode = "I";
                    break;

                case "Hold":
                    sStatusCode = "H";
                    break;

                case "No Print":
                    sStatusCode = "N";
                    break;

                case "Printed":
                    sStatusCode = "P";
                    break;

                case "Ready":
                    sStatusCode = "Y";
                    break;

                case "Test":
                    sStatusCode = "T";
                    break;

                default:
                    sStatusCode = "";
                    break;
            }
            return (sStatusCode);
        }
    }
}
