﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;

namespace MyStuff
{
    public class clsAddProjects : EventArgs
    {
        public int iCardID { get; set; }
        public string sToName { get; set; }
        public string sFromName { get; set; }
        public string sGiftItems { get; set; }
        public string sCardType { get; set; }
        public string sInMemoryOf { get; set; }
        public string sReturnToSender { get; set; }
        public bool bCancelled { get; set; }
        public event EventFormIsComplete FormCompleteEvent;
        public string SUserName
        {
            get
            {
                string sun;
                string sgroup;
                GetuserName(out sun, out sgroup);
                return (sun);
            }
        }
        private void GetuserName(out string sUserName, out string sGroupName)
        {
            int i;
            string sName;
            WindowsPrincipal wp = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
            sName = wp.Identity.Name;
            sUserName = "";
            sGroupName = "";
            string[] saWords = sName.Split('\\');
            i = 0;
            foreach (string sWord in saWords)
            {
                i++;
                if (i == 1) sGroupName = sWord;
                if (i == 2) sUserName = sWord;
                if (i > 2) break;
            }
        }

        //
        //  Allows main form to subscribe to this event.
        //
        public void FormIsCompleteSubscribe(EventFormIsComplete eventHandler)
        {
            FormCompleteEvent += new EventFormIsComplete(eventHandler);
        }
        //
        //  Called by second form to raise the event.
        //
        public void SignalFormIsComplete()
        {
            FormCompleteEvent();
        }
    }
    public delegate void EventFormIsComplete();
}
