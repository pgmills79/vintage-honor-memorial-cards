﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyCode;
using System.Data;
using Xceed.Wpf.DataGrid;
using Xceed.Wpf.Controls;
using System.ComponentModel;
using System.Windows.Threading;
using System.Threading;
using MyStuff;
using CardStatus;

namespace ViewHonorMemorialCards
{
    /// <summary>
    /// Interaction logic for wndViewHonorMemorialCards.xaml
    /// </summary>
    public partial class wndViewHonorMemorialCards : Window
    {
        // Projects form
        private wndProjects wpForm = null;
        private wndNewCard ncForm = null;
        private wndEnvironment envForm = null;
        private clsAddProjects clsap = null;
        private clsNewCard clsnc = null;
        public wndViewHonorMemorialCards()
        {
            InitializeComponent();
        }
        private static DataTable m_SearchList;
        public static DataTable SearchList
        {
            get
            {
                return m_SearchList;
            }
        }
        //public DataTable datasource;
        private static DataTable m_OriginalSearchList;

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            InProcess();
            DoTheSearch();
            Done();
        }
        private bool DoTheSearch()
        {
            long lAccountNumber;
            int iBatchNumber;
            DateTime dBeginDate;
            DateTime dEndDate;
            string sPrintStatus;
            int lGroup;
            int lOf;
            string sFromName;
            string sToName;
            string sMemorialName;
            string sAddress1;
            string sCity;
            string sZip;
            int lRowsReturned;
            bool bMaxLimitWarning;
            bool bStatus;
            clsDatabase c = new clsDatabase();
            tbStatus.Visibility = Visibility.Hidden;
            if ((txtAccountNumber.Text == null) || (txtAccountNumber.Text == ""))
            {
                lAccountNumber = 0;
            }
            else
            {
                lAccountNumber = Convert.ToInt32(txtAccountNumber.Text);
            }
            if ((txtBatchNumber.Text == null) || (txtBatchNumber.Text == ""))
            {
                iBatchNumber = 0;
            }
            else
            {
                iBatchNumber = Convert.ToInt32(txtBatchNumber.Text);
            }
            dBeginDate = Convert.ToDateTime(dtBeginDate.SelectedDate.ToString());
            dEndDate = Convert.ToDateTime(dtEndDate.SelectedDate.ToString());

            sPrintStatus = cbStatus.Text.ToString().DescriptionToStatusCode();
            lGroup = Convert.ToInt32(cbGroup.Text);
            lOf = Convert.ToInt32(cbOf.Text);
            sFromName = txtFromName.Text;
            sToName = txtToName.Text;
            sMemorialName = txtMemorialName.Text;
            sAddress1 = txtAddress1.Text;
            sCity = txtCity.Text;
            sZip = txtZip.Text;
            if (bStatus = c.uspSearchHonorMemorialCards(lAccountNumber,
                                                  dBeginDate,
                                                  dEndDate,
                                                  sPrintStatus,
                                                  lGroup,
                                                  lOf,
                                                  sFromName,
                                                  sToName,
                                                  sMemorialName,
                                                  sAddress1,
                                                  sCity,
                                                  sZip,
                                                  iBatchNumber,
                                                  out lRowsReturned,
                                                  out bMaxLimitWarning,
                                                  out m_SearchList))
            {
                m_SearchList.RowChanged += new DataRowChangeEventHandler(SearchList_RowChanged);
                m_OriginalSearchList = m_SearchList.Copy();

                dgHonorCards.ItemsSource = new DataGridCollectionView(SearchList.DefaultView);
                //dgHonorCards.UpdateSourceTrigger = DataGridUpdateSourceTrigger.RowEndingEdit;

                if (bMaxLimitWarning)
                {
                    tbStatus.Text = lRowsReturned.ToString() + " displayed.  Not all listings are displayed.";
                    SolidColorBrush colDarkRed = new SolidColorBrush(Colors.DarkRed);
                    tbStatus.Foreground = colDarkRed;
                }
                else
                {
                    tbStatus.Text = lRowsReturned.ToString() + " displayed.";
                    SolidColorBrush colBlack = new SolidColorBrush(Colors.Black);
                    tbStatus.Foreground = colBlack;
                }
                tbStatus.Visibility = Visibility.Visible;
            }
            c = null;
            return (bStatus);
        }

        private bool bChangingRow = false; // Flag to prevent recursion.
        public bool bDontUpdateRow = false; // Flag when we are updating status.

        void SearchList_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            long lCardID = 0;
            string sFromName = "";
            string sToName = "";
            string sMemorialName = "";
            string sToAddress1 = "";
            string sToAddress2 = "";
            string sToAddress3 = "";
            string sToCity = "";
            string sToState = "";
            string sToZip = "";
            string sToCountry = "";
            string sToStatus = "";
            string sUpdatedStatus = "";
            bool bRecordUpdated = false;
            string sNewChangedDate = "";
            string sNewChangedUser = "";
            try
            {
                if (bChangingRow) return;
                if (bDontUpdateRow) return;
                bChangingRow = true;
                lCardID = Convert.ToInt32(e.Row["CardDocumentNumber"].ToString());
                sFromName = e.Row["FromName"].ToString();
                sToName = e.Row["ToName"].ToString();
                sMemorialName = e.Row["MemorialName"].ToString();
                sToAddress1 = e.Row["ToAddress1"].ToString();
                sToAddress2 = e.Row["ToAddress2"].ToString();
                sToAddress3 = e.Row["ToAddress3"].ToString();
                sToCity = e.Row["ToCity"].ToString();
                sToState = e.Row["ToState"].ToString();
                sToZip = e.Row["ToZip"].ToString();
                sToCountry = e.Row["ToCountry"].ToString();
                sToStatus = "";
                clsDatabase cd = new clsDatabase();
                cd.uspUpdateHonorMemorialCards(lCardID,
                                               sFromName,
                                               sToName,
                                               sMemorialName,
                                               sToAddress1,
                                               sToAddress2,
                                               sToAddress3,
                                               sToCity,
                                               sToState,
                                               sToZip,
                                               sToCountry,
                                               sToStatus,
                                               out sUpdatedStatus,
                                               out bRecordUpdated,
                                               out sNewChangedUser,
                                               out sNewChangedDate);
                //
                // Update the status
                //
                System.Data.DataRow[] foundRows;
                foundRows = m_SearchList.Select("CardDocumentNumber = " + lCardID.ToString());
                if (foundRows != null)
                {
                    foundRows[0]["NewStatus"] = "";
                    if (bRecordUpdated)
                    {
                        foundRows[0]["Status"] = sUpdatedStatus.DescriptionToStatusCode();
                        foundRows[0]["ChangeDate"] = sNewChangedDate;
                        foundRows[0]["ChangeUser"] = sNewChangedUser;
                    }
                }
                //
                // If we did not update, restore old values.
                //
                if (!bRecordUpdated)
                {
                    System.Data.DataRow[] foundRowsoriginal;
                    foundRowsoriginal = m_OriginalSearchList.Select("CardDocumentNumber = " + lCardID.ToString());
                    if (foundRowsoriginal != null)
                    {
                        foundRows[0]["Status"] = foundRowsoriginal[0]["Status"];
                        foundRows[0]["FromName"] = foundRowsoriginal[0]["FromName"];
                        foundRows[0]["ToName"] = foundRowsoriginal[0]["ToName"];
                        foundRows[0]["MemorialName"] = foundRowsoriginal[0]["MemorialName"];
                        foundRows[0]["ToAddress1"] = foundRowsoriginal[0]["ToAddress1"];
                        foundRows[0]["ToAddress2"] = foundRowsoriginal[0]["ToAddress2"];
                        foundRows[0]["ToAddress3"] = foundRowsoriginal[0]["ToAddress3"];
                        foundRows[0]["ToCity"] = foundRowsoriginal[0]["ToCity"];
                        foundRows[0]["ToState"] = foundRowsoriginal[0]["ToState"];
                        foundRows[0]["ToZip"] = foundRowsoriginal[0]["ToZip"];
                        foundRows[0]["ToCountry"] = foundRowsoriginal[0]["ToCountry"];
                    }
                }
                //
                // Once we got things updated, create a new original list in case same record is edited more than once.
                //
                m_OriginalSearchList = null;
                m_OriginalSearchList = m_SearchList.Copy();
            }
            catch (Exception ee)
            {
                MessageBox.Show("Error updating record - " + ee.Message);
            }
            bChangingRow = false;
        }


        private void digitonly_KeyDown(object sender, KeyEventArgs e)
        {
            //
            // Key down for numeric fields
            //
            KeyConverter kc = new KeyConverter();
            string xChar = kc.ConvertToString(e.Key);
            if (xChar.Length > 1)
                e.Handled = false;
            else
            {
                if (Char.IsDigit(xChar[0]))
                    e.Handled = false;
                else
                    e.Handled = true;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string sUserName;
            string sGroupName;

            ClearSearch();
            ncForm = null;
            wpForm = null;
            //
            //  Decide what environment go to into.
            //
            clsEnvironment.GetuserName(out sUserName, out sGroupName);
            sUserName = sUserName + "       ";
            if (sUserName.ToLower().Substring(0, 7).Trim() == "student")
            {
                clsEnvironment.DSEnv = clsEnvironment.DSEnvironment.Training;
                MessageBox.Show("You will be working against the training environment.");
                tbEnvironment.Text = "Training";
                tbEnvironment.Visibility = Visibility.Visible;
            }
            else
            {
                clsEnvironment.DSEnv = clsEnvironment.DSEnvironment.Production;
                tbEnvironment.Text = "Production";
                tbEnvironment.Visibility = Visibility.Hidden;
            }
            SetForms();
        }

        private void SetForms()
        {
            if (wpForm != null)
            {
                if (wpForm.IsLoaded) wpForm.Close();
            }
            wpForm = null;
            wpForm = new wndProjects();
            wpForm.ExitForm += FormHasCompleted;
            wpForm.Show();
            wpForm.Visibility = Visibility.Hidden;

            if (ncForm != null)
            {
                if (ncForm.IsLoaded) ncForm.Close();
            }
            ncForm = null;
            ncForm = new wndNewCard();
            ncForm.ExitForm += CardFormHasCompleted;
            ncForm.Show();
            ncForm.Visibility = Visibility.Hidden;

            clsap = null;
            clsap = new clsAddProjects();

            clsnc = null;
            clsnc = new clsNewCard();
            // Subscribe to event that is defined in class.
            clsap.FormIsCompleteSubscribe(clsAddProjects_FormCompleteEvent);
            clsnc.CardFormIsCompleteSubscribe(clsNewCard_FormCompleteEvent);
        }

        private void FormHasCompleted(bool bIsCancelled)
        {
            //if (bIsCancelled)
            //    MessageBox.Show("Form is cancelled");
            //else
            //    MessageBox.Show("Form closed");
            Done();
        }
        private void CardFormHasCompleted(bool bIsCancelled)
        {
            //if (bIsCancelled)
            //    MessageBox.Show("Form is cancelled");
            //else
            //    MessageBox.Show("Form closed");
            Done();
        }


        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearSearch();
        }
        private void ClearSearch()
        {
            string sYear = "2000";
            txtAccountNumber.Text = "";
            txtBatchNumber.Text = "";
            txtAddress1.Text = "";
            txtCity.Text = "";
            txtFromName.Text = "";
            txtMemorialName.Text = "";
            txtToName.Text = "";
            txtZip.Text = "";
            cbStatus.Text = "All";
            dtEndDate.SelectedDate = DateTime.Today;
            if (DateTime.Today.Month >= 10)
                sYear = DateTime.Today.Year.ToString().Trim();
            else
                sYear = (DateTime.Today.Year-1).ToString().Trim();
            dtBeginDate.SelectedDate = Convert.ToDateTime("10/01/" + sYear + " 00:00");
            tbStatus.Visibility = Visibility.Hidden;
            cbOf.Text = "0";
            cbGroup.Text = "0";
            DoEvents();
        }
        private void InProcess()
        {
            txtAccountNumber.IsEnabled = false;
            txtBatchNumber.IsEnabled = false;
            txtAddress1.IsEnabled = false;
            txtCity.IsEnabled = false;
            txtFromName.IsEnabled = false;
            txtMemorialName.IsEnabled = false;
            txtToName.IsEnabled = false;
            txtZip.IsEnabled = false;
            cbStatus.IsEnabled = false;
            dtBeginDate.IsEnabled = false;
            dtEndDate.IsEnabled = false;
            cbOf.IsEnabled = false;
            cbGroup.IsEnabled = false;
            btnClear.IsEnabled = false;
            btnSearch.IsEnabled = false;
            dgHonorCards.IsEnabled = false;
            DoEvents();
        }
        private void Done()
        {
            DoneNoDoEvents();
            DoEvents();
        }
        private void DoneNoDoEvents()
        {
            txtAccountNumber.IsEnabled = true;
            txtBatchNumber.IsEnabled = true;
            txtAddress1.IsEnabled = true;
            txtCity.IsEnabled = true;
            txtFromName.IsEnabled = true;
            txtMemorialName.IsEnabled = true;
            txtToName.IsEnabled = true;
            txtZip.IsEnabled = true;
            cbStatus.IsEnabled = true;
            dtBeginDate.IsEnabled = true;
            dtEndDate.IsEnabled = true;
            cbOf.IsEnabled = true;
            cbGroup.IsEnabled = true;
            btnClear.IsEnabled = true;
            btnSearch.IsEnabled = true;
            dgHonorCards.IsEnabled = true;
        }

        //
        // Do events
        //
        private void DoEvents()
        {
            DispatcherFrame f = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
            (SendOrPostCallback)delegate(object arg)
            {
                DispatcherFrame fr = arg as DispatcherFrame;
                fr.Continue = false;
            }, f);
            Dispatcher.PushFrame(f);
            //Thread.Sleep(1000);
        }

        private void dgHonorCards_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //AddTheProjects();
        }
        private void AddTheProjects()
        {
            System.Data.DataRow row = (System.Data.DataRow)dgHonorCards.SelectedItem;
            if (row != null)
            {
                InProcess();
                clsap.iCardID = Convert.ToInt32(row["CardDocumentNumber"].ToString());
                clsap.sFromName = (string)row["FromName"];
                clsap.sToName = (string)row["ToName"];
                clsap.bCancelled = false;
                clsap.sGiftItems = "";
                if (!wpForm.IsLoaded)
                {
                    wpForm = null;
                    wpForm = new wndProjects();
                    wpForm.Show();
                }
                wpForm.RevealScreen(clsap);
                wpForm.Visibility = Visibility.Visible;
            }
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
            wpForm.Close();
            wpForm = null;
            clsap = null;
            clsnc = null;
        }
        void clsAddProjects_FormCompleteEvent()
        {
            string sStatus = "";
            bDontUpdateRow = true;
            System.Data.DataRow[] foundRows = null;
            if (clsap != null)
            {
                if (!clsap.bCancelled)
                {
                    foundRows = m_SearchList.Select("CardDocumentNumber = " + clsap.iCardID.ToString());
                    if (foundRows != null)
                    {
                        sStatus = foundRows[0]["Status"].ToString();
                        foundRows[0]["Gifts"] = clsap.sGiftItems;
                        foundRows[0]["MemorialName"] = clsap.sInMemoryOf.Trim();
                        foundRows[0]["LetterCode"] = clsap.sCardType;
                        foundRows[0]["ReturnToSender"] = clsap.sReturnToSender;
                        foundRows[0]["Status"] = sStatus;
                        foundRows[0]["ChangeDate"] = RemoveDay(DateTime.Now.ToLongDateString()) + " " + DateTime.Now.ToShortTimeString();
                        foundRows[0]["ChangeUser"] = clsap.SUserName;
                    }
                    clsDatabase cdb = new clsDatabase();
                    DataTable dt;
                    if (cdb.uspHonorMemorialCardsByCardID(clsap.iCardID, out dt))
                    {
                        foreach (System.Data.DataRow dr in dt.Rows)
                        {
                            foundRows[0]["FromName"] = (string)dr["FromName"].ToString().Trim();
                            foundRows[0]["Toname"] = (string)dr["ToName"].ToString().Trim();
                            foundRows[0]["ToAddress1"] = (string)dr["ToAddress1"].ToString().Trim();
                            foundRows[0]["ToAddress2"] = (string)dr["ToAddress2"].ToString().Trim();
                            foundRows[0]["ToAddress3"] = (string)dr["ToAddress3"].ToString().Trim();
                            foundRows[0]["ToCity"] = (string)dr["ToCity"].ToString().Trim();
                            foundRows[0]["ToState"] = (string)dr["ToState"].ToString().Trim();
                            foundRows[0]["ToZip"] = (string)dr["ToZip"].ToString().Trim();
                            foundRows[0]["ToCountry"] = (string)dr["ToCountry"].ToString().Trim();
                            foundRows[0]["MemorialName"] = (string)dr["MemorialName"].ToString().Trim();
                            foundRows[0]["LetterCode"] = (string)dr["LetterCode"].ToString().Trim();
                            foundRows[0]["ReturnToSender"] = (string)dr["ReturnToSender"].ToString().Trim();
                            foundRows[0]["status"] = (string)dr["status"].ToString().Trim();
                        }
                    }
                }
                Done();
                bDontUpdateRow = false;
            }
        }
        string RemoveDay(string s)
        {
            string ss;
            ss = s;
            ss = ss.Replace("Sunday,", "");
            ss = ss.Replace("Monday,", "");
            ss = ss.Replace("Tuesday,", "");
            ss = ss.Replace("Wednesday,", "");
            ss = ss.Replace("Thursday,", "");
            ss = ss.Replace("Friday,", "");
            ss = ss.Replace("Saturday,", "");
            return (ss.Trim());
        }

        void clsNewCard_FormCompleteEvent()
        {
            if (clsnc != null)
            {
                if (!clsnc.bCancelled)
                {
                    clsDatabase cdb = new clsDatabase();
                    if (cdb.uspHonorMemorialCardsByCardID(clsnc.iCardID, out m_SearchList))
                    {

                        m_SearchList.RowChanged += new DataRowChangeEventHandler(SearchList_RowChanged);
                        m_OriginalSearchList = m_SearchList.Copy();

                        dgHonorCards.ItemsSource = new DataGridCollectionView(SearchList.DefaultView);
                        //dgHonorCards.UpdateSourceTrigger = DataGridUpdateSourceTrigger.RowEndingEdit;
                        tbStatus.Text = "1 displayed.";
                        SolidColorBrush colBlack = new SolidColorBrush(Colors.Black);
                        tbStatus.Foreground = colBlack;
                        tbStatus.Visibility = Visibility.Visible;
                    }
                }
            }
            wndViweHonorMemorialCardForm.Visibility = Visibility.Visible;
            Done();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }


        private void miChangeProjects_Click(object sender, RoutedEventArgs e)
        {
            AddTheProjects();
        }

        private void miHonorMemorialCards_Click(object sender, RoutedEventArgs e)
        {
            clsIni p = new clsIni();
            clsReportChoice c = new clsReportChoice(p.SHonorMemorialReport,
                                                    p.SReportServer);
            frmReport f = new frmReport(c);
            f.Show();
        }
        private void AddTheCard()
        {
            InProcess();
            if (!ncForm.IsLoaded)
            {
                ncForm = null;
                ncForm = new wndNewCard();
                ncForm.Show();
            }
            ncForm.RevealScreen(clsnc);
            ncForm.Visibility = Visibility.Visible;
            wndViweHonorMemorialCardForm.Visibility = Visibility.Hidden;
        }

        private void miNewCard_Click(object sender, RoutedEventArgs e)
        {
            AddTheCard();
        }

        private void dgHonorCards_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            AddTheProjects();
        }

        private void miEnvironment_Click(object sender, RoutedEventArgs e)
        {
            envForm = null;
            envForm = new wndEnvironment();
            envForm.Show();
            envForm.Visibility = Visibility.Visible;
            envForm.EnvironmentFormIsCompleteSubscribe(EnvironmentFormIsComplete);
            wndViweHonorMemorialCardForm.Visibility = Visibility.Hidden;
        }
        private void EnvironmentFormIsComplete(bool isCancel)
        {
            if (!isCancel)
            {
                switch (clsEnvironment.DSEnv)
                {
                    case clsEnvironment.DSEnvironment.Development:
                        tbEnvironment.Text = "Development";
                        tbEnvironment.Visibility = Visibility.Visible;
                        break;
                    case clsEnvironment.DSEnvironment.Production:
                        tbEnvironment.Text = "Production";
                        tbEnvironment.Visibility = Visibility.Visible;
                        break;
                    case clsEnvironment.DSEnvironment.Training:
                        tbEnvironment.Text = "Training";
                        tbEnvironment.Visibility = Visibility.Visible;
                        break;
                }
                dgHonorCards.ItemsSource = null;
                tbStatus.Text = "";
                SetForms();
            }
            envForm.Close();
            envForm = null;
            wndViweHonorMemorialCardForm.Visibility = Visibility.Visible;
        }
    }
}