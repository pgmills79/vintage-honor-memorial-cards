﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyStuff;
using MyCode;
using System.Data;
using Xceed.Wpf.DataGrid;
using System.Windows.Threading;
using System.Threading;

namespace ViewHonorMemorialCards
{
    /// <summary>
    /// Interaction logic for wndNewCard.xaml
    /// </summary>

    public delegate void CardProjectsFormExit(bool bCancelled);

    public partial class wndNewCard : Window
    {
        private clsNewCard cnc;
        private static DataTable m_NCSearchList;
        public static DataTable NCSearchList
        {
            get
            {
                return m_NCSearchList;
            }
        }

        public event CardProjectsFormExit ExitForm;

        public wndNewCard()
        {
            InitializeComponent();
            cnc = null;
        }
        public void RevealScreen(clsNewCard nc)
        {
            cnc = nc;
            cnc.bCancelled = true;
            m_NCSearchList = null;
            dgDocuments.ItemsSource = null;
            txtAccount.Text = "";
            txtDocumentNumber.Text = "";
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            if (cnc != null)
                cnc.CardSignalFormIsComplete();
        }

        private void InProcess()
        {
            btnSearch.IsEnabled = false;
            btnCancel.IsEnabled = false;
            DoEvents();
        }
        private void Done()
        {
            btnSearch.IsEnabled = true;
            btnCancel.IsEnabled = true;
            DoEvents();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            int lAccount;
            int lDocumentNumber;
            clsDatabase cdb = new clsDatabase();
            MessageBoxImage MBImage;
            InProcess();
            Int32.TryParse(txtDocumentNumber.Text.ToString(), out lDocumentNumber);
            Int32.TryParse(txtAccount.Text.ToString(), out lAccount);

            if (lDocumentNumber > 0)
            {
                //
                // Prompt user to validate
                //
                long lAccountNumber;
                string sAccountName;
                if (cdb.uspCheckCardDocumentNumber(lDocumentNumber, out lAccountNumber, out sAccountName))
                {
                    if (lAccountNumber > 0)
                    {

                        MBImage = MessageBoxImage.Question;
                        MessageBoxResult result = MessageBox.Show("Add card for " + sAccountName.Trim() + "?", "Add Card?", MessageBoxButton.YesNo, MBImage);
                        if (result == MessageBoxResult.Yes)
                        {
                            AddTheCard(lDocumentNumber);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Could not find document number - " + lDocumentNumber.ToString());
                    }
                }
            }
            else
            {
                if (lAccount > 0)
                {
                    if (cdb.uspGetDocumentProducts(lAccount, out m_NCSearchList))
                    {

                        dgDocuments.ItemsSource = new DataGridCollectionView(NCSearchList.DefaultView);
                    }
                }
            }
            Done();
        }

        private void AddTheCard(long lDocumentNumber)
        {
            long lCardID;
            clsDatabase cdb = new clsDatabase();
            if (cdb.uspAddHonorMemorialCard(lDocumentNumber, out lCardID))
            {
                if (lCardID > 0)
                {
                    cnc.iCardID = (int)lCardID;
                    this.Visibility = Visibility.Hidden;
                    cnc.bCancelled = false;
                    cnc.CardSignalFormIsComplete();
                    if (ExitForm != null)
                        ExitForm(false);
                }
            }
            cdb = null;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            cnc.bCancelled = true;
            cnc.CardSignalFormIsComplete();
            if (ExitForm != null)
                ExitForm(true);
        }

        private void txtAccount_KeyDown(object sender, KeyEventArgs e)
        {
            //
            // Key down for numeric fields
            //
            KeyConverter kc = new KeyConverter();
            string xChar = kc.ConvertToString(e.Key);
            if (xChar.Length > 1)
                e.Handled = false;
            else
            {
                if (Char.IsDigit(xChar[0]))
                    e.Handled = false;
                else
                    e.Handled = true;
            }
        }

        private void txtDocumentNumber_KeyDown(object sender, KeyEventArgs e)
        {
            //
            // Key down for numeric fields
            //
            KeyConverter kc = new KeyConverter();
            string xChar = kc.ConvertToString(e.Key);
            if (xChar.Length > 1)
                e.Handled = false;
            else
            {
                if (Char.IsDigit(xChar[0]))
                    e.Handled = false;
                else
                    e.Handled = true;
            }
        }

        private void txtDocumentNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtDocumentNumber.Text.ToString().Trim().Length > 0)
                btnSearch.Content = "Add";
            else
                btnSearch.Content = "Search";
        }

        private void dgDocuments_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            long lDocumentNumber;
            if (dgDocuments.SelectedItems.Count > 0)
            {
                System.Data.DataRow row = (System.Data.DataRow)dgDocuments.SelectedItem;
                lDocumentNumber = (long)row["DocumentNumber"];
                AddTheCard(lDocumentNumber);
            }
        }

        //
        // Do events
        //
        private void DoEvents()
        {
            DispatcherFrame f = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
            (SendOrPostCallback)delegate(object arg)
            {
                DispatcherFrame fr = arg as DispatcherFrame;
                fr.Continue = false;
            }, f);
            Dispatcher.PushFrame(f);
            //Thread.Sleep(1000);
        }
    }
}
