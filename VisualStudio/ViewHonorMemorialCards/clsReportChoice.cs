﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyCode
{
    public class clsReportChoice
    {
        private String sReportPath;
        private String sReportServer;
        private int iParameterType;   // 1 is PrintGroup - none else defined.

        public int IParameterType
        {
            get { return iParameterType; }
        }

        public clsReportChoice() { }

        //public clsReportChoice(DateTime BeginDate,
        //                DateTime EndDate,
        //                string ReportPath,
        //                string ReportServer)
        //{
        //    SReportPath = ReportPath;
        //    DBeginDate = BeginDate;
        //    DEndDate = EndDate;
        //    SReportServer = ReportServer;
        //    iParameterType = 1;
        //}


        public clsReportChoice(string ReportPath,
                               string ReportServer)
        {
            SReportPath = ReportPath;
            SReportServer = ReportServer;
            iParameterType = 1;
        }

        public String SReportPath
        {
            get { return sReportPath; }
            set { sReportPath = value; }
        }

        public String SReportServer
        {
            get { return sReportServer; }
            set { sReportServer = value; }
        }
    }
}

