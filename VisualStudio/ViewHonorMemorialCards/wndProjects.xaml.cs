﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyStuff;
using MyCode;
using System.Data;
using CardStatus;

namespace ViewHonorMemorialCards
{
    /// <summary>
    /// Interaction logic for wndProjects.xaml
    /// </summary>
    public delegate void ProjectsFormExit(bool bCancelled);

    public partial class wndProjects : Window
    {
        private clsAddProjects cap;
        public bool bLoading = true;

        private static DataTable m_ProjectCodes;
        public static DataTable ProjectCodes
        {
            get
            {
                return m_ProjectCodes;
            }
        }

        private static DataTable m_CardTypes;
        public static DataTable CardTypes
        {
            get
            {
                return m_CardTypes;
            }
        }

        public event ProjectsFormExit ExitForm;

        public wndProjects()
        {
            InitializeComponent();
            cap = null;
            SetDropDownBoxes();
        }
        private static Binding b;
        private static Binding bCard;
        private void SetDropDownBoxes()
        {
            clsDatabase cdb = new clsDatabase();
            cdb.uspGetListOfValidCardProjects(out m_ProjectCodes);
            cdb.uspGetListHonorMemorialCards(out m_CardTypes);
            //cbLetters.DataContext = DonorMinistryLetters;
            b = new Binding();
            b.Source = ProjectCodes;
            b.Mode = BindingMode.OneWay;

            bCard = new Binding();
            bCard.Source = CardTypes;
            bCard.Mode = BindingMode.OneWay;

            cbProject1.IsEditable = true;
            cbProject1.DisplayMemberPath = "Description";
            cbProject1.SelectedValuePath = "Project";
            cbProject1.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            //cbProject2.IsEditable = true;
            cbProject2.DisplayMemberPath = "Description";
            cbProject2.SelectedValuePath = "Project";
            cbProject2.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject3.DisplayMemberPath = "Description";
            cbProject3.SelectedValuePath = "Project";
            cbProject3.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject4.DisplayMemberPath = "Description";
            cbProject4.SelectedValuePath = "Project";
            cbProject4.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject5.DisplayMemberPath = "Description";
            cbProject5.SelectedValuePath = "Project";
            cbProject5.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject6.DisplayMemberPath = "Description";
            cbProject6.SelectedValuePath = "Project";
            cbProject6.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject7.DisplayMemberPath = "Description";
            cbProject7.SelectedValuePath = "Project";
            cbProject7.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject8.DisplayMemberPath = "Description";
            cbProject8.SelectedValuePath = "Project";
            cbProject8.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject9.DisplayMemberPath = "Description";
            cbProject9.SelectedValuePath = "Project";
            cbProject9.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject10.DisplayMemberPath = "Description";
            cbProject10.SelectedValuePath = "Project";
            cbProject10.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject11.DisplayMemberPath = "Description";
            cbProject11.SelectedValuePath = "Project";
            cbProject11.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject12.DisplayMemberPath = "Description";
            cbProject12.SelectedValuePath = "Project";
            cbProject12.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject13.DisplayMemberPath = "Description";
            cbProject13.SelectedValuePath = "Project";
            cbProject13.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject14.DisplayMemberPath = "Description";
            cbProject14.SelectedValuePath = "Project";
            cbProject14.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject15.DisplayMemberPath = "Description";
            cbProject15.SelectedValuePath = "Project";
            cbProject15.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject16.DisplayMemberPath = "Description";
            cbProject16.SelectedValuePath = "Project";
            cbProject16.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject17.DisplayMemberPath = "Description";
            cbProject17.SelectedValuePath = "Project";
            cbProject17.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject18.DisplayMemberPath = "Description";
            cbProject18.SelectedValuePath = "Project";
            cbProject18.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject19.DisplayMemberPath = "Description";
            cbProject19.SelectedValuePath = "Project";
            cbProject19.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject20.DisplayMemberPath = "Description";
            cbProject20.SelectedValuePath = "Project";
            cbProject20.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject21.DisplayMemberPath = "Description";
            cbProject21.SelectedValuePath = "Project";
            cbProject21.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject22.DisplayMemberPath = "Description";
            cbProject22.SelectedValuePath = "Project";
            cbProject22.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject23.DisplayMemberPath = "Description";
            cbProject23.SelectedValuePath = "Project";
            cbProject23.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject24.DisplayMemberPath = "Description";
            cbProject24.SelectedValuePath = "Project";
            cbProject24.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject25.DisplayMemberPath = "Description";
            cbProject25.SelectedValuePath = "Project";
            cbProject25.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject26.DisplayMemberPath = "Description";
            cbProject26.SelectedValuePath = "Project";
            cbProject26.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject27.DisplayMemberPath = "Description";
            cbProject27.SelectedValuePath = "Project";
            cbProject27.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject28.DisplayMemberPath = "Description";
            cbProject28.SelectedValuePath = "Project";
            cbProject28.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject29.DisplayMemberPath = "Description";
            cbProject29.SelectedValuePath = "Project";
            cbProject29.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject30.DisplayMemberPath = "Description";
            cbProject30.SelectedValuePath = "Project";
            cbProject30.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject31.DisplayMemberPath = "Description";
            cbProject31.SelectedValuePath = "Project";
            cbProject31.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject32.DisplayMemberPath = "Description";
            cbProject32.SelectedValuePath = "Project";
            cbProject32.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject33.DisplayMemberPath = "Description";
            cbProject33.SelectedValuePath = "Project";
            cbProject33.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject34.DisplayMemberPath = "Description";
            cbProject34.SelectedValuePath = "Project";
            cbProject34.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject35.DisplayMemberPath = "Description";
            cbProject35.SelectedValuePath = "Project";
            cbProject35.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject36.DisplayMemberPath = "Description";
            cbProject36.SelectedValuePath = "Project";
            cbProject36.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject37.DisplayMemberPath = "Description";
            cbProject37.SelectedValuePath = "Project";
            cbProject37.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject38.DisplayMemberPath = "Description";
            cbProject38.SelectedValuePath = "Project";
            cbProject38.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject39.DisplayMemberPath = "Description";
            cbProject39.SelectedValuePath = "Project";
            cbProject39.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject40.DisplayMemberPath = "Description";
            cbProject40.SelectedValuePath = "Project";
            cbProject40.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject41.DisplayMemberPath = "Description";
            cbProject41.SelectedValuePath = "Project";
            cbProject41.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject42.DisplayMemberPath = "Description";
            cbProject42.SelectedValuePath = "Project";
            cbProject42.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject43.DisplayMemberPath = "Description";
            cbProject43.SelectedValuePath = "Project";
            cbProject43.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject44.DisplayMemberPath = "Description";
            cbProject44.SelectedValuePath = "Project";
            cbProject44.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject45.DisplayMemberPath = "Description";
            cbProject45.SelectedValuePath = "Project";
            cbProject45.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject46.DisplayMemberPath = "Description";
            cbProject46.SelectedValuePath = "Project";
            cbProject46.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject47.DisplayMemberPath = "Description";
            cbProject47.SelectedValuePath = "Project";
            cbProject47.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject48.DisplayMemberPath = "Description";
            cbProject48.SelectedValuePath = "Project";
            cbProject48.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject49.DisplayMemberPath = "Description";
            cbProject49.SelectedValuePath = "Project";
            cbProject49.SetBinding(ComboBox.ItemsSourceProperty, b);
            //
            cbProject50.DisplayMemberPath = "Description";
            cbProject50.SelectedValuePath = "Project";
            cbProject50.SetBinding(ComboBox.ItemsSourceProperty, b);
            //

            cbCardType.IsEditable = true;
            cbCardType.DisplayMemberPath = "Description";
            cbCardType.SelectedValuePath = "LetterCode";
            cbCardType.SetBinding(ComboBox.ItemsSourceProperty, bCard);

        }
        public void btnOK_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(cbProject1.Text + " | " + cbProject1.SelectedValue.ToString());
            if (CanWeWriteGifts())
            {
                this.Visibility = Visibility.Hidden;
                WriteGifts();
                cap.bCancelled = false;
                cap.SignalFormIsComplete();
                if (ExitForm != null)
                    ExitForm(false);
            }
        }

        public bool CanWeWriteGifts()
        {
            //
            // Check number of gifts as we can't do more than eight if we are test.
            //
            int iNumGifts = 0;
            bool bSuccess = true;

            if ((cbProject1.SelectedValue != null) && (cbProject1.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject2.SelectedValue != null) && (cbProject2.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject3.SelectedValue != null) && (cbProject3.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject4.SelectedValue != null) && (cbProject4.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject5.SelectedValue != null) && (cbProject5.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject6.SelectedValue != null) && (cbProject6.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject7.SelectedValue != null) && (cbProject7.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject8.SelectedValue != null) && (cbProject8.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject9.SelectedValue != null) && (cbProject9.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject10.SelectedValue != null) && (cbProject10.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject11.SelectedValue != null) && (cbProject11.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject12.SelectedValue != null) && (cbProject12.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject13.SelectedValue != null) && (cbProject13.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject14.SelectedValue != null) && (cbProject14.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject15.SelectedValue != null) && (cbProject15.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject16.SelectedValue != null) && (cbProject16.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject17.SelectedValue != null) && (cbProject17.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject18.SelectedValue != null) && (cbProject18.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject19.SelectedValue != null) && (cbProject19.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject20.SelectedValue != null) && (cbProject20.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject21.SelectedValue != null) && (cbProject21.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject22.SelectedValue != null) && (cbProject22.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject23.SelectedValue != null) && (cbProject23.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject24.SelectedValue != null) && (cbProject24.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject25.SelectedValue != null) && (cbProject25.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject26.SelectedValue != null) && (cbProject26.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject27.SelectedValue != null) && (cbProject27.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject28.SelectedValue != null) && (cbProject28.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject29.SelectedValue != null) && (cbProject29.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject30.SelectedValue != null) && (cbProject30.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject31.SelectedValue != null) && (cbProject31.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject32.SelectedValue != null) && (cbProject32.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject33.SelectedValue != null) && (cbProject33.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject34.SelectedValue != null) && (cbProject34.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject35.SelectedValue != null) && (cbProject35.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject36.SelectedValue != null) && (cbProject36.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject37.SelectedValue != null) && (cbProject37.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject38.SelectedValue != null) && (cbProject38.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject39.SelectedValue != null) && (cbProject39.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject40.SelectedValue != null) && (cbProject40.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject41.SelectedValue != null) && (cbProject41.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject42.SelectedValue != null) && (cbProject42.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject43.SelectedValue != null) && (cbProject43.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject44.SelectedValue != null) && (cbProject44.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject45.SelectedValue != null) && (cbProject45.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject46.SelectedValue != null) && (cbProject46.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject47.SelectedValue != null) && (cbProject47.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject48.SelectedValue != null) && (cbProject48.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject49.SelectedValue != null) && (cbProject49.SelectedValue.ToString() != "")) iNumGifts++;
            if ((cbProject50.SelectedValue != null) && (cbProject50.SelectedValue.ToString() != "")) iNumGifts++;
            string sNewStatusCode = cbStatus.Text.ToString().DescriptionToStatusCode();
            if ((iNumGifts > 8) && (sNewStatusCode == "T"))
            {
                MessageBox.Show("Cannot have more than eight gifts on a test account.");
                bSuccess = false;
            }
            return (bSuccess);
        }
        public void WriteGifts()
        {
            string sGuid = "";
            clsDatabase cdb = new clsDatabase();

            //
            // Put and write gifts.
            //
            if (cbProject1.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject1.SelectedValue.ToString(), ref sGuid);
            if (cbProject2.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject2.SelectedValue.ToString(), ref sGuid);
            if (cbProject3.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject3.SelectedValue.ToString(), ref sGuid);
            if (cbProject4.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject4.SelectedValue.ToString(), ref sGuid);
            if (cbProject5.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject5.SelectedValue.ToString(), ref sGuid);
            if (cbProject6.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject6.SelectedValue.ToString(), ref sGuid);
            if (cbProject7.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject7.SelectedValue.ToString(), ref sGuid);
            if (cbProject8.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject8.SelectedValue.ToString(), ref sGuid);
            if (cbProject9.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject9.SelectedValue.ToString(), ref sGuid);
            if (cbProject10.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject10.SelectedValue.ToString(), ref sGuid);
            if (cbProject11.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject11.SelectedValue.ToString(), ref sGuid);
            if (cbProject12.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject12.SelectedValue.ToString(), ref sGuid);
            if (cbProject13.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject13.SelectedValue.ToString(), ref sGuid);
            if (cbProject14.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject14.SelectedValue.ToString(), ref sGuid);
            if (cbProject15.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject15.SelectedValue.ToString(), ref sGuid);
            if (cbProject16.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject16.SelectedValue.ToString(), ref sGuid);
            if (cbProject17.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject17.SelectedValue.ToString(), ref sGuid);
            if (cbProject18.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject18.SelectedValue.ToString(), ref sGuid);
            if (cbProject19.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject19.SelectedValue.ToString(), ref sGuid);
            if (cbProject20.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject20.SelectedValue.ToString(), ref sGuid);
            if (cbProject21.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject21.SelectedValue.ToString(), ref sGuid);
            if (cbProject22.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject22.SelectedValue.ToString(), ref sGuid);
            if (cbProject23.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject23.SelectedValue.ToString(), ref sGuid);
            if (cbProject24.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject24.SelectedValue.ToString(), ref sGuid);
            if (cbProject25.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject25.SelectedValue.ToString(), ref sGuid);
            if (cbProject26.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject26.SelectedValue.ToString(), ref sGuid);
            if (cbProject27.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject27.SelectedValue.ToString(), ref sGuid);
            if (cbProject28.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject28.SelectedValue.ToString(), ref sGuid);
            if (cbProject29.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject29.SelectedValue.ToString(), ref sGuid);
            if (cbProject30.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject30.SelectedValue.ToString(), ref sGuid);
            if (cbProject31.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject31.SelectedValue.ToString(), ref sGuid);
            if (cbProject32.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject32.SelectedValue.ToString(), ref sGuid);
            if (cbProject33.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject33.SelectedValue.ToString(), ref sGuid);
            if (cbProject34.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject34.SelectedValue.ToString(), ref sGuid);
            if (cbProject35.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject35.SelectedValue.ToString(), ref sGuid);
            if (cbProject36.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject36.SelectedValue.ToString(), ref sGuid);
            if (cbProject37.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject37.SelectedValue.ToString(), ref sGuid);
            if (cbProject38.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject38.SelectedValue.ToString(), ref sGuid);
            if (cbProject39.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject39.SelectedValue.ToString(), ref sGuid);
            if (cbProject40.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject40.SelectedValue.ToString(), ref sGuid);
            if (cbProject41.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject41.SelectedValue.ToString(), ref sGuid);
            if (cbProject42.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject42.SelectedValue.ToString(), ref sGuid);
            if (cbProject43.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject43.SelectedValue.ToString(), ref sGuid);
            if (cbProject44.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject44.SelectedValue.ToString(), ref sGuid);
            if (cbProject45.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject45.SelectedValue.ToString(), ref sGuid);
            if (cbProject46.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject46.SelectedValue.ToString(), ref sGuid);
            if (cbProject47.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject47.SelectedValue.ToString(), ref sGuid);
            if (cbProject48.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject48.SelectedValue.ToString(), ref sGuid);
            if (cbProject49.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject49.SelectedValue.ToString(), ref sGuid);
            if (cbProject50.SelectedValue != null) cdb.uspPutGiftsOfCard(cap.iCardID, cbProject50.SelectedValue.ToString(), ref sGuid);
            string sGiftItems;
            cdb.uspCommitGiftCardProject(cap.iCardID, sGuid, out sGiftItems);
            cap.sGiftItems = sGiftItems;
            cap.sInMemoryOf = txtInMemoryOf.Text.Trim();
//            if ((bool)btnHonor.IsChecked)
            cap.sCardType = cbCardType.SelectedValue.ToString();
//            else
//                cap.sCardType = "Memorial";
            if ((bool)chkReturnToDonor.IsChecked)
                cap.sReturnToSender = "Yes";
            else
                cap.sReturnToSender = "No";
            string sNewStatusCode = cbStatus.Text.ToString().DescriptionToStatusCode();
            string sUpdatedStatus = "";
            bool bRecordUpdated = false;
            string sChangedName = "";
            string sChangedDate = "";
            cdb.uspUpdateHonorMemorialCards(cap.iCardID,
                                            txtFromName.Text,
                                            txtToName.Text,
                                            txtInMemoryOf.Text,
                                            txtToAddress1.Text,
                                            txtToAddress2.Text,
                                            txtToAddress3.Text,
                                            txtToCity.Text,
                                            txtToState.Text,
                                            txtToZip.Text,
                                            txtToCountry.Text,
                                            sNewStatusCode,
                                            out sUpdatedStatus,
                                            out bRecordUpdated,
                                            out sChangedName,
                                            out sChangedDate);
            cdb.uspCommitTypeOfCard(cap.iCardID, cap.sCardType, cap.sInMemoryOf,cap.sReturnToSender.Substring(0, 1));
            cdb = null;
        }
        public void RevealScreen(clsAddProjects ca)
        {
            cap = ca;
            cap.bCancelled = true;
            this.Visibility = Visibility.Visible;
            //
            // Fill the fields.
            //
            FillTheFields(ca.iCardID);

        }
        private void FillTheFields(int iCardID)
        {
            DataTable dt;
            string sReturnToSender;

            int i = 0;
            bLoading = true;
            ClearTheFields();
            clsDatabase cdb = new clsDatabase();
            cdb.uspGetGiftsOfCard(iCardID, out dt);
            foreach (System.Data.DataRow dr in dt.Rows)
            {
                string sProject = (string)dr["Project"].ToString().Trim();
                if (sProject != "")
                {
                    i++;
                    if (i == 1) cbProject1.SelectedValue = sProject;
                    if (i == 2) cbProject2.SelectedValue = sProject;
                    if (i == 3) cbProject3.SelectedValue = sProject;
                    if (i == 4) cbProject4.SelectedValue = sProject;
                    if (i == 5) cbProject5.SelectedValue = sProject;
                    if (i == 6) cbProject6.SelectedValue = sProject;
                    if (i == 7) cbProject7.SelectedValue = sProject;
                    if (i == 8) cbProject8.SelectedValue = sProject;
                    if (i == 9) cbProject9.SelectedValue = sProject;
                    if (i == 10) cbProject10.SelectedValue = sProject;
                    if (i == 11) cbProject11.SelectedValue = sProject;
                    if (i == 12) cbProject12.SelectedValue = sProject;
                    if (i == 13) cbProject13.SelectedValue = sProject;
                    if (i == 14) cbProject14.SelectedValue = sProject;
                    if (i == 15) cbProject15.SelectedValue = sProject;
                    if (i == 16) cbProject16.SelectedValue = sProject;
                    if (i == 16) cbProject16.SelectedValue = sProject;
                    if (i == 18) cbProject18.SelectedValue = sProject;
                    if (i == 19) cbProject19.SelectedValue = sProject;
                    if (i == 20) cbProject20.SelectedValue = sProject;
                    if (i == 21) cbProject21.SelectedValue = sProject;
                    if (i == 22) cbProject22.SelectedValue = sProject;
                    if (i == 22) cbProject22.SelectedValue = sProject;
                    if (i == 23) cbProject23.SelectedValue = sProject;
                    if (i == 24) cbProject24.SelectedValue = sProject;
                    if (i == 25) cbProject25.SelectedValue = sProject;
                    if (i == 26) cbProject26.SelectedValue = sProject;
                    if (i == 27) cbProject27.SelectedValue = sProject;
                    if (i == 28) cbProject28.SelectedValue = sProject;
                    if (i == 29) cbProject29.SelectedValue = sProject;
                    if (i == 30) cbProject30.SelectedValue = sProject;
                    if (i == 31) cbProject31.SelectedValue = sProject;
                    if (i == 32) cbProject32.SelectedValue = sProject;
                    if (i == 33) cbProject33.SelectedValue = sProject;
                    if (i == 34) cbProject34.SelectedValue = sProject;
                    if (i == 35) cbProject35.SelectedValue = sProject;
                    if (i == 36) cbProject36.SelectedValue = sProject;
                    if (i == 37) cbProject37.SelectedValue = sProject;
                    if (i == 38) cbProject38.SelectedValue = sProject;
                    if (i == 39) cbProject39.SelectedValue = sProject;
                    if (i == 40) cbProject40.SelectedValue = sProject;
                    if (i == 41) cbProject41.SelectedValue = sProject;
                    if (i == 42) cbProject42.SelectedValue = sProject;
                    if (i == 43) cbProject43.SelectedValue = sProject;
                    if (i == 44) cbProject44.SelectedValue = sProject;
                    if (i == 45) cbProject45.SelectedValue = sProject;
                    if (i == 46) cbProject46.SelectedValue = sProject;
                    if (i == 47) cbProject47.SelectedValue = sProject;
                    if (i == 48) cbProject48.SelectedValue = sProject;
                    if (i == 49) cbProject49.SelectedValue = sProject;
                    if (i == 50) cbProject50.SelectedValue = sProject;
                }
            }
            //
            // Set up the rest of the information
            //
            dt = null;
            if (cdb.uspHonorMemorialCardsByCardID(iCardID, out dt))
            {
                foreach (System.Data.DataRow dr in dt.Rows)
                {
                    txtFromName.Text = (string)dr["FromName"].ToString().Trim();
                    txtToName.Text = (string)dr["ToName"].ToString().Trim();
                    txtToAddress1.Text = (string)dr["ToAddress1"].ToString().Trim();
                    txtToAddress2.Text = (string)dr["ToAddress2"].ToString().Trim();
                    txtToAddress3.Text = (string)dr["ToAddress3"].ToString().Trim();
                    txtToCity.Text = (string)dr["ToCity"].ToString().Trim();
                    txtToState.Text = (string)dr["ToState"].ToString().Trim();
                    txtToZip.Text = (string)dr["ToZip"].ToString().Trim();
                    txtToCountry.Text = (string)dr["ToCountry"].ToString().Trim();
                    txtInMemoryOf.Text = (string)dr["MemorialName"].ToString().Trim();
                    string sLetterCode = (string)dr["LetterCode"].ToString().Trim();
                    cbCardType.SelectedValue = sLetterCode;
                    bool bIsMemorial = false;
                    cdb.uspIsThisCardMemorial(sLetterCode, out bIsMemorial);

                    if (bIsMemorial)
                    {
                        txtInMemoryOf.IsEnabled = true;
                    }
                    else
                    {
                        txtInMemoryOf.IsEnabled = false;
                    }
                    //if (sLetterCode.Substring(0, 1) == "H")
                    //{
                    //    btnHonor.IsChecked = true;
                    //    txtInMemoryOf.IsEnabled = false;
                    //}
                    //else
                    //{
                    //    btnMemory.IsChecked = true;
                    //    txtInMemoryOf.IsEnabled = true;
                    //}
                    sReturnToSender = (string)dr["ReturnToSender"].ToString().Trim();
                    if (sReturnToSender.Substring(0, 1) == "Y")
                    {
                        chkReturnToDonor.IsChecked = true;
                    }
                    else
                    {
                        chkReturnToDonor.IsChecked = false;
                    }
                    cbStatus.Text = (string)dr["status"].ToString().Trim();

                }

            }
            bLoading = false;
        }
        private void ClearTheFields()
        {
            cbProject1.SelectedValue = null;
            cbProject2.SelectedValue = null;
            cbProject3.SelectedValue = null;
            cbProject4.SelectedValue = null;
            cbProject5.SelectedValue = null;
            cbProject6.SelectedValue = null;
            cbProject7.SelectedValue = null;
            cbProject8.SelectedValue = null;
            cbProject9.SelectedValue = null;
            cbProject10.SelectedValue = null;
            cbProject11.SelectedValue = null;
            cbProject12.SelectedValue = null;
            cbProject13.SelectedValue = null;
            cbProject14.SelectedValue = null;
            cbProject15.SelectedValue = null;
            cbProject16.SelectedValue = null;
            cbProject16.SelectedValue = null;
            cbProject18.SelectedValue = null;
            cbProject19.SelectedValue = null;
            cbProject20.SelectedValue = null;
            cbProject21.SelectedValue = null;
            cbProject22.SelectedValue = null;
            cbProject22.SelectedValue = null;
            cbProject23.SelectedValue = null;
            cbProject24.SelectedValue = null;
            cbProject25.SelectedValue = null;
            cbProject26.SelectedValue = null;
            cbProject27.SelectedValue = null;
            cbProject28.SelectedValue = null;
            cbProject29.SelectedValue = null;
            cbProject30.SelectedValue = null;
            cbProject31.SelectedValue = null;
            cbProject32.SelectedValue = null;
            cbProject33.SelectedValue = null;
            cbProject34.SelectedValue = null;
            cbProject35.SelectedValue = null;
            cbProject36.SelectedValue = null;
            cbProject37.SelectedValue = null;
            cbProject38.SelectedValue = null;
            cbProject39.SelectedValue = null;
            cbProject40.SelectedValue = null;
            cbProject41.SelectedValue = null;
            cbProject42.SelectedValue = null;
            cbProject43.SelectedValue = null;
            cbProject44.SelectedValue = null;
            cbProject45.SelectedValue = null;
            cbProject46.SelectedValue = null;
            cbProject47.SelectedValue = null;
            cbProject48.SelectedValue = null;
            cbProject49.SelectedValue = null;
            cbProject50.SelectedValue = null;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            cap.bCancelled = true;
            cap.SignalFormIsComplete();
            if (ExitForm != null)
                ExitForm(true);
        }

        //private void btnHonor_Click(object sender, RoutedEventArgs e)
        //{
        //    if ((bool)btnHonor.IsChecked)
        //    {
        //        txtInMemoryOf.IsEnabled = false;
        //        txtInMemoryOf.Text = "";
        //    }
        //    else
        //        txtInMemoryOf.IsEnabled = true;
        //}

        //private void btnMemory_Click(object sender, RoutedEventArgs e)
        //{
        //    if ((bool)btnMemory.IsChecked)
        //        txtInMemoryOf.IsEnabled = true;
        //    else
        //    {
        //        txtInMemoryOf.IsEnabled = false;
        //        txtInMemoryOf.Text = "";
        //    }
        //}

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            if (cap != null)
                cap.SignalFormIsComplete();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            bLoading = true;
        }

        private void cbStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = null ;
            string sNewChoice = "";
            string sNewChoiceCode = "";
            if (!bLoading)
            {
                foreach (ComboBoxItem item in e.AddedItems)
                {
                    if (item.IsSelected)
                        sNewChoice = item.Content.ToString();
                    break;
                }

                sNewChoiceCode = sNewChoice.DescriptionToStatusCode();
                switch (sNewChoiceCode)
                {
                    case "X":
                        //
                        // Handle deletion.
                        //
                        MessageBoxResult result = MessageBox.Show("This will mark this card for deletion.  Do you want to do this?", "Delete The Card", MessageBoxButton.YesNo, MessageBoxImage.Question);

                        // Process message box results
                        switch (result)
                        {
                            case MessageBoxResult.Yes:
                                break;
                            case MessageBoxResult.No:
                                cb = (ComboBox)sender;
                                bLoading = true;
                                cb.SelectedItem = e.RemovedItems[0];
                                bLoading = false;
                                break;
                        }
                        break;
                    case "I":
                    case "P":
                        //
                        // Can't use codes In progress or Printed.
                        //
                        MessageBox.Show("You cannot manually set this status - " + sNewChoice);
                        cb = (ComboBox)sender;
                        bLoading = true;
                        cb.SelectedItem = e.RemovedItems[0];
                        bLoading = false;
                        break;

                }
            }
        }

        private void cbCardType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedValue != null)
            {
                string sLetterCode = (sender as ComboBox).SelectedValue.ToString();
                clsDatabase cdb = new clsDatabase();
                bool bIsMemorial = false;
                cdb.uspIsThisCardMemorial(sLetterCode, out bIsMemorial);
                if (bIsMemorial)
                {
                    txtInMemoryOf.IsEnabled = true;
                }
                else
                {
                    txtInMemoryOf.IsEnabled = false;
                }
            }
            

        }
    }

}
