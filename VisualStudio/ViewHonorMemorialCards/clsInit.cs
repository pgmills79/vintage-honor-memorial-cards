﻿using System;
using System.Xml;
using System.Windows;
using MyCode;

namespace MyCode
{

    public class clsIni
    {
        public enum Environment
        {
            Production = 1,
            Development = 2,
            Train = 3,
        }

        private string sApplicationServer;
        public string SApplicationServer
        {
            get { return sApplicationServer; }
        }

        private string sApplicationDatabase;
        public string SApplicationDatabase
        {
            get { return sApplicationDatabase; }
        }

        private string sEnvironment;
        public string SEnvironment
        {
            get { return sEnvironment; }
        }

        private string sHonorMemorialReport;
        public string SHonorMemorialReport
        {
            get { return sHonorMemorialReport; }
        }
        private string sReportServer;
        public string SReportServer
        {
            get { return sReportServer; }
        }
        private bool bSuccess;
        public bool BSuccess
        {
            get { return bSuccess; }
        }

        private string sProdApplicationServer;
        private string sProdApplicationDatabase;
        private string sProdEnvironment;
        private string sProdReportServer;

        private string sDevApplicationServer;
        private string sDevApplicationDatabase;
        private string sDevEnvironment;
        private string sDevReportServer;

        private string sTrainApplicationServer;
        private string sTrainApplicationDatabase;
        private string sTrainEnvironment;
        private string sTrainReportServer;

        public clsIni()
        {
            sApplicationDatabase = "";
            sApplicationServer = "";
            sEnvironment = "";
            sReportServer = "";
            bSuccess = LoadConfigurationParameters();
            if (bSuccess)
            {
                if (clsEnvironment.DSEnv == clsEnvironment.DSEnvironment.Development)
                {
                    sApplicationServer = sDevApplicationServer;
                    sApplicationDatabase = sDevApplicationDatabase;
                    sEnvironment = sDevEnvironment;
                    sReportServer = sDevReportServer;
                }

                if (clsEnvironment.DSEnv == clsEnvironment.DSEnvironment.Production)
                {
                    sApplicationServer = sProdApplicationServer;
                    sApplicationDatabase = sProdApplicationDatabase;
                    sEnvironment = sProdEnvironment;
                    sReportServer = sProdReportServer;
                }

                if (clsEnvironment.DSEnv == clsEnvironment.DSEnvironment.Training)
                {
                    sApplicationServer = sTrainApplicationServer;
                    sApplicationDatabase = sTrainApplicationDatabase;
                    sEnvironment = sTrainEnvironment;
                    sReportServer = sTrainReportServer;
                }


            }
        }
        ~clsIni()
        {
        }
        private bool LoadConfigurationParameters()
        {

            bool bStatus;

            bStatus = GetOneItem("ProdApplicationServer", out sProdApplicationServer);
            if (bStatus)
                bStatus = GetOneItem("ProdApplicationDatabase", out sProdApplicationDatabase);
            if (bStatus)
                bStatus = GetOneItem("ProdEnvironment", out sProdEnvironment);
            if (bStatus)
                bStatus = GetOneItem("ProdReportServer", out sProdReportServer);

            if (bStatus)
                bStatus = GetOneItem("DevApplicationServer", out sDevApplicationServer);
            if (bStatus)
                bStatus = GetOneItem("DevApplicationDatabase", out sDevApplicationDatabase);
            if (bStatus)
                bStatus = GetOneItem("DevEnvironment", out sDevEnvironment);
            if (bStatus)
                bStatus = GetOneItem("DevReportServer", out sDevReportServer);

            if (bStatus)
                bStatus = GetOneItem("TrainApplicationServer", out sTrainApplicationServer);
            if (bStatus)
                bStatus = GetOneItem("TrainApplicationDatabase", out sTrainApplicationDatabase);
            if (bStatus)
                bStatus = GetOneItem("TrainEnvironment", out sTrainEnvironment);
            if (bStatus)
                bStatus = GetOneItem("TrainReportServer", out sTrainReportServer);

            if (bStatus)
                bStatus = GetOneItem("HonorMemorialReport", out sHonorMemorialReport);

            return bStatus;
        }
        private bool GetOneItem(string sItem, out string sValue)
        //
        //    Get one SQL item from the SQL file.
        //    
        {
            bool sFound;

            sValue = "";
            sFound = false;
            XmlDocument xDoc = new XmlDocument();
            try
            {
                string sStartupDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                string sFile = sStartupDirectory.Trim().Replace(@"file:\", "") + @"\ViewHonorMemorialCards.xml";
                xDoc.Load(sFile);
                XmlNodeList XmlList = xDoc.GetElementsByTagName(sItem);
                foreach (XmlNode XmlNode in XmlList)
                {
                    string sParent = XmlNode.ParentNode.Name;
                    if (sParent.ToLower() == "initialization")
                    {
                        sValue = XmlNode.InnerText;
                        sFound = true;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to process initialization file - " + e.ToString());
            }
            return sFound;
        }
    }

}

