﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Reflection;
using System.Threading;
using MyCode;
using System.Windows;
using System.Windows.Threading;
using System.Security.Principal;

namespace MyCode
{
    public class clsDatabase
    {
        private const string sUserName = "spapps";
        private const string sPassword = "sampurse";
        private string _sServer;
        private string _sDatabase;
        private bool bSuccess;
        public bool BSuccess
        {
            get { return bSuccess; }
        }

        private void GetConnection(out SqlConnection conn)
        {
            clsIni ci = new clsIni();
            bSuccess = true;
            _sServer = ci.SApplicationServer;
            _sDatabase = ci.SApplicationDatabase;
            ci = null;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.Add("Server", _sServer);
            builder.Add("Database", _sDatabase);
            builder.Add("UID", sUserName);
            builder.Add("PWD", sPassword);
            //builder.Add("Connection Timeout", 3600);
            builder.Add("Connection Timeout", 0);
            conn = new SqlConnection(builder.ConnectionString);
        }

        public clsDatabase()
        {
        }

        ~clsDatabase()
        {

        }

        //
        // Do events
        //
        private void DoEvents()
        {
            DispatcherFrame f = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
            (SendOrPostCallback)delegate(object arg)
            {
                DispatcherFrame fr = arg as DispatcherFrame;
                fr.Continue = false;
            }, f);
            Dispatcher.PushFrame(f);
            //Thread.Sleep(1000);
        }

        public bool uspSearchHonorMemorialCards(long lAccountNumber,
                                                DateTime dBeginDate,
                                                DateTime dEndDate,
                                                string sPrintStatus,
                                                int lGroup,
                                                int lOf,
                                                string sFromName,
                                                string sToName,
                                                string sMemorialName,
                                                string sAddress1,
                                                string sCity,
                                                string sZip,
                                                int iBatchNumber,
                                                out int lRowsReturned,
                                                out bool bMaxLimitWarning,
                                                out DataTable dt)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            long lStatus = 0;
            lRowsReturned = 0;
            bMaxLimitWarning = false;
            DataSet ds = null;
            dt = null;
            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspSearchHonorMemorialCardsDonorStudio2";
                //sProcedure = _sDatabase == "SPAPPSDEV" ? "uspSearchHonorMemorialCardsDonorStudio" : "uspSearchHonorMemorialCards";
                ds = new DataSet();

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@AccountNumber", SqlDbType.BigInt);
                cmdRunSP.Parameters["@AccountNumber"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@AccountNumber"].Value = lAccountNumber;

                cmdRunSP.Parameters.Add("@BeginDate", SqlDbType.DateTime);
                cmdRunSP.Parameters["@BeginDate"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@BeginDate"].Value = dBeginDate;

                cmdRunSP.Parameters.Add("@EndDate", SqlDbType.DateTime);
                cmdRunSP.Parameters["@EndDate"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@EndDate"].Value = dEndDate;

                cmdRunSP.Parameters.Add("@PrintStatus", SqlDbType.Char, 1);
                cmdRunSP.Parameters["@PrintStatus"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@PrintStatus"].Value = sPrintStatus;

                cmdRunSP.Parameters.Add("@Group", SqlDbType.Int);
                cmdRunSP.Parameters["@Group"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@Group"].Value = lGroup;

                cmdRunSP.Parameters.Add("@Of", SqlDbType.Int);
                cmdRunSP.Parameters["@Of"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@Of"].Value = lOf;

                cmdRunSP.Parameters.Add("@FromName", SqlDbType.VarChar, 20);
                cmdRunSP.Parameters["@FromName"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@FromName"].Value = sFromName;

                cmdRunSP.Parameters.Add("@ToName", SqlDbType.VarChar, 20);
                cmdRunSP.Parameters["@ToName"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@ToName"].Value = sToName;

                cmdRunSP.Parameters.Add("@MemorialName", SqlDbType.VarChar, 20);
                cmdRunSP.Parameters["@MemorialName"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@MemorialName"].Value = sMemorialName;

                cmdRunSP.Parameters.Add("@Address1", SqlDbType.VarChar, 20);
                cmdRunSP.Parameters["@Address1"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@Address1"].Value = sAddress1;

                cmdRunSP.Parameters.Add("@City", SqlDbType.VarChar, 20);
                cmdRunSP.Parameters["@City"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@City"].Value = sCity;

                cmdRunSP.Parameters.Add("@Zip", SqlDbType.VarChar, 20);
                cmdRunSP.Parameters["@Zip"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@Zip"].Value = sZip;

                cmdRunSP.Parameters.Add("@BatchNumber", SqlDbType.Int, 10);
                cmdRunSP.Parameters["@BatchNumber"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@BatchNumber"].Value = iBatchNumber;

                cmdRunSP.Parameters.Add("@RowsReturned", SqlDbType.Int);
                cmdRunSP.Parameters["@RowsReturned"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@MaxLimitWarning", SqlDbType.Int);
                cmdRunSP.Parameters["@MaxLimitWarning"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 3000);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmdRunSP;
                adapter.Fill(ds, "GRID");
                dt = ds.Tables["GRID"];

                lRowsReturned = (int)cmdRunSP.Parameters["@RowsReturned"].Value;
                int lMaxLimitWarning = (int)cmdRunSP.Parameters["@MaxLimitWarning"].Value;
                if (lMaxLimitWarning == 0)
                    bMaxLimitWarning = false;
                else
                    bMaxLimitWarning = true;
                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;
                if (lStatus != 0)
                {
                    MessageBox.Show("Could not get data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not get data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }
        public bool uspUpdateHonorMemorialCards(long lCardID,
                                        string sFromName,
                                        string sToName,
                                        string sMemorialName,
                                        string sToAddress1,
                                        string sToAddress2,
                                        string sToAddress3,
                                        string sToCity,
                                        string sToState,
                                        string sToZip,
                                        string sToCountry,
                                        string sNewStatus,
                                        out string sUpdatedStatus,
                                        out bool bRecordUpdated,
                                        out string sChangedName,
                                        out string sChangedDate)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            string sWarningMessage = "";
            string sUserName = "";
            string sGroupName = "";
            long lStatus = 0;
            sUpdatedStatus = "";
            bRecordUpdated = false;
            sChangedName = "";
            sChangedDate = "";

            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspUpdateHonorMemorialCardsDonorStudio";
                //sProcedure = _sDatabase == "SPAPPSDEV" ? "uspUpdateHonorMemorialCardsDonorStudio" : "uspUpdateHonorMemorialCards";

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@CardID", SqlDbType.Int);
                cmdRunSP.Parameters["@CardID"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@CardID"].Value = lCardID;

                cmdRunSP.Parameters.Add("@FromName", SqlDbType.VarChar, 50);
                cmdRunSP.Parameters["@FromName"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@FromName"].Value = sFromName;

                cmdRunSP.Parameters.Add("@ToName", SqlDbType.VarChar, 50);
                cmdRunSP.Parameters["@ToName"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@ToName"].Value = sToName;

                cmdRunSP.Parameters.Add("@MemorialName", SqlDbType.VarChar, 50);
                cmdRunSP.Parameters["@MemorialName"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@MemorialName"].Value = sMemorialName;

                cmdRunSP.Parameters.Add("@ToAddress1", SqlDbType.VarChar, 70);
                cmdRunSP.Parameters["@ToAddress1"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@ToAddress1"].Value = sToAddress1;

                cmdRunSP.Parameters.Add("@ToAddress2", SqlDbType.VarChar, 70);
                cmdRunSP.Parameters["@ToAddress2"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@ToAddress2"].Value = sToAddress2;

                cmdRunSP.Parameters.Add("@ToAddress3", SqlDbType.VarChar, 70);
                cmdRunSP.Parameters["@ToAddress3"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@ToAddress3"].Value = sToAddress3;

                cmdRunSP.Parameters.Add("@ToCity", SqlDbType.VarChar, 40);
                cmdRunSP.Parameters["@ToCity"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@ToCity"].Value = sToCity;

                cmdRunSP.Parameters.Add("@ToState", SqlDbType.VarChar, 10);
                cmdRunSP.Parameters["@ToState"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@ToState"].Value = sToState;

                cmdRunSP.Parameters.Add("@ToZip", SqlDbType.VarChar, 15);
                cmdRunSP.Parameters["@ToZip"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@ToZip"].Value = sToZip;

                cmdRunSP.Parameters.Add("@ToCountry", SqlDbType.VarChar, 10);
                cmdRunSP.Parameters["@ToCountry"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@ToCountry"].Value = sToCountry;

                cmdRunSP.Parameters.Add("@NewStatus", SqlDbType.Char, 1);
                cmdRunSP.Parameters["@NewStatus"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@NewStatus"].Value = sNewStatus;

                GetuserName(out sUserName, out sGroupName);
                cmdRunSP.Parameters.Add("@UserName", SqlDbType.VarChar, 50);
                cmdRunSP.Parameters["@UserName"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@UserName"].Value = sUserName;

                cmdRunSP.Parameters.Add("@UpdatedStatus", SqlDbType.VarChar, 10);
                cmdRunSP.Parameters["@UpdatedStatus"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@RecordUpdated", SqlDbType.Int);
                cmdRunSP.Parameters["@RecordUpdated"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@NewChangedName", SqlDbType.VarChar, 50);
                cmdRunSP.Parameters["@NewChangedName"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@NewChangedDate", SqlDbType.VarChar, 20);
                cmdRunSP.Parameters["@NewChangedDate"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@WarningMessage", SqlDbType.VarChar, 3000);
                cmdRunSP.Parameters["@WarningMessage"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 3000);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                cmdRunSP.ExecuteNonQuery();

                sUpdatedStatus = (string)cmdRunSP.Parameters["@UpdatedStatus"].Value;
                int lRecordUpdated = (int)cmdRunSP.Parameters["@RecordUpdated"].Value;
                bRecordUpdated = (lRecordUpdated == 0 ? false : true);
                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;
                sWarningMessage = (string)cmdRunSP.Parameters["@WarningMessage"].Value;
                sChangedDate = (string)cmdRunSP.Parameters["@NewChangedDate"].Value;
                sChangedName = (string)cmdRunSP.Parameters["@NewChangedName"].Value;

                if (lStatus != 0)
                {
                    MessageBox.Show("Could not get data - " + sErrorMessage);
                    bSuccess = false;
                }
                else
                {
                    if (sWarningMessage != "")
                    {
                        MessageBox.Show(sWarningMessage);
                        bSuccess = false;
                    }
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not get data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }
        private void GetuserName(out string sUserName, out string sGroupName)
        {
            int i;
            string sName;
            WindowsPrincipal wp = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
            sName = wp.Identity.Name;
            sUserName = "";
            sGroupName = "";
            string[] saWords = sName.Split('\\');
            i = 0;
            foreach (string sWord in saWords)
            {
                i++;
                if (i == 1) sGroupName = sWord;
                if (i == 2) sUserName = sWord;
                if (i > 2) break;
            }
        }
       
        public bool uspGetGiftsOfCard(long lCardID, out DataTable dt)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            long lStatus = 0;

            DataSet ds = null;
            dt = null;
            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspGetGiftsOfCardDonorStudio";
                //sProcedure = _sDatabase == "SPAPPSDEV" ? "uspGetGiftsOfCardDonorStudio" : "uspGetGiftsOfCard";
                ds = new DataSet();

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@CardID", SqlDbType.Int);
                cmdRunSP.Parameters["@CardID"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@CardID"].Value = lCardID;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 256);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmdRunSP;
                adapter.Fill(ds, "LIST");
                dt = ds.Tables["LIST"];

                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;
                if (lStatus != 0)
                {
                    MessageBox.Show("Could not get data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not get data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }

        public bool uspGetTypeOfCard(long lCardID, out string cCardType, out string cReturnToSender, out string sInMemorialOf)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            cCardType = "";
            cReturnToSender = "";
            sInMemorialOf = "";
            long lStatus = 0;

            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspGetTypeOfCardDonorStudio";
                //sProcedure = _sDatabase == "SPAPPSDEV" ? "uspGetTypeOfCardDonorStudio" : "uspGetTypeOfCard";

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@CardID", SqlDbType.Int);
                cmdRunSP.Parameters["@CardID"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@CardID"].Value = lCardID;

                cmdRunSP.Parameters.Add("@CardType", SqlDbType.Char, 1);
                cmdRunSP.Parameters["@CardType"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@InMemorialOf", SqlDbType.VarChar, 50);
                cmdRunSP.Parameters["@InMemorialOf"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ReturnToSender", SqlDbType.Char, 1);
                cmdRunSP.Parameters["@ReturnToSender"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 256);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                cmdRunSP.ExecuteNonQuery();

                cCardType = (string)cmdRunSP.Parameters["@CardType"].Value;
                sInMemorialOf = (string)cmdRunSP.Parameters["@InMemorialOf"].Value;
                cReturnToSender = (string)cmdRunSP.Parameters["@ReturnToSender"].Value;
                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;
                if (lStatus != 0)
                {
                    MessageBox.Show("Could not get data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not get data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }

        
        public bool uspPutGiftsOfCard(long lCardID,
                                string sProject,
                                ref string sGuid )
        {
            string sProcedure = "";
            string sErrorMessage = "";
            long lStatus = 0;

            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspPutGiftsOfCardDonorStudio";
                //sProcedure = _sDatabase == "SPAPPSDEV" ? "uspPutGiftsOfCardDonorStudio" : "uspPutGiftsOfCard";

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@CardID", SqlDbType.Int);
                cmdRunSP.Parameters["@CardID"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@CardID"].Value = lCardID;

                cmdRunSP.Parameters.Add("@Project", SqlDbType.VarChar, 10);
                cmdRunSP.Parameters["@Project"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@Project"].Value = sProject;

                cmdRunSP.Parameters.Add("@Guid", SqlDbType.VarChar, 200);
                cmdRunSP.Parameters["@Guid"].Direction = ParameterDirection.InputOutput;
                cmdRunSP.Parameters["@Guid"].Value = sGuid;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 256);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                cmdRunSP.ExecuteNonQuery();

                sGuid = (string)cmdRunSP.Parameters["@Guid"].Value;
                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;

                if (lStatus != 0)
                {
                    MessageBox.Show("Could not put data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not put data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }

        public bool uspCommitGiftCardProject(long lCardID,
                                             string sGuid,
                                             out string sGiftLine)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            long lStatus = 0;

            sGiftLine = "";
            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspCommitGiftCardProjectDonorStudio";
                //sProcedure = _sDatabase == "SPAPPSDEV" ? "uspCommitGiftCardProjectDonorStudio" : "uspCommitGiftCardProject";

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@CardID", SqlDbType.Int);
                cmdRunSP.Parameters["@CardID"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@CardID"].Value = lCardID;

                cmdRunSP.Parameters.Add("@Guid", SqlDbType.VarChar, 200);
                cmdRunSP.Parameters["@Guid"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@Guid"].Value = sGuid;

                cmdRunSP.Parameters.Add("@GiftLine", SqlDbType.VarChar, 2048);
                cmdRunSP.Parameters["@GiftLine"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 256);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                cmdRunSP.ExecuteNonQuery();

                sGiftLine = (string)cmdRunSP.Parameters["@GiftLine"].Value;
                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;

                if (lStatus != 0)
                {
                    MessageBox.Show("Could not commit data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not commit data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }
        public bool uspGetListOfValidCardProjects(out DataTable dt)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            long lStatus = 0;

            DataSet ds = null;
            dt = null;
            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspGetListOfValidCardProjects";       // Didn't need conversion. 8/10/2011
                ds = new DataSet();

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 256);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmdRunSP;
                adapter.Fill(ds, "LIST");
                dt = ds.Tables["LIST"];

                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;
                if (lStatus != 0)
                {
                    MessageBox.Show("Could not get data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not get data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }
        public bool uspGetListHonorMemorialCards(out DataTable dt)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            long lStatus = 0;

            DataSet ds = null;
            dt = null;
            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspGetListHonorMemorialCards";       // Didn't need conversion. 8/10/2011
                ds = new DataSet();

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 256);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmdRunSP;
                adapter.Fill(ds, "LIST");
                dt = ds.Tables["LIST"];

                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;
                if (lStatus != 0)
                {
                    MessageBox.Show("Could not get data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not get data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }


        public bool uspGetDocumentProducts(int lAccountNumber, out DataTable dt)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            long lStatus = 0;

            DataSet ds = null;
            dt = null;
            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspGetDocumentProducts";              // No need to convert 8/10/2011  
                ds = new DataSet();

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@AccountNumber", SqlDbType.Int);
                cmdRunSP.Parameters["@AccountNumber"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@AccountNumber"].Value = lAccountNumber;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 256);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmdRunSP;
                adapter.Fill(ds, "LIST");
                dt = ds.Tables["LIST"];

                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;
                if (lStatus != 0)
                {
                    MessageBox.Show("Could not get data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not get data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }

        public bool uspCommitTypeOfCard(long lCardID,
                                         string sCardType,
                                         string sInMemorialOf,
                                         string sReturnToSender)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            string sUserName;
            string sGroupName;
            long lStatus = 0;
            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspCommitTypeOfCardDonorStudio2";
                //sProcedure = _sDatabase == "SPAPPSDEV" ? "uspCommitTypeOfCardDonorStudio" : "uspCommitTypeOfCard";

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@CardID", SqlDbType.Int);
                cmdRunSP.Parameters["@CardID"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@CardID"].Value = lCardID;

                cmdRunSP.Parameters.Add("@CardType", SqlDbType.VarChar, 10);
                cmdRunSP.Parameters["@CardType"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@CardType"].Value = sCardType;

                cmdRunSP.Parameters.Add("@InMemorialOf", SqlDbType.VarChar, 50);
                cmdRunSP.Parameters["@InMemorialOf"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@InMemorialOf"].Value = sInMemorialOf;

                GetuserName(out sUserName, out sGroupName);
                cmdRunSP.Parameters.Add("@UserName", SqlDbType.VarChar, 50);
                cmdRunSP.Parameters["@UserName"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@UserName"].Value = sUserName;

                cmdRunSP.Parameters.Add("@ReturnToSender", SqlDbType.Char, 1);
                cmdRunSP.Parameters["@ReturnToSender"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@ReturnToSender"].Value = sReturnToSender;
                
                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 256);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                cmdRunSP.ExecuteNonQuery();

                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;

                if (lStatus != 0)
                {
                    MessageBox.Show("Could not commit data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not commit data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }
        public bool uspAddHonorMemorialCard(long lDocumentNumber,
                                 out long lCardID)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            string sUserName="";
            string sGroupName="";
            long lStatus = 0;
            lCardID = 0;

            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspAddHonorMemorialCardWithUserDonorStudio";
                //sProcedure = _sDatabase == "SPAPPSDEV" ? "uspAddHonorMemorialCardWithUserDonorStudio" : "uspAddHonorMemorialCardWithUser";

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@DocumentNumber", SqlDbType.BigInt);
                cmdRunSP.Parameters["@DocumentNumber"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@DocumentNumber"].Value = lDocumentNumber;

                GetuserName(out sUserName, out sGroupName);
                cmdRunSP.Parameters.Add("@UserName", SqlDbType.VarChar, 30);
                cmdRunSP.Parameters["@UserName"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@UserName"].Value = sUserName;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@CardNumber", SqlDbType.Int);
                cmdRunSP.Parameters["@CardNumber"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 256);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                cmdRunSP.ExecuteNonQuery();

                lCardID = (int)cmdRunSP.Parameters["@CardNumber"].Value;
                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;

                if (lStatus != 0)
                {
                    MessageBox.Show("Could not commit data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                lCardID = 0;
                MessageBox.Show("Could not commit data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }
        public bool uspCheckCardDocumentNumber(long lDocumentNumber,
                         out long lAccountNumber,
                         out string sAccountName)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            long lStatus = 0;
            lAccountNumber = 0;
            sAccountName = "";

            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspCheckCardDocumentNumber";              // No conversion.  

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@DocumentNumber", SqlDbType.BigInt);
                cmdRunSP.Parameters["@DocumentNumber"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@DocumentNumber"].Value = lDocumentNumber;

                cmdRunSP.Parameters.Add("@AccountNumber", SqlDbType.BigInt);
                cmdRunSP.Parameters["@AccountNumber"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@AccountName", SqlDbType.VarChar,80);
                cmdRunSP.Parameters["@AccountName"].Direction = ParameterDirection.Output;
                
                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 256);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                cmdRunSP.ExecuteNonQuery();

                long.TryParse(cmdRunSP.Parameters["@AccountNumber"].Value.ToString(), out  lAccountNumber);
                sAccountName = (string)cmdRunSP.Parameters["@AccountName"].Value;
                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;

                if (lStatus != 0)
                {
                    MessageBox.Show("Could not check data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                lAccountNumber = 0;
                sAccountName = "";
                MessageBox.Show("Could not commit data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }

        public bool uspHonorMemorialCardsByCardID(long lCardID,
                                        out DataTable dt)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            long lStatus = 0;
            DataSet ds = null;
            dt = null;
            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspHonorMemorialCardsByCardIDDonorStudio";
                //sProcedure = _sDatabase == "SPAPPSDEV" ? "uspHonorMemorialCardsByCardIDDonorStudio" : "uspHonorMemorialCardsByCardID";

                ds = new DataSet();

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@CardID", SqlDbType.BigInt);
                cmdRunSP.Parameters["@CardID"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@CardID"].Value = lCardID;


                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 3000);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmdRunSP;
                adapter.Fill(ds, "GRID");
                dt = ds.Tables["GRID"];

                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;
                if (lStatus != 0)
                {
                    MessageBox.Show("Could not get data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not get data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }

        public bool uspIsThisCardMemorial(string lCard, out bool bIsMemorial)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            long lStatus = 0;
            bIsMemorial = false;

            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspIsThisCardMemorial";       // Didn't need conversion. 8/10/2011

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@LetterCode", SqlDbType.VarChar, 10);
                cmdRunSP.Parameters["@LetterCode"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@LetterCode"].Value = lCard;

                cmdRunSP.Parameters.Add("@IsMemorial", SqlDbType.Int);
                cmdRunSP.Parameters["@IsMemorial"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 256);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                cmdRunSP.ExecuteNonQuery();

                int i = 0;
                i = (int)cmdRunSP.Parameters["@IsMemorial"].Value;
                bIsMemorial = (i == 1 ? true : false);

                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;
                if (lStatus != 0)
                {
                    MessageBox.Show("Could not get data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not get data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }


    }
}

