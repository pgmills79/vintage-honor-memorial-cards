﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using System.IO;
using System.Data;
using System.Windows;
using System.Windows.Threading;
using System.Threading;
using System.Windows.Controls;

namespace MyCode
{
    class clsWriteACECSV
    {
        private TextWriter tw;
        private bool bFileOpen;

        public clsWriteACECSV()
        {
            bFileOpen = false;
        }
        public bool OpenFile(string sfileName)
        {
            bool bSuccess = true;
            try
            {
                bSuccess = true;
                tw = new StreamWriter(sfileName);
                bFileOpen = true;
            }
            catch (Exception e)
            {
                bSuccess = false;
                MessageBox.Show("Unable to open file - " + e.Message);
            }
            return (bSuccess);
        }
        public bool CloseFile()
        {
            bool bSuccess = true;
            try
            {
                if (bFileOpen)
                {
                    tw.Close();
                }
            }
            catch (Exception e)
            {
                bSuccess = false;
                MessageBox.Show("Unable to close file - " + e.Message);
            }
            finally
            {
                tw = null;
            }
            return (bSuccess);
        }
        public bool WriteFile(DataSet ds)
        {
            bool bStatus = true;
            string sLine;
            long i = 0;

            DoEvents();
            try
            {
                sLine = "CardID," +
                        "PrintGroup," +
                        "AddressLine1," +
                        "AddressLine2," +
                        "AddressLine3," +
                        "AddressLine4," +
                        "City," +
                        "State," +
                        "ZipPostal," +
                        "Country";
                //tw.WriteLine(sLine);
                foreach (DataRow dr in ds.Tables["CSV"].Rows)
                {
                    sLine = "";
                    sLine = sLine + AddFieldBigInt((string)dr["CardID"].ToString()) + ",";
                    sLine = sLine + AddFieldBigInt((string)dr["PrintGroup"].ToString()) + ",";
                    sLine = sLine + AddFieldString((string)dr["AddressLine1"]) + ",";
                    sLine = sLine + AddFieldString((string)dr["AddressLine2"]) + ",";
                    sLine = sLine + AddFieldString((string)dr["AddressLine3"]) + ",";
                    sLine = sLine + AddFieldString((string)dr["AddressLine4"]) + ",";
                    sLine = sLine + AddFieldString((string)dr["City"]) + ",";
                    sLine = sLine + AddFieldString((string)dr["State"]) + ",";
                    sLine = sLine + AddFieldString((string)dr["ZipPostal"]) + ",";
                    sLine = sLine + AddFieldString((string)dr["Country"]) + ",";
                    tw.WriteLine(sLine);
                    i++;
                    if ((i % 1000) == 0)
                    {
                        DoEvents();
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not write to file - " + e.ToString());
                bStatus = false;
            }
            return (bStatus);
        }
        private string AddFieldBigInt(string s)
        {
            long l;
            if (!long.TryParse(s, out l))
            {
                l = 0;
            }
            return (AddFieldString(l.ToString()));
        }
        private string AddFieldMoney(string s)
        {

            decimal f;
            if (!decimal.TryParse(s, out f))
            {
                f = 0;
            }
            return (AddFieldString("$" + String.Format("{0:0.00}", f).Trim()));
        }
        private string AddFieldString(string s)
        {
            string stempString1;
            string stempString2;
            stempString1 = s;
            for (int i = 0; i < stempString1.Length; i++)
            {
                if ((stempString1[i] < ' ') || ((stempString1[i] > '~')))
                {
                    stempString2 = stempString1.Replace(stempString1[i], ' ');
                    stempString1 = stempString2;
                }

                if (stempString1[i] == '"')
                {
                    stempString2 = stempString1.Replace(stempString1[i], '\'');
                    stempString1 = stempString2;
                }
            }
            return ("\"" + s + "\"");
        }
        //
        // Do events
        //
        private void DoEvents()
        {
            DispatcherFrame f = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
            (SendOrPostCallback)delegate(object arg)
            {
                DispatcherFrame fr = arg as DispatcherFrame;
                fr.Continue = false;
            }, f);
            Dispatcher.PushFrame(f);
            //Thread.Sleep(1000);
        }


    }
}