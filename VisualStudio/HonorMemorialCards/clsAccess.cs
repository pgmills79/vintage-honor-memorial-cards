﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using System.Reflection;
using System.Threading;
using MyCode;
using System.Windows;
using System.Windows.Threading;
using System.Security.Principal;

namespace MyCode
{
    public class clsAccess
    {
        private string _sAccessDatabase;
        private bool bSuccess;
        public bool BSuccess
        {
            get { return bSuccess; }
        }

        private void GetConnection(out OleDbConnection conn, bool bAsync)
        {
            //SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            string cs = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + "\"" + _sAccessDatabase + "\"" + @";Persist Security Info=False;";
            conn = new OleDbConnection(cs);
        }

        public clsAccess()
        {
            clsIni ci = new clsIni();
            bSuccess = true;
            _sAccessDatabase = ci.SAccessDatabase;
        }

        ~clsAccess()
        {

        }
        public bool DeleteRecords()
        {
            //
            // This calls the stored procedure that loads the staging table.
            //
            OleDbConnection conn = null;
            bool bSuccess = true;
            try
            {
                OleDbCommand cmdLoadStaging;
                GetConnection(out conn, true);
                conn.Open();
                cmdLoadStaging = new OleDbCommand("delete from HonorMemorialCards;", conn);
                cmdLoadStaging.CommandTimeout = 3600;
                cmdLoadStaging.CommandType = CommandType.Text;
                cmdLoadStaging.ExecuteNonQuery();
                cmdLoadStaging = null;
                cmdLoadStaging = new OleDbCommand("delete from CardLabels;", conn);
                cmdLoadStaging.CommandTimeout = 3600;
                cmdLoadStaging.CommandType = CommandType.Text;
                cmdLoadStaging.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                bSuccess = false;
                MessageBox.Show("Unable to delete records: - " + e.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
                conn = null;
            }
            return (bSuccess);
        }
        public bool WriteHonorMemorialLabels(DataSet ds)
        {
            string sSQL;
            //
            // This calls the stored procedure that loads the staging table.
            //
            OleDbConnection conn = null;
            bool bSuccess = true;
            try
            {
                OleDbCommand cmdLoadStaging;
                GetConnection(out conn, true);
                conn.Open();
                foreach (DataRow dr in ds.Tables["LIST"].Rows)
                {
                    sSQL = @"insert into CardLabels " +
                           @"(AccountNumber, [Type], [FileName], Line1, Line2, Line3, Line4, Line5, Line6) " +
                           @"values " +
                           String.Format(@"({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')",
                                        (string)dr["AccountNumber"].ToString(),
                                        (string)dr["Type"].ToString(),
                                        (string)dr["FileName"],
                                        (string)dr["Line1"].ToString().Replace("'", "''"),
                                        (string)dr["Line2"].ToString().Replace("'", "''"),
                                        (string)dr["Line3"].ToString().Replace("'", "''"),
                                        (string)dr["Line4"].ToString().Replace("'", "''"),
                                        (string)dr["Line5"].ToString().Replace("'", "''"),
                                        (string)dr["Line6"].ToString().Replace("'", "''"));
                    cmdLoadStaging = new OleDbCommand(sSQL, conn);
                    cmdLoadStaging.CommandTimeout = 3600;
                    cmdLoadStaging.CommandType = CommandType.Text;
                    cmdLoadStaging.ExecuteNonQuery();
                    cmdLoadStaging = null;
                }
            }
            catch (Exception e)
            {
                bSuccess = false;
                MessageBox.Show("Unable to write label record: - " + e.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
                conn = null;
            }
            return (bSuccess);
        }
    
        public bool WriteHonorMemorialCards(DataSet ds)
        {
            string sSQL;
            //
            // This calls the stored procedure that loads the staging table.
            //
            OleDbConnection conn = null;
            bool bSuccess = true;
            try
            {
                OleDbCommand cmdLoadStaging;
                GetConnection(out conn, true);
                conn.Open();
                foreach (DataRow dr in ds.Tables["LIST"].Rows)
                {
                    sSQL = @"insert into HonorMemorialCards " +
                           @"(PrintGroup, CardID, DocumentNumber, AccountNumber, [Type], [FileName], " +
                           @"DateGenerated, FromName, MemorialName, ToName, " +
                           @"ToAddress1, ToAddress2, ToAddress3, ToAddress4, ToAddress5, " +
                           @"StdError, " +
                           @"Project01, ShortDescription01, LongDescription01, " +
                           @"Project02, ShortDescription02, LongDescription02, " +
                           @"Project03, ShortDescription03, LongDescription03, " +
                           @"Project04, ShortDescription04, LongDescription04, " +
                           @"Project05, ShortDescription05, LongDescription05, " +
                           @"Project06, ShortDescription06, LongDescription06, " +
                           @"Project07, ShortDescription07, LongDescription07, " +
                           @"Project08, ShortDescription08, LongDescription08, " +
                           @"Project09, ShortDescription09, LongDescription09, " +
                           @"Project10, ShortDescription10, LongDescription10, " +
                           @"CardNumber, OfNumber, CardTag) " +
                           @"values " +
                           String.Format(@"({0}, {1}, {2}, {3}, {4}, '{5}', '{6}', '{7}', '{8}', '{9}', " +
                                         @"'{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', " +
                                         @"'{20}', '{21}', '{22}', '{23}', '{24}', '{25}', '{26}', '{27}', '{28}', '{29}', " +
                                         @"'{30}', '{31}', '{32}', '{33}', '{34}', '{35}', '{36}', '{37}', '{38}', '{39}', " +
                                         @"'{40}', '{41}', '{42}', '{43}', '{44}', '{45}', '{46}', '{47}', '{48}')",
                                        (string)dr["PrintGroup"].ToString(),
                                        (string)dr["CardID"].ToString(),
                                        (string)dr["DocumentNumber"].ToString(),
                                        (string)dr["AccountNumber"].ToString(),
                                        (string)dr["Type"].ToString(),
                                        (string)dr["FileName"],
                                        (string)dr["DateGenerated"].ToString(),
                                        (string)dr["FromName"].ToString().Replace("'", "''"),
                                        (string)dr["MemorialName"].ToString().Replace("'", "''"),
                                        (string)dr["ToName"].ToString().Replace("'", "''"),
                                        (string)dr["ToAddress1"].ToString().Replace("'", "''"),
                                        (string)dr["ToAddress2"].ToString().Replace("'", "''"),
                                        (string)dr["ToAddress3"].ToString().Replace("'", "''"),
                                        (string)dr["ToAddress4"].ToString().Replace("'", "''"),
                                        (string)dr["ToAddress5"].ToString().Replace("'", "''"),
                                        (string)dr["StdError"].ToString().Replace("'", "''"),
                                        (string)dr["Project01"].ToString().Replace("'", "''"),
                                        (string)dr["ShortDescription01"].ToString().Replace("'", "''"),
                                        (string)dr["LongDescription01"].ToString().Replace("'", "''"),
                                        (string)dr["Project02"].ToString().Replace("'", "''"),
                                        (string)dr["ShortDescription02"].ToString().Replace("'", "''"),
                                        (string)dr["LongDescription02"].ToString().Replace("'", "''"),
                                        (string)dr["Project03"].ToString().Replace("'", "''"),
                                        (string)dr["ShortDescription03"].ToString().Replace("'", "''"),
                                        (string)dr["LongDescription03"].ToString().Replace("'", "''"),
                                        (string)dr["Project04"].ToString().Replace("'", "''"),
                                        (string)dr["ShortDescription04"].ToString().Replace("'", "''"),
                                        (string)dr["LongDescription04"].ToString().Replace("'", "''"),
                                        (string)dr["Project05"].ToString().Replace("'", "''"),
                                        (string)dr["ShortDescription05"].ToString().Replace("'", "''"),
                                        (string)dr["LongDescription05"].ToString().Replace("'", "''"),
                                        (string)dr["Project06"].ToString().Replace("'", "''"),
                                        (string)dr["ShortDescription06"].ToString().Replace("'", "''"),
                                        (string)dr["LongDescription06"].ToString().Replace("'", "''"),
                                        (string)dr["Project07"].ToString().Replace("'", "''"),
                                        (string)dr["ShortDescription07"].ToString().Replace("'", "''"),
                                        (string)dr["LongDescription07"].ToString().Replace("'", "''"),
                                        (string)dr["Project08"].ToString().Replace("'", "''"),
                                        (string)dr["ShortDescription08"].ToString().Replace("'", "''"),
                                        (string)dr["LongDescription08"].ToString().Replace("'", "''"),
                                        (string)dr["Project09"].ToString().Replace("'", "''"),
                                        (string)dr["ShortDescription09"].ToString().Replace("'", "''"),
                                        (string)dr["LongDescription09"].ToString().Replace("'", "''"),
                                        (string)dr["Project10"].ToString().Replace("'", "''"),
                                        (string)dr["ShortDescription10"].ToString().Replace("'", "''"),
                                        (string)dr["LongDescription10"].ToString().Replace("'", "''"),
                                        (string)dr["CardNumber"].ToString(),
                                        (string)dr["OfNumber"].ToString(),
                                        (string)dr["CardTag"]);

                    cmdLoadStaging = new OleDbCommand(sSQL, conn);
                    cmdLoadStaging.CommandTimeout = 3600;
                    cmdLoadStaging.CommandType = CommandType.Text;
                    cmdLoadStaging.ExecuteNonQuery();
                    cmdLoadStaging = null;
                }
            }
            catch (Exception e)
            {
                bSuccess = false;
                MessageBox.Show("Unable to write label record: - " + e.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
                conn = null;
            }
            return (bSuccess);
        }
        public bool uspGetPrintGroupToMerge(out long lPrintGroup)
        {
            string sProcedure;
            lPrintGroup = 0;
            DataSet ds = null;
            bSuccess = true;
            try
            {
                OleDbConnection conn;
                GetConnection(out conn, true);
                OleDbCommand commLoadStaging = new OleDbCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "select top 1 PrintGroup from HonorMemorialCards;";
                ds = new DataSet();

                OleDbCommand cmdRunSP = new OleDbCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.Text;

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmdRunSP;
                adapter.Fill(ds, "PG");
                conn.Close();
                conn.Dispose();
                foreach (DataRow dr in ds.Tables["PG"].Rows)
                {
                    lPrintGroup = (int)dr["PrintGroup"];
                }
                ds = null;
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not print group number. - " + e.ToString());
                bSuccess = false;
            }
            return bSuccess;
        }

    }

}

