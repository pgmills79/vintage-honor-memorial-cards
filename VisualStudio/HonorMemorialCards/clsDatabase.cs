﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Reflection;
using System.Threading;
using MyCode;
using System.Windows;
using System.Windows.Threading;
using System.Security.Principal;


//
// 07/05/2016 Steve Burton
// Added logic to pull cards through a particular date. 
//

namespace MyCode
{
    public class clsDatabase
    {
        private const string sUserName = "spapps";
        private const string sPassword = "sampurse";
        private string _sServer;
        private string _sDatabase;
        private bool bSuccess;
        public bool BSuccess
        {
            get { return bSuccess; }
        }
        private bool bTaskComplete;
        private int lReturnStatus;
        private string sReturnErrorMessage;
        private int iReturnCountRegular;
        private int iReturnCountAcknowledge;
        private int iReturnCountOHOP;
        private int iReturnCountMOTH;
        private long lReturnPrintGroup;
        private void TaskComplete(IAsyncResult c1)
        {
            try
            {
                SqlCommand comm = (SqlCommand)c1.AsyncState;
                int rows = comm.EndExecuteNonQuery(c1);
                lReturnStatus = (int)comm.Parameters["@status"].Value;
                if (comm.Parameters["@ErrorMessage"].Value != null)
                    sReturnErrorMessage = (string)comm.Parameters["@ErrorMessage"].Value;
                else
                    sReturnErrorMessage = "";
            }
            catch (Exception e)
            {
                sReturnErrorMessage = "Could not get results - " + e.Message;
                lReturnStatus = 1;
            }
            finally
            {
                bTaskComplete = true;
            }
        }

        private void GetConnection(out SqlConnection conn, bool bAsync)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.Add("Server", _sServer);
            builder.Add("Database", _sDatabase);
            builder.Add("UID", sUserName);
            builder.Add("PWD", sPassword);
            //builder.Add("Connection Timeout", 3600);
            builder.Add("Connection Timeout", 0);
            if (bAsync)
                builder.Add("Asynchronous Processing", true);
            conn = new SqlConnection(builder.ConnectionString);
        }

        public clsDatabase()
        {
            clsIni ci = new clsIni();
            bSuccess = true;
            _sServer = ci.SApplicationServer;
            _sDatabase = ci.SApplicationDatabase;

        }

        ~clsDatabase()
        {

        }
        //
        //
        //    Count number of cards.
        //
        //
        public bool uspCountCardsReady( string sPullStatus, 
                                        DateTime dtThroughDate,      // S. Burton
                                        out long lCNTRegular,
                                        out long lCNTAcknowledge,
                                        out long lCNTOHOP,
                                        out long lCNTMOTH,
                                        out string sMessage)
        {
            //
            // This calls the stored procedure that loads the staging table.
            //
            int lStatus;
            string sErrorMessage;
            //Console.WriteLine (sStoredProcedure);
            bool bSuccess;
            bSuccess = true;
            sErrorMessage = "";
            try
            {
                SqlConnection conn;
                GetConnection(out conn, true);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;

                SqlCommand cmdRunSP = new SqlCommand("uspCountCardsReady4", conn);         // S. Burton
                //SqlCommand cmdRunSP = new SqlCommand("uspCountCardsReady3", conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@PullType", SqlDbType.Char, 1);
                cmdRunSP.Parameters["@PullType"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@PullType"].Value = sPullStatus;

                cmdRunSP.Parameters.Add("@ThroughDate", SqlDbType.DateTime);                // S. Burton
                cmdRunSP.Parameters["@ThroughDate"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@ThroughDate"].Value = dtThroughDate;

                cmdRunSP.Parameters.Add("@iCountRegular", SqlDbType.Int);
                cmdRunSP.Parameters["@iCountRegular"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@iCountAcknowledge", SqlDbType.Int);
                cmdRunSP.Parameters["@iCountAcknowledge"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@iCountOHOP", SqlDbType.Int);
                cmdRunSP.Parameters["@iCountOHOP"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@iCountMOTH", SqlDbType.Int);
                cmdRunSP.Parameters["@iCountMOTH"].Direction = ParameterDirection.Output;
                                
                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 255);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;
                bTaskComplete = false;

                AsyncCallback acb = new AsyncCallback(uspCountCardsReadyComplete);
                cmdRunSP.BeginExecuteNonQuery(acb, cmdRunSP);
                while (!bTaskComplete)
                {
                    DoEvents();
                    Thread.Sleep(1000);
                }
                lStatus = lReturnStatus;
                sErrorMessage = sReturnErrorMessage;
                if (lStatus != 0)
                {
                    MessageBox.Show("uspCountCardsReady4 failed:  " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }

            catch (Exception e)
            {
                bSuccess = false;
                MessageBox.Show("uspCountCardsReady4 failed:  " + e.ToString());
            }
            sMessage = sErrorMessage;
            lCNTRegular = iReturnCountRegular;
            lCNTAcknowledge = iReturnCountAcknowledge;
            lCNTOHOP = iReturnCountOHOP;
            lCNTMOTH = iReturnCountMOTH;
            return (bSuccess);
        }

        private void uspCountCardsReadyComplete(IAsyncResult c1)
        {
            try
            {
                SqlCommand comm = (SqlCommand)c1.AsyncState;
                int rows = comm.EndExecuteNonQuery(c1);
                iReturnCountRegular = (int)comm.Parameters["@iCountRegular"].Value;
                iReturnCountAcknowledge = (int)comm.Parameters["@iCountAcknowledge"].Value;
                iReturnCountOHOP = (int)comm.Parameters["@iCountOHOP"].Value;
                iReturnCountMOTH = (int)comm.Parameters["@iCountMOTH"].Value;
                lReturnStatus = (int)comm.Parameters["@status"].Value;
                if (comm.Parameters["@ErrorMessage"].Value != null)
                {
                    sReturnErrorMessage = (string)comm.Parameters["@ErrorMessage"].Value;
                    if (lReturnStatus == 0)
                        bSuccess = true;
                    else
                        bSuccess = false;
                }
                else
                {
                    sReturnErrorMessage = "";
                    bSuccess = false;
                }
            }
            catch (Exception e)
            {
                sReturnErrorMessage = "Could not get results - " + e.Message;
                lReturnStatus = 1;
            }
            finally
            {
                bTaskComplete = true;
            }
        }
        //
        //
        //    Process Packing Slips
        //
        //
        public bool uspFormatCardData(string sPullStatus,
                                      int iWhichCards,
                                      DateTime dtThroughDate,                           // S. Burton
                                      out long lPrintGroup,
                                      out string sMessage)
        {
            //
            // This calls the stored procedure that loads the staging table.
            //
            int lStatus;
            string sErrorMessage;
            //Console.WriteLine (sStoredProcedure);
            bool bSuccess;
            bSuccess = true;
            lPrintGroup = 0;
            sErrorMessage = "";
            try
            {
                SqlConnection conn;
                GetConnection(out conn, true);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;

                SqlCommand cmdRunSP = new SqlCommand("uspFormatCardData4", conn);       // S. Burton
                //SqlCommand cmdRunSP = new SqlCommand("uspFormatCardData3", conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@PullType", SqlDbType.Char, 1);
                cmdRunSP.Parameters["@PullType"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@PullType"].Value = sPullStatus;

                cmdRunSP.Parameters.Add("@WhichCards", SqlDbType.Int);
                cmdRunSP.Parameters["@WhichCards"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@WhichCards"].Value = iWhichCards;

                cmdRunSP.Parameters.Add("@ThroughDate", SqlDbType.DateTime);
                cmdRunSP.Parameters["@ThroughDate"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@ThroughDate"].Value = dtThroughDate;

                cmdRunSP.Parameters.Add("@PrintGroup", SqlDbType.BigInt);
                cmdRunSP.Parameters["@PrintGroup"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 255);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;
                bTaskComplete = false;

                AsyncCallback acb = new AsyncCallback(uspFormatCardDataComplete);
                cmdRunSP.BeginExecuteNonQuery(acb, cmdRunSP);
                while (!bTaskComplete)
                {
                    DoEvents();
                    Thread.Sleep(1000);
                }
                lStatus = lReturnStatus;
                sErrorMessage = sReturnErrorMessage;
                lPrintGroup = lReturnPrintGroup;
                if (lStatus != 0)
                {
                    MessageBox.Show("uspFormatCardData4 failed:  " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }

            catch (Exception e)
            {
                bSuccess = false;
                MessageBox.Show("uspFormatCardData4 failed:  " + e.ToString());
            }
            sMessage = sErrorMessage;

            return (bSuccess);
        }
        private string GetuserName()
        {
            WindowsPrincipal wp = new WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
            return (wp.Identity.Name);
        }
        private void uspFormatCardDataComplete(IAsyncResult c1)
        {
            try
            {
                SqlCommand comm = (SqlCommand)c1.AsyncState;
                int rows = comm.EndExecuteNonQuery(c1);
                lReturnPrintGroup = (long)comm.Parameters["@PrintGroup"].Value;
                lReturnStatus = (int)comm.Parameters["@status"].Value;
                if (comm.Parameters["@ErrorMessage"].Value != null)
                {
                    sReturnErrorMessage = (string)comm.Parameters["@ErrorMessage"].Value;
                    if (lReturnStatus == 0)
                        bSuccess = true;
                    else
                        bSuccess = false;
                }
                else
                {
                    sReturnErrorMessage = "";
                    bSuccess = false;
                }
            }
            catch (Exception e)
            {
                sReturnErrorMessage = "Could not get results - " + e.Message;
                lReturnStatus = 1;
            }
            finally
            {
                bTaskComplete = true;
            }
        }

        //
        //
        //    Ship Packing Slip Lot
        //
        //
        public bool uspResetFormattedCardData(long lPrintGroup,
                                               out string sMessage)
        {
            //
            // This calls the stored procedure that loads the staging table.
            //
            int lStatus;
            string sErrorMessage;
            //Console.WriteLine (sStoredProcedure);
            bool bSuccess;
            bSuccess = true;
            sErrorMessage = "";
            try
            {
                SqlConnection conn;
                GetConnection(out conn, true);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;

                SqlCommand cmdRunSP = new SqlCommand("uspResetFormattedCardData", conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@PrintGroup", SqlDbType.BigInt);
                cmdRunSP.Parameters["@PrintGroup"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@PrintGroup"].Value = lPrintGroup;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 255);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;
                bTaskComplete = false;

                AsyncCallback acb = new AsyncCallback(uspResetFormattedCardDataComplete);
                cmdRunSP.BeginExecuteNonQuery(acb, cmdRunSP);
                while (!bTaskComplete)
                {
                    DoEvents();
                    Thread.Sleep(1000);
                }
                lStatus = lReturnStatus;
                sErrorMessage = sReturnErrorMessage;
                if (lStatus != 0)
                {
                    MessageBox.Show("uspResetFormattedCardData failed:  " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }

            catch (Exception e)
            {
                bSuccess = false;
                MessageBox.Show("uspResetFormattedCardData failed:  " + e.ToString());
            }
            sMessage = sErrorMessage;

            return (bSuccess);
        }

        private void uspResetFormattedCardDataComplete(IAsyncResult c1)
        {
            try
            {
                SqlCommand comm = (SqlCommand)c1.AsyncState;
                int rows = comm.EndExecuteNonQuery(c1);
                lReturnStatus = (int)comm.Parameters["@status"].Value;
                if (comm.Parameters["@ErrorMessage"].Value != null)
                {
                    sReturnErrorMessage = (string)comm.Parameters["@ErrorMessage"].Value;
                    if (lReturnStatus == 0)
                        bSuccess = true;
                    else
                        bSuccess = false;
                }
                else
                {
                    sReturnErrorMessage = "";
                    bSuccess = false;
                }
            }
            catch (Exception e)
            {
                sReturnErrorMessage = "Could not get results - " + e.Message;
                lReturnStatus = 1;
            }
            finally
            {
                bTaskComplete = true;
            }
        }
        //
        // Do events
        //
        private void DoEvents()
        {
            DispatcherFrame f = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
            (SendOrPostCallback)delegate(object arg)
            {
                DispatcherFrame fr = arg as DispatcherFrame;
                fr.Continue = false;
            }, f);
            Dispatcher.PushFrame(f);
            //Thread.Sleep(1000);
        }

        public bool uspGetFormattedCardData(long lPrintGroup,
                                            out DataSet ds)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            long lStatus = 0;
            ds = null;
            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn, true);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspGetFormattedCardData";
                ds = new DataSet();

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@PrintGroup", SqlDbType.Int);
                cmdRunSP.Parameters["@PrintGroup"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@PrintGroup"].Value = lPrintGroup;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 255);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;


                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmdRunSP;
                adapter.Fill(ds, "LIST");

                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;
                if (lStatus != 0)
                {
                    MessageBox.Show("Could not get data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not get data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }

        public bool uspGetReturnToSenderLabelData(long lPrintGroup,
                                    out DataSet ds)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            long lStatus = 0;
            ds = new DataSet();
            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn, true);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspGetReturnToSenderLabelData";

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@PrintGroup", SqlDbType.Int);
                cmdRunSP.Parameters["@PrintGroup"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@PrintGroup"].Value = lPrintGroup;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 255);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;


                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmdRunSP;
                adapter.Fill(ds, "LIST");

                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;
                if (lStatus != 0)
                {
                    MessageBox.Show("Could not get data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not get data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }

        public bool uspGridCardPrintGroups(out DataTable dt)
        {
            string sProcedure = "";
            string sErrorMessage = "";
            long lStatus = 0;
            DataSet ds = null;
            dt = null;
            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn, true);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspGridCardPrintGroups";
                ds = new DataSet();

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@status", SqlDbType.Int);
                cmdRunSP.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdRunSP.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 255);
                cmdRunSP.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;


                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmdRunSP;
                adapter.Fill(ds, "LIST");
                dt = ds.Tables["LIST"];

                lStatus = (int)cmdRunSP.Parameters["@status"].Value;
                sErrorMessage = (string)cmdRunSP.Parameters["@ErrorMessage"].Value;
                if (lStatus != 0)
                {
                    MessageBox.Show("Could not get data - " + sErrorMessage);
                    bSuccess = false;
                }
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not get data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }


        public bool uspGetCardAddressToAce(long lPrintGroup, out DataSet ds)
        {
            string sProcedure;
            ds = null;
            bSuccess = true;
            try
            {
                SqlConnection conn;
                GetConnection(out conn, true);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                commLoadStaging.Connection = conn;
                sProcedure = "uspGetCardAddressToAce";
                ds = new DataSet();

                SqlCommand cmdRunSP = new SqlCommand(sProcedure, conn);

                //cmdRunSP.CommandTimeout = 3600;
                cmdRunSP.CommandTimeout = 0;
                cmdRunSP.CommandType = CommandType.StoredProcedure;

                cmdRunSP.Parameters.Add("@PrintGroup", SqlDbType.VarChar, 80);
                cmdRunSP.Parameters["@PrintGroup"].Direction = ParameterDirection.Input;
                cmdRunSP.Parameters["@PrintGroup"].Value = lPrintGroup;

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmdRunSP;
                adapter.Fill(ds, "CSV");
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not get ACE card data - " + e.ToString());
                bSuccess = false;
            }
            DoEvents();
            return bSuccess;
        }

        public bool uspUpdateParsedCards(DataRow dr,
                                       out string sErrorMessage)
        {
            //
            // This calls the stored procedure that loads the staging table.
            //
            SqlConnection conn = null;
            long iStatus = 0;
            bool bSuccess = true;
            sErrorMessage = "";
            try
            {
                GetConnection(out conn, true);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                SqlCommand cmdLoadStaging = new SqlCommand("dbo.uspUpdateParsedCards", conn);
                cmdLoadStaging.CommandTimeout = 3600;
                cmdLoadStaging.CommandType = CommandType.StoredProcedure;

                cmdLoadStaging.Parameters.Add("@CardID", SqlDbType.BigInt);
                cmdLoadStaging.Parameters["@CardID"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@CardID"].Value = Convert.ToInt64(dr["CardID"]);

                cmdLoadStaging.Parameters.Add("@PrintGroup", SqlDbType.BigInt);
                cmdLoadStaging.Parameters["@PrintGroup"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@PrintGroup"].Value = Convert.ToInt64(dr["PrintGroup"]);

                cmdLoadStaging.Parameters.Add("@StdAddress1", SqlDbType.VarChar, 70);
                cmdLoadStaging.Parameters["@StdAddress1"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@StdAddress1"].Value = dr["STDADDR1"];

                cmdLoadStaging.Parameters.Add("@StdAddress2", SqlDbType.VarChar, 70);
                cmdLoadStaging.Parameters["@StdAddress2"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@StdAddress2"].Value = dr["STDADDR2"];

                cmdLoadStaging.Parameters.Add("@StdAddress3", SqlDbType.VarChar, 70);
                cmdLoadStaging.Parameters["@StdAddress3"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@StdAddress3"].Value = dr["STDADDR3"];

                cmdLoadStaging.Parameters.Add("@STDCITY", SqlDbType.VarChar, 70);
                cmdLoadStaging.Parameters["@STDCITY"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@STDCITY"].Value = dr["STDCITY"];

                cmdLoadStaging.Parameters.Add("@STDSTATE", SqlDbType.VarChar, 10);
                cmdLoadStaging.Parameters["@STDSTATE"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@STDSTATE"].Value = dr["STDSTATE"];

                cmdLoadStaging.Parameters.Add("@STDZIP", SqlDbType.VarChar, 15);
                cmdLoadStaging.Parameters["@STDZIP"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@STDZIP"].Value = dr["STDZIP10"];

                cmdLoadStaging.Parameters.Add("@STDCountry", SqlDbType.VarChar, 15);
                cmdLoadStaging.Parameters["@STDCountry"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@STDCountry"].Value = "";
                //cmdLoadStaging.Parameters.Add("@STANDAD01", SqlDbType.VarChar, 80);
                //cmdLoadStaging.Parameters["@STANDAD01"].Direction = ParameterDirection.Input;
                //cmdLoadStaging.Parameters["@STANDAD01"].Value = dr["STANDAD01"];

                //cmdLoadStaging.Parameters.Add("@STANDAD02", SqlDbType.VarChar, 80);
                //cmdLoadStaging.Parameters["@STANDAD02"].Direction = ParameterDirection.Input;
                //cmdLoadStaging.Parameters["@STANDAD02"].Value = dr["STANDAD02"];

                //cmdLoadStaging.Parameters.Add("@STANDAD03", SqlDbType.VarChar, 80);
                //cmdLoadStaging.Parameters["@STANDAD03"].Direction = ParameterDirection.Input;
                //cmdLoadStaging.Parameters["@STANDAD03"].Value = dr["STANDAD03"];

                //cmdLoadStaging.Parameters.Add("@STANDAD04", SqlDbType.VarChar, 80);
                //cmdLoadStaging.Parameters["@STANDAD04"].Direction = ParameterDirection.Input;
                //cmdLoadStaging.Parameters["@STANDAD04"].Value = dr["STANDAD04"];

                //cmdLoadStaging.Parameters.Add("@STANDAD05", SqlDbType.VarChar, 80);
                //cmdLoadStaging.Parameters["@STANDAD05"].Direction = ParameterDirection.Input;
                //cmdLoadStaging.Parameters["@STANDAD05"].Value = dr["STANDAD05"];

                cmdLoadStaging.Parameters.Add("@ACE_ERROR", SqlDbType.VarChar, 10);
                cmdLoadStaging.Parameters["@ACE_ERROR"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@ACE_ERROR"].Value = dr["ACE_ERROR"];

                cmdLoadStaging.Parameters.Add("@status", SqlDbType.Int);
                cmdLoadStaging.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdLoadStaging.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 255);
                cmdLoadStaging.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;


                cmdLoadStaging.ExecuteNonQuery();
                iStatus = (int)cmdLoadStaging.Parameters["@status"].Value;
                if (cmdLoadStaging.Parameters["@ErrorMessage"].Value != null)
                    sErrorMessage = (string)cmdLoadStaging.Parameters["@ErrorMessage"].Value;
                else
                    sErrorMessage = "";
                if (iStatus != 0)
                {
                    MessageBox.Show("uspPutAcedResponse failed: - " + sErrorMessage);
                }
            }
            catch (Exception e)
            {
                bSuccess = false;
                MessageBox.Show("Unable to call uspPutAcedResponse: - " + e.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
                conn = null;
            }
            return (bSuccess);
        }

        public bool uspPutAcedBillingResponse(DataRow dr,
                                       out string sErrorMessage)
        {
            //
            // This calls the stored procedure that loads the staging table.
            //
            SqlConnection conn = null;
            long iStatus = 0;
            bool bSuccess = true;
            sErrorMessage = "";
            try
            {
                GetConnection(out conn, true);
                SqlCommand commLoadStaging = new SqlCommand();
                conn.Open();
                SqlCommand cmdLoadStaging = new SqlCommand("dbo.uspPutAcedPackingSlipBilling", conn);
                cmdLoadStaging.CommandTimeout = 3600;
                cmdLoadStaging.CommandType = CommandType.StoredProcedure;

                cmdLoadStaging.Parameters.Add("@ResponseID", SqlDbType.BigInt);
                cmdLoadStaging.Parameters["@ResponseID"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@ResponseID"].Value = Convert.ToInt64(dr["ResponseID"]);

                cmdLoadStaging.Parameters.Add("@AddressID", SqlDbType.BigInt);
                cmdLoadStaging.Parameters["@AddressID"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@AddressID"].Value = Convert.ToInt64(dr["AddressID"]);

                cmdLoadStaging.Parameters.Add("@Address1", SqlDbType.VarChar, 70);
                cmdLoadStaging.Parameters["@Address1"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@Address1"].Value = dr["Address1"];

                cmdLoadStaging.Parameters.Add("@Address2", SqlDbType.VarChar, 70);
                cmdLoadStaging.Parameters["@Address2"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@Address2"].Value = dr["Address2"];

                cmdLoadStaging.Parameters.Add("@Address3", SqlDbType.VarChar, 70);
                cmdLoadStaging.Parameters["@Address3"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@Address3"].Value = dr["Address3"];

                cmdLoadStaging.Parameters.Add("@Address4", SqlDbType.VarChar, 70);
                cmdLoadStaging.Parameters["@Address4"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@Address4"].Value = dr["Address4"];

                cmdLoadStaging.Parameters.Add("@City", SqlDbType.VarChar, 70);
                cmdLoadStaging.Parameters["@City"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@City"].Value = dr["City"];

                cmdLoadStaging.Parameters.Add("@State", SqlDbType.VarChar, 10);
                cmdLoadStaging.Parameters["@State"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@State"].Value = dr["State"];

                cmdLoadStaging.Parameters.Add("@Zip", SqlDbType.VarChar, 10);
                cmdLoadStaging.Parameters["@Zip"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@Zip"].Value = dr["Zip"];

                cmdLoadStaging.Parameters.Add("@Country", SqlDbType.VarChar, 10);
                cmdLoadStaging.Parameters["@Country"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@Country"].Value = dr["Country"];

                cmdLoadStaging.Parameters.Add("@STANDAD01", SqlDbType.VarChar, 80);
                cmdLoadStaging.Parameters["@STANDAD01"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@STANDAD01"].Value = dr["STANDAD01"];

                cmdLoadStaging.Parameters.Add("@STANDAD02", SqlDbType.VarChar, 80);
                cmdLoadStaging.Parameters["@STANDAD02"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@STANDAD02"].Value = dr["STANDAD02"];

                cmdLoadStaging.Parameters.Add("@STANDAD03", SqlDbType.VarChar, 80);
                cmdLoadStaging.Parameters["@STANDAD03"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@STANDAD03"].Value = dr["STANDAD03"];

                cmdLoadStaging.Parameters.Add("@STANDAD04", SqlDbType.VarChar, 80);
                cmdLoadStaging.Parameters["@STANDAD04"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@STANDAD04"].Value = dr["STANDAD04"];

                cmdLoadStaging.Parameters.Add("@STANDAD05", SqlDbType.VarChar, 80);
                cmdLoadStaging.Parameters["@STANDAD05"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@STANDAD05"].Value = dr["STANDAD05"];

                cmdLoadStaging.Parameters.Add("@ACE_ERROR", SqlDbType.VarChar, 80);
                cmdLoadStaging.Parameters["@ACE_ERROR"].Direction = ParameterDirection.Input;
                cmdLoadStaging.Parameters["@ACE_ERROR"].Value = dr["ACE_ERROR"];

                cmdLoadStaging.Parameters.Add("@status", SqlDbType.Int);
                cmdLoadStaging.Parameters["@status"].Direction = ParameterDirection.Output;

                cmdLoadStaging.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 80);
                cmdLoadStaging.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;

                cmdLoadStaging.ExecuteNonQuery();
                iStatus = (int)cmdLoadStaging.Parameters["@status"].Value;
                if (cmdLoadStaging.Parameters["@ErrorMessage"].Value != null)
                    sErrorMessage = (string)cmdLoadStaging.Parameters["@ErrorMessage"].Value;
                else
                    sErrorMessage = "";
                if (iStatus != 0)
                {
                    MessageBox.Show("uspPutAcedPackingSlipBilling failed: - " + sErrorMessage);
                }
            }
            catch (Exception e)
            {
                bSuccess = false;
                MessageBox.Show("Unable to call uspPutAcedPackingSlipBilling: - " + e.ToString());
            }
            finally
            {
                if (conn != null)
                    conn.Close();
                conn = null;
            }
            return (bSuccess);
        }
    }
}

