﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using MyCode;
using System.Net;

namespace HonorMemorialCards
{
    public partial class frmReport : Form
    {
        private string sUserName = "spapps";
        private string sPassWord = "Pr@ise the L0rd";
        private string sDomain = "samaritan";
        private clsReportChoice cReportChoice;

        public frmReport(clsReportChoice ReportChoice)
        {
            cReportChoice = ReportChoice;
            InitializeComponent();
        }

        private void frmReport_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            reportViewer1.ProcessingMode = ProcessingMode.Remote;
            reportViewer1.ServerReport.ReportServerUrl = new Uri(cReportChoice.SReportServer);
            reportViewer1.ServerReport.ReportPath = cReportChoice.SReportPath;
            NetworkCredential myCred = new NetworkCredential(sUserName, sPassWord, sDomain);
            reportViewer1.ServerReport.ReportServerCredentials.NetworkCredentials = myCred;
            List<ReportParameter> paramList = new List<ReportParameter>();
            if (cReportChoice.IParameterType == 1)
            {
                paramList.Add(new ReportParameter("PrintGroup", cReportChoice.IPrintGroup.ToString(), true));
            }
            //if (cReportChoice.IParameterType == 2)
            //{
            //    paramList.Add(new ReportParameter("ientry_id", cReportChoice.IApplicantID.ToString(), true));
            //    paramList.Add(new ReportParameter("igopherpro_id", cReportChoice.IGopherPROID.ToString(), true));
            //    paramList.Add(new ReportParameter("igopherres_id", cReportChoice.IGopherRESID.ToString(), true));
            //}
            reportViewer1.ServerReport.SetParameters(paramList);
            //string mimeType;
            //string encoding;
            //string fileNameExtension;
            //Warning[] warnings;
            //string[] streamids;
            //byte[] bytes = reportViewer1.ServerReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
            //FileStream fs = new FileStream(@"C:\holdtemp\test.pdf", FileMode.Create);
            //fs.Write(bytes, 0, bytes.Length);
            //fs.Close();
        }
    }
}
