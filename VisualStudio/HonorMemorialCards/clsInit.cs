﻿using System;
using System.Xml;
using System.Windows;
using MyCode;

namespace MyCode
{

    public class clsIni
    {
        private string sApplicationServer;
        public string SApplicationServer
        {
            get { return sApplicationServer; }
        }

        private string sApplicationDatabase;
        public string SApplicationDatabase
        {
            get { return sApplicationDatabase; }
        }

        private string sEnvironment;
        public string SEnvironment
        {
            get { return sEnvironment; }
        }

        private string sParseDirectory;
        public string SParseDirectory
        {
            get { return sParseDirectory; }
        }

        private string sAccessDatabase;
        public string SAccessDatabase
        {
            get { return sAccessDatabase; }
        }
        private string sCardSegmentReport;
        public string SCardSegmentReport
        {
            get { return sCardSegmentReport; }
        }
        private string sLabelToCardReport;
        public string SLabelToCardReport
        {
            get { return sLabelToCardReport; }
        }
        private string sReportServer;

        public string SReportServer
        {
            get { return sReportServer; }
        }
        private bool bSuccess;
        public bool BSuccess
        {
            get { return bSuccess; }
        }

        public clsIni()
        {
            bSuccess = LoadConfigurationParameters();
        }
        ~clsIni()
        {
        }
        private bool LoadConfigurationParameters()
        {

            bool bStatus;

            bStatus = GetOneItem("ApplicationServer", out sApplicationServer);
            if (bStatus)
                bStatus = GetOneItem("ApplicationDatabase", out sApplicationDatabase);
            if (bStatus)
                bStatus = GetOneItem("Environment", out sEnvironment);
            if (bStatus)
                bStatus = GetOneItem("ParseDirectory", out sParseDirectory);
            if (bStatus)
                bStatus = GetOneItem("AccessDataBase", out sAccessDatabase);
            if (bStatus)
                bStatus = GetOneItem("CardSegmentReport", out sCardSegmentReport);
            if (bStatus)
                bStatus = GetOneItem("LabelToCardReport", out sLabelToCardReport);
            if (bStatus)
                bStatus = GetOneItem("ReportServer", out sReportServer);
            return bStatus;
        }
        private bool GetOneItem(string sItem, out string sValue)
        //
        //    Get one SQL item from the SQL file.
        //    
        {
            bool sFound;

            sValue = "";
            sFound = false;
            XmlDocument xDoc = new XmlDocument();
            try
            {
                string sStartupDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                string sFile = sStartupDirectory.Trim().Replace(@"file:\", "") + @"\HonorMemorialCards.xml";
                xDoc.Load(sFile);
                XmlNodeList XmlList = xDoc.GetElementsByTagName(sItem);
                foreach (XmlNode XmlNode in XmlList)
                {
                    string sParent = XmlNode.ParentNode.Name;
                    if (sParent.ToLower() == "initialization")
                    {
                        sValue = XmlNode.InnerText;
                        sFound = true;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to process initialization file - " + e.ToString());
            }
            return sFound;
        }
    }

}

