﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Reflection;
using System.Threading;
using MyCode;
using System.Security.Principal;
using System.Windows;


namespace MyCode
{
    class clsSemaphore
    {
        private const string sUserName = "spapps";
        private const string sPassword = "sampurse";
        private static string _sJDEServer;
        private static string _sJDEDatabase;
        public string sErrorMessage;
        public bool bSuccess;
        private SqlConnection connSemaphore;

        private void GetConnection(out SqlConnection conn)
        //
        // Create connection string to connect with the database.
        //
        {

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.Add("Server", _sJDEServer);
            builder.Add("Database", _sJDEDatabase);
            builder.Add("UID", sUserName);
            builder.Add("PWD", sPassword);
            builder.Add("Connection Timeout", 0);
            conn = new SqlConnection(builder.ConnectionString);
        }

        public clsSemaphore()
        {
            clsIni cIni = new clsIni();
            // Initiates connections with SQL.
            _sJDEServer = cIni.SApplicationServer;
            _sJDEDatabase = cIni.SApplicationDatabase;
            sErrorMessage = "";
            bSuccess = true;
        }
        ~clsSemaphore()
        {
        }


        public bool OpenSemaphore()
        {
            string sSQL;
            bool bStatus;
            bStatus = true;
            try
            {
                GetConnection(out connSemaphore);
                connSemaphore.Open();
                SqlCommand comm = new SqlCommand();
                comm.Connection = connSemaphore;
                comm.CommandTimeout = 0;

                // Make sure our record is there.
                sSQL = @"if not exists (select * from [dbo].[Semaphores] where programname = 'HONORMEMORIAL') " +
                        @"insert into [dbo].[Semaphores] ([ProgramName],	[Name]) values ('HONORMEMORIAL', '')";
                comm.CommandText = sSQL;
                comm.ExecuteNonQuery();

                // Update it and lock it
                sSQL = "Begin transaction " +
                       "update dbo.Semaphores with (holdlock) set Name = '" +
                        WindowsIdentity.GetCurrent().Name.ToString() +
                        "'where programname = 'HONORMEMORIAL'";
                comm.CommandText = sSQL;
                comm.ExecuteNonQuery();
                comm.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to get semaphore - " + e.Message);
                bStatus = false;
            }
            return bStatus;
        }

        public bool CloseSemaphore()
        {
            string sSQL;
            bool bStatus;
            bStatus = true;
            try
            {
                SqlCommand comm = new SqlCommand();
                sSQL = "if (@@Trancount > 0) commit transaction";
                comm.Connection = connSemaphore;
                comm.CommandTimeout = 0;
                comm.CommandText = sSQL;
                comm.ExecuteNonQuery();
                comm.Dispose();
            }
            catch
            {
                bStatus = false;
            }
            finally
            {

                connSemaphore.Close();
                connSemaphore.Dispose();
            }
            return bStatus;
        }
    }
}





