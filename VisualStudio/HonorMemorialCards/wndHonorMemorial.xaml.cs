﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MyCode;
using System.Data;
using Xceed.Wpf.DataGrid;
using System.Windows.Threading;
using System.Threading;
using System.ComponentModel;

namespace HonorMemorialCards
{
    /// <summary>
    /// Interaction logic for wndHonorMemorial.xaml
    /// </summary>
    /// 

    //-----------------------------------------------------------------------------
    //
    // 07/05/2016 Steve Burton
    // Added logic to pull cards through a particular date. 
    //
    // 02/13/2017 S. Burton
    // Added code to display a Message Box alert if the ACE process fails.
    //
    //-----------------------------------------------------------------------------

    public partial class wndHonorMemorial : Window
    {
        DateTime dtThroughDate = DateTime.Now;    // S. Burton

        public static int ACEStatus = 0;

        private static DataTable m_ListPrinted;

        public wndHonorMemorial()
        {
            InitializeComponent();
        }


        public static DataTable dtListPrinted
        {
            get
            {
                return m_ListPrinted;
            }
        }

        private void ListPrinted()
        {
            clsDatabase cdb = new clsDatabase();
            cdb.uspGridCardPrintGroups(out m_ListPrinted);
            dataGridPrinted.ItemsSource = new DataGridCollectionView(dtListPrinted.DefaultView);
        }

        private void winPrintHonorMemorialCards_Loaded(object sender, RoutedEventArgs e)
        {
            chkTest.IsChecked = false;
            chkAcknowledgement.IsChecked = true;
            chkOHOP.IsChecked = true;
            chkRegular.IsChecked = true;
            chkMOTH.IsChecked = true;
            Done();

            //
            // Register date picker change
            //
            DependencyPropertyDescriptor myDescriptor = DependencyPropertyDescriptor.FromProperty(Xceed.Wpf.Controls.DatePicker.SelectedDateProperty, typeof(Xceed.Wpf.Controls.DatePicker));
            myDescriptor.AddValueChanged(dpThroughDate, new EventHandler(DatePickerChanged));

        }

        protected void DatePickerChanged(object sender, EventArgs e)     // S. Burton
        {
            InProgress();
            dtThroughDate = Convert.ToDateTime(dpThroughDate.SelectedDate);
            Done();
        }

        private void miRefresh_Click(object sender, RoutedEventArgs e)
        {
            InProgress();
            Done();
        }

        private void miReset_Click(object sender, RoutedEventArgs e)
        {
            long lPrintGroup;
            string sMessage;
            MessageBoxImage MBImage;
            InProgress();
            if (dataGridPrinted.SelectedItems.Count > 0)
            {
                System.Data.DataRow row = (System.Data.DataRow)dataGridPrinted.SelectedItem;
                if (row != null)
                {
                    lPrintGroup = (int)row["PrintGroup"];
                    sMessage = "Would you like to reset card print group " + lPrintGroup.ToString() + "?";
                    MBImage = MessageBoxImage.Question;
                    MessageBoxResult result = MessageBox.Show(sMessage, "Reset Card Print Group", MessageBoxButton.YesNo, MBImage);
                    if (result == MessageBoxResult.Yes)
                    {
                        clsDatabase cdb = new clsDatabase();
                        if (!cdb.uspResetFormattedCardData(lPrintGroup, out sMessage))
                            MessageBox.Show(" Could not reset print group  - " + sMessage);
                    }
                }
            }
            Done();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void miLabelToCard_Click(object sender, RoutedEventArgs e)
        {
            int lPrintGroup;
            InProgress();
            System.Data.DataRow row = (System.Data.DataRow)dataGridPrinted.SelectedItem;
            if (row != null)
            {
                lPrintGroup = (int)row["PrintGroup"];
                clsIni p = new clsIni();
                clsReportChoice c = new clsReportChoice(lPrintGroup,
                                                        p.SLabelToCardReport,
                                                        p.SReportServer);
                frmReport f = new frmReport(c);
                f.Show();
            }

            Done();
        }

        private void miPrintSummary_Click(object sender, RoutedEventArgs e)
        {
            int lPrintGroup;
            InProgress();
            System.Data.DataRow row = (System.Data.DataRow)dataGridPrinted.SelectedItem;
            if (row != null)
            {
                lPrintGroup = (int)row["PrintGroup"];
                clsIni p = new clsIni();
                clsReportChoice c = new clsReportChoice(lPrintGroup,
                                                        p.SCardSegmentReport,
                                                        p.SReportServer);
                frmReport f = new frmReport(c);
                f.Show();
            }
            Done();
        }

        private void InProgress()
        {
            Mouse.OverrideCursor = Cursors.Wait;
            chkTest.IsEnabled = false;
            chkRegular.IsEnabled = false;
            chkAcknowledgement.IsEnabled = false;
            chkOHOP.IsEnabled = false;
            chkMOTH.IsEnabled = false;
            miTools.IsEnabled = false;
            miReports.IsEnabled = false;
            btnProcess.IsEnabled = false;
            DoEvents();
            Mouse.OverrideCursor = Cursors.Wait;
        }
        private void Done()
        {
            ListPrinted();
            AvailableToPrint();
            PrintGroupToMerge();
            chkRegular.IsEnabled = true;
            chkAcknowledgement.IsEnabled = true;
            chkOHOP.IsEnabled = true;
            chkMOTH.IsEnabled = true;
            chkTest.IsEnabled = true;
            miTools.IsEnabled = true;
            miReports.IsEnabled = true;
            btnProcess.IsEnabled = true;
            DoEvents();
            Mouse.OverrideCursor = null;
        }
        //
        // Do events
        //
        private void DoEvents()
        {
            DispatcherFrame f = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
            (SendOrPostCallback)delegate(object arg)
            {
                DispatcherFrame fr = arg as DispatcherFrame;
                fr.Continue = false;
            }, f);
            Dispatcher.PushFrame(f);
            //Thread.Sleep(1000);
        }

        private void btnProcess_Click(object sender, RoutedEventArgs e)
        {
            bool bStatus;
            long lPrintGroup = 0;
          
            InProgress();
            if (bStatus = ProcessCards(out lPrintGroup))
            {
                bStatus = AceData(lPrintGroup);

                if (ACEStatus == 100)
                {
                    MessageBox.Show("ACE process failed. Terminating.");
                    bStatus = false;
                    ACEStatus = 0;
                }

                if (bStatus)
                    bStatus = LoadAccessTable(lPrintGroup);
            }
            if ((!bStatus) && (lPrintGroup > 0))
            {
                clsDatabase cdb = new clsDatabase();
                string sMessage;
                cdb.uspResetFormattedCardData(lPrintGroup, out sMessage);
            }
            Done();
        }

        private bool ProcessCards(out long lPrintGroup)
        {
            bool bStatus;
            string sMessage;
            bool isTest = false;
            int iWhichCard = 0;
            isTest = (bool)(chkTest.IsChecked == null ? false : chkTest.IsChecked);
            clsDatabase cdb = new clsDatabase();
            if (chkRegular.IsChecked == true) iWhichCard = iWhichCard + 1;
            if (chkAcknowledgement.IsChecked== true) iWhichCard = iWhichCard + 2;
            if (chkOHOP.IsChecked == true) iWhichCard = iWhichCard + 4;
            if (chkMOTH.IsChecked == true) iWhichCard = iWhichCard + 8;

            bStatus = cdb.uspFormatCardData((isTest ? "T" : "Y"), iWhichCard, dtThroughDate, out lPrintGroup, out sMessage);
            if (lPrintGroup == 0) bStatus = false;
            cdb = null;
            return (bStatus);
        }
        private bool LoadAccessTable(long lPrintGroup)
        {
            DataSet ds = null;
            clsAccess ca = new clsAccess();
            clsDatabase cd = new clsDatabase();
            bool bStatus = true;
            bStatus = ca.DeleteRecords();
            if (bStatus)
                bStatus = cd.uspGetFormattedCardData(lPrintGroup, out ds);
            if (bStatus)
                bStatus = ca.WriteHonorMemorialCards(ds);
            if (bStatus)
                bStatus = cd.uspGetReturnToSenderLabelData(lPrintGroup, out ds);
            if (bStatus)
                bStatus = ca.WriteHonorMemorialLabels(ds);
            return (bStatus);
        }
        private void AvailableToPrint()
        {
            long lCountRegular;
            long lCountAcknowlege;
            long lCountOHOP;
            long lCountMOTH;
            string sMessage;
            bool isTest = false;

            isTest = (bool)(chkTest.IsChecked == null ? false : chkTest.IsChecked);
            clsDatabase cd = new clsDatabase();
            cd.uspCountCardsReady((isTest ? "T" : "Y"), dtThroughDate, out lCountRegular, out lCountAcknowlege, out lCountOHOP, out lCountMOTH, out sMessage);
            //cd.uspCountCardsReady((isTest ? "T" : "Y"), out lCountRegular, out lCountAcknowlege, out lCountOHOP, out lCountMOTH, out sMessage);
            tbAvailableRegular.Text = lCountRegular.ToString();
            tbAvailableAcknowledge.Text = lCountAcknowlege.ToString();
            tbAvailableOHOP.Text = lCountOHOP.ToString();
            tbAvailableMOTH.Text = lCountMOTH.ToString();
        }
        private void PrintGroupToMerge()
        {
            long lPrintGroup;
            clsAccess ca = new clsAccess();
            ca.uspGetPrintGroupToMerge(out lPrintGroup);
            if (lPrintGroup > 0)
                tbPrintGroup.Text = "Print group to merge: " + lPrintGroup.ToString() + ".";
            else
                tbPrintGroup.Text = "No print group to merge.";
        }
        private bool AceData(long lPrintGroup)
        {
            DataSet ds;
            bool bStatus = true;
            try
            {
                clsDatabase cdb = new clsDatabase();
                bStatus = cdb.uspGetCardAddressToAce(lPrintGroup, out ds);
                if (bStatus)
                {
                    cdb = null;
                    clsIni ci = new clsIni(); 
                    clsWriteACECSV cw = new clsWriteACECSV();
                    if (bStatus = cw.OpenFile(ci.SParseDirectory + @"\ResponseAce.csv"))
                    {
                        bStatus = cw.WriteFile(ds);
                        if (bStatus)
                            bStatus = cw.CloseFile();
                    }
                    cw = null;
                    //
                    // Do the actual acing.
                    //
                    clsResponseAce cra = new clsResponseAce();
                    bStatus = cra.ParseAddresses();
                    cra = null;
                }
            }
            catch
            {
                bStatus = false;
            }
            return (bStatus);
        }

        private void miReprint_Click(object sender, RoutedEventArgs e)
        {
            long lPrintGroup;
            string sMessage;
            MessageBoxImage MBImage;
            InProgress();
            if (dataGridPrinted.SelectedItems.Count > 0)
            {
                System.Data.DataRow row = (System.Data.DataRow)dataGridPrinted.SelectedItem;
                if (row != null)
                {
                    lPrintGroup = (int)row["PrintGroup"];
                    sMessage = "Would you like to put " + lPrintGroup.ToString() + " into Access database in order to do a mail merge?";
                    MBImage = MessageBoxImage.Question;
                    MessageBoxResult result = MessageBox.Show(sMessage, "Reprint Card Print Group", MessageBoxButton.YesNo, MBImage);
                    if (result == MessageBoxResult.Yes)
                    {
                        LoadAccessTable(lPrintGroup);
                    }
                }
            }
            Done();

        }

        private void chkTest_Click(object sender, RoutedEventArgs e)
        {
            InProgress();
            Done();
        }
    }
}
