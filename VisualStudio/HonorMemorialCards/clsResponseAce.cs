﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.Data.Odbc;
using System.Security.Principal;
using MyCode;
using System.Windows;
using System.Windows.Threading;


namespace MyCode
{
    class clsResponseAce
    {
        private bool bNoDataToParse;
        private string sDirectory;
        //public bool sStatus;

        public clsResponseAce()
        {
            clsIni ci = new clsIni();
            sDirectory = ci.SParseDirectory;
            ci = null;
        }

        public bool ParseAddresses()
        {
            bool bStatus;
            clsSemaphore cs = new clsSemaphore();

            bStatus = true;
            bNoDataToParse = false;
            try
            {
                if (cs.OpenSemaphore() == true)
                {
                    if ((bStatus = MoveFilesToDirectory()) == true)
                    {
                        if (bStatus = RunAddressStandard())
                        {
                            bStatus = UpdateTables();
                        }
                    }
                }
            }
            catch
            { }
            finally
            {
                cs.CloseSemaphore();
                cs = null;
            }
            return (bStatus);
        }

        //
        //
        //  Move files to directory
        //
        //
        //
        private bool MoveFilesToDirectory()
        {
            bool bStatus;

            bStatus = true;
            if ((bStatus = DeleteOneFileInDirectory(@"ACE_AddressErrors.txt")) == false) goto Done;
            if ((bStatus = DeleteOneFileInDirectory(@"ACE_ExecutiveSummary.txt")) == false) goto Done;
            if ((bStatus = DeleteOneFileInDirectory(@"ACE_JobSummary.txt")) == false) goto Done;
            if ((bStatus = DeleteOneFileInDirectory(@"Header.csv")) == false) goto Done;
            if ((bStatus = DeleteOneFileInDirectory(@"ResponseAce.ace")) == false) goto Done;
            if ((bStatus = DeleteOneFileInDirectory(@"ResponseAce.bat")) == false) goto Done;
            if ((bStatus = DeleteOneFileInDirectory(@"ResponseAce.def")) == false) goto Done;
            if ((bStatus = DeleteOneFileInDirectory(@"ResponseAce.dmt")) == false) goto Done;
            if ((bStatus = DeleteOneFileInDirectory(@"ResponseAce.idx")) == false) goto Done;
            if ((bStatus = DeleteOneFileInDirectory(@"ResponseOut.csv")) == false) goto Done;
            if ((bStatus = DeleteOneFileInDirectory(@"ResponseOut.dmt")) == false) goto Done;
            if ((bStatus = DeleteOneFileInDirectory(@"ResponseOut.idx")) == false) goto Done;
            if ((bStatus = DeleteOneFileInDirectory(@"Done.txt")) == false) goto Done;
            if ((bStatus = MoveOneFileToDirectory(@"Header.csv")) == false) goto Done;
            if ((bStatus = MoveOneFileToDirectory(@"ResponseAce.ace")) == false) goto Done;
            if ((bStatus = MoveOneFileToDirectory(@"ResponseAce.bat")) == false) goto Done;
            if ((bStatus = MoveOneFileToDirectory(@"ResponseAce.def")) == false) goto Done;
            if ((bStatus = MoveOneFileToDirectory(@"ResponseAce.dmt")) == false) goto Done;
        Done:
            return (bStatus);
        }

        private bool CheckFile(string sFileName)
        {
            string sDeleteFile;
            bool bStatus;
            bStatus = false;
            sDeleteFile = sDirectory.Trim() + @"\" + sFileName;
            try
            {
                if (File.Exists(sDeleteFile))
                {
                    bStatus = true;
                }
            }
            catch
            {
                bStatus = false;
                MessageBox.Show("Unable to check file - " + sDeleteFile);
            }
            return (bStatus);
        }
        private bool DeleteOneFileInDirectory(string sFileName)
        {
            string sDeleteFile;
            bool bStatus;
            bStatus = true;
            sDeleteFile = sDirectory.Trim() + @"\" + sFileName;
            try
            {
                if (File.Exists(sDeleteFile))
                {
                    File.Delete(sDeleteFile);
                }
            }
            catch
            {
                bStatus = false;
                MessageBox.Show("Unable to delete file - " + sDeleteFile);
            }
            return (bStatus);
        }
        private bool CopyFile(string sf1, string sf2)
        {
            bool bStatus;
            bStatus = true;
            try
            {
                string sInFile;
                string sOutFile;
                sInFile = sDirectory.Trim() + @"\" + sf1;
                sOutFile = sDirectory.Trim() + @"\" + sf2;
                if (File.Exists(sInFile))
                {
                    File.Copy(sInFile, sOutFile, true);
                }
            }
            catch
            {
                bStatus = false;
                MessageBox.Show("Unable to move file - " + sf1 + " to " + sf2);
            }
            return (bStatus);
        }
        private bool MoveOneFileToDirectory(string sFileName)
        {
            string sInFile;
            string sOutFile;
            string sContents;
            string sNewContents;
            bool bStatus;

            bStatus = true;
            try
            {
                string sStartupDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                string sStartupPath = sStartupDirectory.Trim().Replace(@"file:\", "");
                sInFile = sStartupPath + @"\" + sFileName;
                sOutFile = sDirectory + @"\" + sFileName;
                // Read the file as one string.
                System.IO.StreamReader inFile = new System.IO.StreamReader(sInFile);
                sContents = inFile.ReadToEnd();
                sNewContents = sContents.Replace("%Directory%", sDirectory.Trim());
                System.IO.StreamWriter outFile = new System.IO.StreamWriter(sOutFile);
                outFile.Write(sNewContents);
                inFile.Close();
                outFile.Close();
            }
            catch
            {
                bStatus = false;
                MessageBox.Show("Unable to move file - " + sFileName);
            }
            return (bStatus);
        }

        private bool RunAddressStandard()
        {
            if (bNoDataToParse)
                return true;
            runSyncAndGetResults(sDirectory + @"\ResponseACE.bat");
            for (int i = 0; i < 2; i++)
            {
                DoEvents();
                if (File.Exists(sDirectory + @"\ResponseOut.csv"))   
                {
                    //MessageBox.Show(i.ToString());
                    return true;
                }
                else
                {

                    Thread.Sleep(1000);
                }
            }

            // Set flag indicating ACE process failed.
            HonorMemorialCards.wndHonorMemorial.ACEStatus = 100;

            return false;
        }


        private bool UpdateTables()
        {
            DataSet ds;
            bool bStatus;

            if (bNoDataToParse)
                return true;

            if (bStatus = GrabDataFromAceFile(out ds))
            {
                bStatus = UpdateDatabase(ds);
            }
            return (bStatus);
        }


        private bool UpdateDatabase(DataSet ds)
        {
            string sErrorMessage;
            bool bStatus = true;
            clsDatabase c = new clsDatabase();
            foreach (DataRow dr in ds.Tables["ACE"].Rows)
            {
                bStatus = c.uspUpdateParsedCards(dr, out sErrorMessage);
                if (sErrorMessage != "")
                {
                    MessageBox.Show("Problem updating ACE response - " + sErrorMessage);
                    break;
                }
                if (!bStatus) break;
            }
            return (bStatus);
        }

        private void runSyncAndGetResults(string sBatchFile)
        {
            try
            {
                Process proc = new Process();
                proc.StartInfo.FileName = sBatchFile;
                proc.StartInfo.Arguments = "";
                proc.Start();
                while (!CheckFile("Done.txt"))
                {
                    Thread.Sleep(1000);
                    DoEvents();
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Could not run ACE - " + e.Message);
            }
        }

        private bool GrabDataFromAceFile(out DataSet ds)
        {
            string sSQL;
            bool bStatus = true;
            string sCSVFolder;
            string sConnectString;
            ds = null;
            sCSVFolder = sDirectory;
            sConnectString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + sCSVFolder + ";";
            //
            //  Grab the data into a data set.
            //
            bStatus = true;
            sSQL = "select " +
                        "CardID, " +
                        "PrintGroup, " +
                        "Address1," +
                        "Address2, " +
                        "Address3, " +
                        "Address4, " +
                        "City, " +
                        "State, " +
                        "Zip, " +
                        "Country, " +
                        "BarCode, " +
                        "STDADDR1, " +
                        "STDADDR2, " +
                        "STDADDR3, " +
                        "STDADDR4, " +
                        "STDCITY, " +
                        "STDSTATE, " +
                        "STDZIP, " +
                        "STDZIP4, " +
                        "STDZIP10, " +
                        "STDZIPNODASH, " +
                        "STANDAD01, " +
                        "STANDAD02, " +
                        "STANDAD03, " +
                        "STANDAD04, " +
                        "STANDAD05, " +
                        "CART, " +
                        "DPBC, " +
                        "DPBC, " +
                        "LOT, " +
                        "LOTORDER, " +
                        "ACE_ERROR from ResponseOut.csv";

            ds = new DataSet();
            try
            {
                OdbcConnection conn;
                conn = new OdbcConnection(sConnectString);
                conn.Open();
                OdbcDataAdapter adapter = new OdbcDataAdapter();
                adapter.SelectCommand = new OdbcCommand(sSQL, conn);
                adapter.Fill(ds, "ACE");
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show("Could Not Get Parsed Names - " + e.ToString() + " " + sSQL);
                bStatus = false;
            }
            DoEvents();
            return bStatus;
        }
        //
        // Do events
        //
        private void DoEvents()
        {
            DispatcherFrame f = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
            (SendOrPostCallback)delegate(object arg)
            {
                DispatcherFrame fr = arg as DispatcherFrame;
                fr.Continue = false;
            }, f);
            Dispatcher.PushFrame(f);
            //Thread.Sleep(1000);
        }
    }
}

