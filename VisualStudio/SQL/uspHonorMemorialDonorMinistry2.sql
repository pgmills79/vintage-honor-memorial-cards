use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspHonorMemorialDonorMinistry2' AND type = 'P')
    DROP PROCEDURE dbo.uspHonorMemorialDonorMinistry2
GO
Create Procedure dbo.uspHonorMemorialDonorMinistry2
		@BeginDate datetime,
	    @EndDate datetime
as
set nocount on
BEGIN TRY
declare @db as datetime
declare @de as datetime
declare @iCount as int

select @db = dbo.udfMinDateTime(@BeginDate)
select @de = dbo.udfMaxDateTime(@EndDate)
--
-- Get honor card information for donor ministry.
--
select	C1.Recordid as CardDocumentNumber, 
		C1.AccountNumber, 
		rtrim(ltrim(FromName)) as FromName, 
		rtrim(ltrim(Memorial)) as MemorialName, 
		T01.Date as TransactionDate, 
		rtrim(ltrim(CardType)) as LetterCode, 
		rtrim(ltrim(ToName)) as ToAddress1, 
		rtrim(ltrim(Address1)) as ToAddress2, 
		rtrim(ltrim(Address2)) as ToAddress3, 
		rtrim(ltrim(Address3)) as ToAddress4, 
		ltrim(rtrim(City)) + ' ' +  ltrim(rtrim(State)) + ' ' + ltrim(rtrim(ZipPostal))  as ToAddress5, 
case
when Country='USA' then ''
else rtrim(ltrim(Country))
end as ToAddress6, rtrim(ltrim(C2.ProjectCode)) as Project, space(80) as ProjectDescription into #a from 
SPDSPROD.dbo.SP02_CardMaster C1, SPDSPROD.dbo.SP01_CardDetails C2, SPDSPROD.dbo.T01_TransactionMaster T01 where
C1.RecordId = C2.CardRecordId and
C1.DocumentNumber = T01.DocumentNumber and
C1.PrintStatus = 'D' and
T01.[Date] between @db and @de
--
-- Null thing, you make my heart sing.
--
update #a set ToAddress1 = '' where ToAddress1 is null
update #a set ToAddress2 = '' where ToAddress2 is null
update #a set ToAddress3 = '' where ToAddress3 is null
update #a set ToAddress4 = '' where ToAddress4 is null
update #a set ToAddress5 = '' where ToAddress5 is null
update #a set ToAddress6 = '' where ToAddress6 is null
update #a set FromName = '' where FromName is null
update #a set MemorialName = '' where MemorialName is null
update #a set LetterCode = '' where LetterCode is null
update #a set Project = '' where Project is null
-- 
-- Shift address to top.
--
select @iCount = 6
While (@iCount > 0)
BEGIN
	update #a set ToAddress1 = ToAddress2, 
                  ToAddress2 = ToAddress3,
				  ToAddress3 = ToAddress4,
				  ToAddress4 = ToAddress5,
				  ToAddress5 = ToAddress6,
				  ToAddress6 = '' 
		where ToAddress1 = ''
	update #a set ToAddress2 = ToAddress3,
				  ToAddress3 = ToAddress4,
				  ToAddress4 = ToAddress5,
				  ToAddress5 = ToAddress6,
				  ToAddress6 = '' 
		where ToAddress2 = ''	
	update #a set ToAddress3 = ToAddress4,
				  ToAddress4 = ToAddress5,
				  ToAddress5 = ToAddress6,
				  ToAddress6 = '' 
		where ToAddress3 = ''
	update #a set ToAddress4 = ToAddress5,
				  ToAddress5 = ToAddress6,
				  ToAddress6 = '' 
		where ToAddress4 = ''
	update #a set ToAddress5 = ToAddress6,
				  ToAddress6 = '' 
		where ToAddress5 = ''

	select @iCount = @iCount - 1
END
--
-- Add int product description.
--
update #a set projectdescription = ''
update #a set Projectdescription = Y01.Description from #a a, SPDSPROD.dbo.Y01_ProjectMaster Y01 where
a.project = Y01.projectcode
--
-- Return
--
select * from #a order by transactiondate, cardDocumentNumber, project
drop table #a
END TRY
BEGIN CATCH
		select substring('Error in  = ' + ERROR_PROCEDURE() + ', procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255)
END CATCH
