use SPAPPSPROD
go
drop function usfGetCardDate
go
CREATE function usfGetCardDate(@RecordID int, @CardOrItem char(1), @AddOrChange char(1))
returns datetime
begin
declare @ReturnDate datetime = null
if ((@CardOrItem = 'C') and (@AddOrChange = 'A'))
	select @ReturnDate = min(actiondatetime) from SPDSPROD.dbo.X30_AuditInformation where tablerecordid = @RecordID and tableid = 'SP02_' and action = 'INSERT'
if ((@CardOrItem = 'C') and (@AddOrChange = 'C'))
	select @ReturnDate = max(actiondatetime) from SPDSPROD.dbo.X30_AuditInformation where tablerecordid = @RecordID and tableid = 'SP02_' and action = 'UPDATE'
if ((@CardOrItem = 'I') and (@AddOrChange = 'A'))
	select @ReturnDate = min(actiondatetime) from SPDSPROD.dbo.X30_AuditInformation where tablerecordid = @RecordID and tableid = 'SP01_' and action = 'INSERT'
if ((@CardOrItem = 'I') and (@AddOrChange = 'C'))
	select @ReturnDate = max(actiondatetime) from SPDSPROD.dbo.X30_AuditInformation where tablerecordid = @RecordID and tableid = 'SP01_' and action = 'UPDATE'
return(@ReturnDate)
end

