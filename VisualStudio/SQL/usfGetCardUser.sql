use SPAPPSPROD
go
drop function usfGetCardUser
go
CREATE function usfGetCardUser(@RecordID int, @CardOrItem char(1), @AddOrChange char(1))
returns varchar(50)
begin
declare @ReturnUser varchar(200) = ''
declare @ReturnRecordID bigint = 0
if ((@CardOrItem = 'C') and (@AddOrChange = 'A'))
	select  @ReturnRecordID = min(RecordID) from SPDSPROD.dbo.X30_AuditInformation where tablerecordid = @RecordID and tableid = 'SP02_' and action = 'INSERT'
if ((@CardOrItem = 'C') and (@AddOrChange = 'C'))
	select @ReturnRecordID = max(RecordID) from SPDSPROD.dbo.X30_AuditInformation where tablerecordid = @RecordID and tableid = 'SP02_' and action = 'UPDATE'
if ((@CardOrItem = 'I') and (@AddOrChange = 'A'))
	select @ReturnRecordID = min(RecordID) from SPDSPROD.dbo.X30_AuditInformation where tablerecordid = @RecordID and tableid = 'SP01_' and action = 'INSERT'
if ((@CardOrItem = 'I') and (@AddOrChange = 'C'))
	select @ReturnRecordID = max(RecordID) from SPDSPROD.dbo.X30_AuditInformation where tablerecordid = @RecordID and tableid = 'SP01_' and action = 'UPDATE'

if ((@ReturnRecordID > 0) and (@ReturnRecordID is not null))
BEGIN
select @ReturnUser = left(replace(SourceId, 'SAMARITAN\', ''), 50) from SPDSPROD.dbo.X29_AccessorSessions X29, 
              SPDSPROD.dbo.X30_AuditInformation X30 where 
              X29.RecordId = X30.SessionRecordId and
              X30.recordID = @ReturnRecordID
END
return(@ReturnUser)
end

