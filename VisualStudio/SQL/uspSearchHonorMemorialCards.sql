use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspSearchHonorMemorialCards' AND type = 'P')
    DROP PROCEDURE dbo.uspSearchHonorMemorialCards
GO
Create Procedure dbo.uspSearchHonorMemorialCards
		@AccountNumber bigint,
		@BeginDate datetime,
		@EndDate datetime,
		@PrintStatus char(1),
		@Group int,
		@Of int,
		@FromName varchar(20),
		@ToName varchar(20),
		@MemorialName varchar(20),
		@Address1 varchar(20),
		@City varchar(20),
		@Zip varchar(10),
		@RowsReturned int output,
		@MaxLimitWarning int output,
		@Status int output,
		@ErrorMessage varchar(3000) output
as
set nocount on
BEGIN TRY
declare @iCount int
declare @SQL varchar(2048)
declare @Fields varchar(2048)
declare @MaxToReturn int
select @MaxToReturn = 1000  -- <-- Change here for maximum number of rows returned.
select @SQL = ''
select @status = 0
select @ErrorMessage = ''
select @RowsReturned = 0
select @MaxLimitWarning = 0
--
-- Make sure accounts are accurate.
--
update SPAPPSPROD.dbo.HonorMemorialCards1 set accountnumber = A11.AccountNumber from SPAPPSPROD.dbo.HonorMemorialCards1 HC1, SPDSPROD.dbo.A11_AccountMergeHeaders A11
where A11.mergedaccountnumber = HC1.AccountNumber
--
-- Make sure all is loaded.
--
--exec dbo.uspLoadHonorMemorialCards	@status output,	@ErrorMessage output
--if (@status <> 0) return
--
if @AccountNumber is null set @AccountNumber = 0
if @BeginDate is null set @BeginDate = '01/01/1900 00:00'
if @EndDate is null set @EndDate = getdate()
select @BeginDate = SPAPPSPROD.dbo.udfMinDateTime(@BeginDate)
select @EndDate = SPAPPSPROD.dbo.udfMaxDateTime(@EndDate)
if @PrintStatus is null set @PrintStatus = ''
if @Group is null set @Group = 0
if @Of is null set @Of = 0
if @FromName is null set @FromName = ''
if @ToName is null set @ToName = ''
if @PrintStatus is null set @PrintStatus = ''
if @Group is null set @Group = 0
if @Of is null set @Of = 0
if @FromName is null set @FromName = ''
if @ToName is null set @ToName = ''
if @MemorialName is null set @MemorialName = ''
if @Address1 is null set @Address1 = ''
if @City is null set @City = ''
if @Zip is null set @Zip = ''
set @PrintStatus = rtrim(ltrim(@PrintStatus))
set @FromName = rtrim(ltrim(@FromName))
set @ToName = rtrim(ltrim(@ToName))
set @MemorialName = rtrim(ltrim(@MemorialName))
set @Address1 = rtrim(ltrim(@Address1))
set @City = rtrim(ltrim(@City))
set @Zip = rtrim(ltrim(@Zip))
--
-- Fields
--
select @Fields = 
	'CardDocumentNumber, ' +
	'AccountNumber, ' +
	'SPAPPSPROD.[dbo].[udfFormatAccountName] (AccountNumber, ''ALL'') as AccountName, ' + 
	'isnull(DocumentNumber, 0) as DocumentNumber, ' +
	'rtrim(isnull(FromName, '''')) as FromName, ' +
	'rtrim(isnull(MemorialName, '''')) as MemorialName, ' +
	'convert(varchar(20), TransactionDate, 101) as TransactionDate, ' +
	'case when left(ltrim(LetterCode), 1) = ''H'' then ''Honor'' when left(ltrim(LetterCode), 1) = ''M'' then ''Memorial'' else LetterCode end as LetterCode , ' +
	'case when Status=''X'' then ''Delete'' 
	      when status = ''D'' then ''Donor Ministry'' 
	      when status = ''I'' then ''Entry In Progress''
	      when status = ''H'' then ''Hold''  
	      when Status=''N'' then ''No Print'' 
	      when status = ''Y'' then ''Ready'' 
	      when status = ''P'' then ''Printed'' 
		  when status = ''T'' then ''Test'' 
	      else status end as status, ' +
	'PrintGroup, ' +
	'rtrim(isnull(ToName, '''')) as ToName, ' +
	'rtrim(isnull(ToAddress1, '''')) as ToAddress1, ' +
	'rtrim(isnull(ToAddress2, '''')) as ToAddress2, ' +
	'rtrim(isnull(ToAddress3, '''')) as ToAddress3, ' +
	'rtrim(isnull(ToCity, '''')) as ToCity, ' +
	'rtrim(isnull(ToState, '''')) as ToState, ' +
	'rtrim(isnull(ToZip, '''')) as ToZip, ' +
	'rtrim(isnull(ToCountry, '''')) as ToCountry, ' +
	'NumberOfGifts, ' +
	'isnull(WebPageName, '''') as WebPageName, ' +
	'convert(varchar(20), AddDate, 100) as AddDate, ' +
	'isnull(ChangeUser, '''') as ChangeUser, ' +
	'isnull(convert(varchar(20), ChangeDate, 100), '''') as ChangeDate, ' +
	'case when ReturnToSender = ''Y''  then ''Yes'' when ReturnToSender= ''N'' then ''No'' else ReturnToSender end as ReturnToSender, ' +
    'dbo.usfGetCardGifts(CardDocumentNumber) as Gifts, ' +
    'dbo.usfGetCardAddUser(CardDocumentNumber) as AddUser, ' +
	''''' as NewStatus '
--
-- Main query
--
set @SQL = 'select top ' + convert(varchar(10), @MaxToReturn) + ' ' + @Fields + ' from SPAPPSPROD.dbo.HonorMemorialCards1 where '
--
-- Transaction Date
--
set @SQL = @SQL + 'TransactionDate between ''' + convert(varchar(20), @BeginDate) + ''' and ''' + convert(varchar(20), @EndDate) + ''' '
--
-- Do not pick up deleted items
--
set @SQL = @SQL + 'and Status <> ''Z'''
--
-- Account ID
--
if (@AccountNumber > 0)
	set @SQL = @SQL + 'and AccountNumber = ' + rtrim(ltrim(convert(varchar(15), @AccountNumber)))
--
-- Print Status
--
if ((@PrintStatus <> '') and (@PrintStatus <> 'A'))
	set @SQL = @SQL + 'and Status = ''' + @PrintStatus + ''' '
--
-- Filter
--
if ((@Group > 0) and (@Of > 0))
	set @SQL = @SQL + ' and (carddocumentnumber % ' + convert(varchar(10), @Group) + ') = ' + convert(varchar(10), @Of) + ' '
--
-- Name address like fields.
--
if (@FromName <> '')
	set @SQL = @SQL + ' and FromName like ''%' + @FromName + '%'' '
if (@ToName <> '')
	set @SQL = @SQL + ' and ToName like ''%' + @ToName + '%'' '
if (@MemorialName <> '')
	set @SQL = @SQL + ' and MemorialName like ''%' + @MemorialName + '%'' '
if (@Address1 <> '')
	set @SQL = @SQL + ' and ToAddress1 like ''%' + @Address1 + '%'' '
if (@City <> '')
	set @SQL = @SQL + ' and ToCity like ''%' + @City + '%'' '
if (@Zip <> '')
	set @SQL = @SQL + ' and ToZip like ''%' + @Zip + '%'' '
--
-- Order
--
select @SQL = @SQL + 'order by TransactionDate, AccountNumber, CardDocumentNumber '
--select @SQL
exec (@SQL)
select @RowsReturned = @@rowcount
select @ErrorMessage = @SQL
if (@RowsReturned >= @MaxToReturn) select @MaxLimitWarning = 1
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspSearchHonorMemorialCards, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) + ' ' + @SQL
	
END CATCH

-- select * from SPAPPSPROD.dbo.HonorMemorialCards1 
