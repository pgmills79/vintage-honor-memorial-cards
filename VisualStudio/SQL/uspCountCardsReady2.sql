use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspCountCardsReady2' AND type = 'P')
    DROP PROCEDURE dbo.uspCountCardsReady2
GO
Create Procedure dbo.uspCountCardsReady2
		@PullType char(1),
		@iCount int output,
		@status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
select @status = 0
select @ErrorMessage = ''
exec uspPullHonorCardsToPrintQueue
select @iCount = count(*) from SPAPPSPROD.dbo.HonorMemorialCards1 where Status = @PullType and AccountNumber > 0 and 
CardDocumentNumber in (select CardDocumentNumber from SPAPPSPROD.dbo.HonorMemorialCards2 where CardDocumentNumber = CardDocumentNumber)
if (@iCount is null) select @iCount = 0
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspCountCardsReady2, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH
