-- =============================================
-- Author:		Dave Sable
-- Create date: 8/13/2007
-- Description:	Give address information for
--              honor/memorial card.
-- =============================================
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspLogCard' AND type = 'P')
    DROP PROCEDURE dbo.uspLogCard
GO
CREATE PROCEDURE dbo.uspLogCard
				 @CardID float,
				 @LogType char(1),
			     @Message char(255)
AS
BEGIN
SET NOCOUNT ON
declare @d as datetime

select @d = getdate()

if @CardID is null set @CardID = 0
if @LogType is null set @LogType = '?'
if @Message is null set @Message = 'None'
insert into SPAPPSPROD.dbo.HonorMemorialCardsLog
(CardID, LogType, [Message], MessageTime)
values
(@CardID, @LogType, @Message, @d)
END
go



