IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspMarkHonorCards' AND type = 'P')
    DROP PROCEDURE dbo.uspMarkHonorCards
GO
Create Procedure dbo.uspMarkHonorCards
	    @pg float output,
		@status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
select @status = 0
select @ErrorMessage = ''
if exists (select * from SPAPPSPROD.dbo.HonorMemorialCards1 where Status = 'Y')
BEGIN
	exec uspGetNextPrintGroup @pg output
	update SPAPPSPROD.dbo.HonorMemorialCards1 set PrintGroup = @pg, Status = 'I' where Status = 'Y'
END
END TRY

BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspMarkHonorCards, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
END CATCH

