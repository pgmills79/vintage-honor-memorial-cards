use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspGetCardAddressToAce' AND type = 'P')
    DROP PROCEDURE dbo.uspGetCardAddressToAce
GO
Create Procedure dbo.uspGetCardAddressToAce
				@PrintGroup Int
as
set nocount on
BEGIN TRY
select Distinct 
		CardID,
		PrintGroup, 
       coalesce(ToAddress1, '') as AddressLine1, 
       coalesce(ToAddress2, '') as AddressLine2, 
       coalesce(ToAddress3, '') as AddressLine3, 
       '' as AddressLine4, 
       coalesce(ToCity, '') as City, 
       coalesce(ToState, '') as State, 
       coalesce(ToZip, '') as ZipPostal,
       coalesce(ToCountry, '') as Country  from  HonorCardFormatting
where PrintGroup = @PrintGroup --or 
--printgroup in (10016122, 10016218)
END TRY
BEGIN CATCH	
select 0 as CardID, 
	   0 as PrintGroup, 
       '' as AddressLine1, 
       '' as AddressLine2, 
       '' as AddressLine3, 
       '' as AddressLine4, 
       '' as City, 
       '' as State, 
       '' as ZipPostal,
       '' as Country 
END CATCH
