use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspGetGiftsOfCardDonorStudio' AND type = 'P')
    DROP PROCEDURE dbo.uspGetGiftsOfCardDonorStudio
GO
Create Procedure dbo.uspGetGiftsOfCardDonorStudio
		@CardID int,
		@Status int output,
		@ErrorMessage varchar(3000) output
as
set nocount on
BEGIN TRY
	select @status = 0
	select @ErrorMessage = 0
	select ProjectCode as project from SPDSPROD.dbo.SP01_CardDetails where CardRecordId = @CardID order by ProjectCode
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspGetGiftsOfCardDonorStudio, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH

-- select * from SPAPPSPROD.dbo.HonorMemorialCards1 
