-- =============================================
-- Author:		Dave Sable
-- Create date: 8/13/2007
-- Description:	Give address information for
--              honor/memorial card.
--
-- 12/5/2007 - Corrected problem where end card is called twice.
-- =============================================
use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspEndCardDonorStudio' AND type = 'P')
    DROP PROCEDURE dbo.uspEndCardDonorStudio
GO
CREATE PROCEDURE dbo.uspEndCardDonorStudio
				 @CardNumber bigint,
				 @Count int,
				 @DonorStudioRecordID bigint output,
                 @ErrorText char(80) output
AS
BEGIN
SET NOCOUNT ON
declare @CardID int
declare @Message char(255)
declare @Done as int
declare @DocumentNumber as float
declare @ErrorFlag int
declare @CountedCards int
--
--  Init
--
select @ErrorText = ''
select @CardID = @CardNumber
select @Done = 0
if (@Count is null) set @Count = 0
--
--  Check to see if transaction and account exists.
--
if not exists (select * from SPAPPSPROD.dbo.HonorMemorialCards1Temp where CardDocumentNumber = @CardNumber) and 
    (@Done = 0)
BEGIN
	select @ErrorText = 'Invalid Card number.'
	select @Message = 'Invalid Card Number'
	exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'E', @Message
	select @Done = 1
END
--
--  Make sure the count is correct.
--
select @CountedCards = count(*) from SPAPPSPROD.dbo.HonorMemorialCards2Temp where CardDocumentNumber = @CardNumber
if (@CountedCards <> @Count) and (@Done = 0)
BEGIN
	select @ErrorText = 'Number of gifts do not match.'
	select @Message = 'Number of gifts do not match.'
	exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'E', @Message
	select @Done = 1
END
--
--  Update count and status field.
--
if (@Done = 0)
BEGIN
		update SPAPPSPROD.dbo.HonorMemorialCards1Temp set NumberOfGifts = @CountedCards where CardDocumentNumber = @CardNumber
		if (@Count > 8) update SPAPPSPROD.dbo.HonorMemorialCards1Temp set Status = 'D' where CardDocumentNumber = @CardNumber
		if (@Count <= 8) update SPAPPSPROD.dbo.HonorMemorialCards1Temp set Status = 'H' where CardDocumentNumber = @CardNumber	
	    update SPAPPSPROD.dbo.HonorMemorialCards1Temp set Status = 'D' from SPAPPSPROD.dbo.HonorMemorialCards1Temp c1, SPAPPSPROD.dbo.HonorMemorialCards2Temp  c2 where
			c1.CardDocumentNumber = c2.CardDocumentNumber and
            Project not in (select Project from SPAPPSPROD.dbo.ChristmasCatalogDescriptions where
                                Project = Project) and c1.CardDocumentNumber = @CardNumber
END
	---------------------------------------------------------------------------------------------------
	--	Load to perminent table
	---------------------------------------------------------------------------------------------------
if (@Done = 0)
BEGIN
select @ErrorFlag = 0
BEGIN TRY
    BEGIN TRANSACTION
    INSERT INTO [SPDSPROD].[dbo].[SP02_CardMaster]
           ([DocumentNumber]
           ,[AccountNumber]
           ,[CardType]
           ,[FromName]
           ,[ToName]
           ,[Memorial]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[Address4]
           ,[City]
           ,[State]
           ,[ZipPostal]
           ,[Country]
           ,[CardStatus]
           ,[PrintStatus]
           ,[PrintDate]
           ,[ReprintDate]
           ,[PrintGroup]
           ,[LetterType]
           ,[ReturnToSender])
     select 
           DocumentNumber,
           AccountNumber,
           LetterCode,
           FromName,
           ToName,
           MemorialName,
           ToAddress1,
           ToAddress2,
           ToAddress3,
           '',
           ToCity,
           ToState,
           ToZip,
           ToCountry,
           Null,
           [Status],
           null,
           null,
           Null,
           Null,
           case when ReturnToSender = 'Y' then 1 else 0 end
	from SPAPPSPROD.dbo.HonorMemorialCards1Temp
		where CardDocumentNumber = @CardNumber
	select @DonorStudioRecordID = SCOPE_IDENTITY()
	---------------------------------------------------------------------------------------------------
	--	Add the items
	---------------------------------------------------------------------------------------------------
	declare @Project varchar(10)
	declare @AddDate datetime
	
	DECLARE ItemCursor CURSOR FOR
	select Project, AddDate from SPAPPSPROD.dbo.HonorMemorialCards2Temp where CardDocumentNumber = @CardNumber
	OPEN ItemCursor
	WHILE (1 = 1)
	BEGIN
		  FETCH NEXT FROM ItemCursor into @Project, @AddDate
		  if (@@FETCH_STATUS != 0) break
			INSERT INTO [SPDSPROD].[dbo].[SP01_CardDetails]
					   ([CardRecordId]
					   ,[ProjectCode])
				 VALUES
					   (@DonorStudioRecordID,
					   @Project)
--
--  Add insert date/time
--
	END
	CLOSE ItemCursor
	DEALLOCATE ItemCursor
	---------------------------------------------------------------------------------------------------
	--	Delete from temporary
	---------------------------------------------------------------------------------------------------	
	delete from SPAPPSPROD.dbo.HonorMemorialCards1Temp where CardDocumentNumber = @CardNumber
	delete from SPAPPSPROD.dbo.HonorMemorialCards2Temp where CardDocumentNumber = @CardNumber
    COMMIT TRANSACTION;
END TRY
BEGIN CATCH
    -- Rollback any active or uncommittable transactions before
    -- inserting information in the ErrorLog
    IF XACT_STATE() <> 0
    BEGIN
        ROLLBACK TRANSACTION
		select @Message = 'Rolling back transaction 1'
		exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'W', @Message
		select @Message = 'Rolling back transaction 2'
		exec SPAPPSPROD.dbo.uspLogCard	 @DonorStudioRecordID, 'W', @Message
    END
	select @ErrorFlag = 1
END CATCH; 
END
--
--  Flag error if necessary
--
if (@ErrorFlag = 0) and (@Done = 0)
BEGIN
	select @Message = 'Card complete 1'
	exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'I', @Message
	select @Message = 'Card complete 2'
	exec SPAPPSPROD.dbo.uspLogCard	 @DonorStudioRecordID, 'I', @Message
END
else
BEGIN
	if (@@Error <> 0)
	BEGIN
		select @Message = 'Completion error - ' + convert(char(20), @@Error)
		exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'I', @Message
	END
END
set nocount off
END
go
