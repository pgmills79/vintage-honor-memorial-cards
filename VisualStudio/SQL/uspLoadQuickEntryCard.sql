-- =============================================
-- Author:		Dave Sable
-- Create date: 9/27/2007
-- Description:	Load one card into print table.
-- =============================================
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspLoadQuickEntryCard' AND type = 'P')
    DROP PROCEDURE dbo.uspLoadQuickEntryCard
GO
CREATE PROCEDURE dbo.uspLoadQuickEntryCard
				 @CardNumber float
AS
BEGIN
set nocount on
declare  @Count float
declare @ErrorText char(80)
declare @ErrorFlag int
declare @Message char(80)
--
--  Count then number of gifts to be loaded.
--
select @ErrorFlag = 0
--select @Count = count(*)  from proddta.f55de03b with (nolock), proddta.f55de03a with (nolock)
--where
--cd55crdno = @CardNumber and CD55CRDST  = 'Y' and
--cd55ccqdn = cp55ccqdn and
--cd55crdno = cp55crdno
----
----  Insert the card information into the temporary table.
----
--BEGIN TRY
--	BEGIN TRANSACTION
--	insert into SPAPPSPROD.dbo.HonorMemorialCards1t
--	(CardDocumentNumber, AccountNumber, DocumentNumber, FromName, MemorialName, TransactionDate, LetterCode, Status, Occurrance, PrintGroup, ToName, ToAddress1, ToAddress2, ToAddress3, ToCity, ToState, ToZip, ToCountry, C1Z103IMST, NumberOfGifts, C155WPN, AddDate, C1Z103ADTM, ChangeUser, ChangeDate, C1Z103CHTM, ReturnToSender)
--	select 
--	CD55CRDNO, THZ103ACNO, 0, CD55CCQFM, CD55CCQMN, THZ103TRDT, CDZ103LTR, '?', (convert(int, CD55CRDNO) % 5) + 1, 0, CD55CCQTO, CDZ103ADR1, CDZ103ADR2, CDZ103ADR3, CDZ103CITY, CDZ103ST, CDZ103ZIP, CDZ103CNTY, 'H', 0, 'QUICKSCREEN', dbo.udfConvCharDateToJde(convert(char(20), getdate(), 101)), dbo.udfConvTimeToJde(getdate()), '', 0, 0, CD55CCRTC
--	from proddta.f55de03a with (nolock), proddta.f55de02 with (nolock)
--	where
--	th55ccqdn = cd55ccqdn and
--	cd55crdno = @CardNumber and CD55CRDST  = 'Y'
----
----  Insert the card projects into the temporary table.
----
--	insert into SPAPPSPROD.dbo.HonorMemorialCards2t
--	(CardDocumentNumber, Project, AddDate, C2Z103ADTM)
--	select 
--	cp55CRDNO, CpZ103PROJ, dbo.udfConvCharDateToJde(convert(char(20) , getdate(), 101)), dbo.udfConvTimeToJde(getdate())
--	from proddta.f55de03b, proddta.f55de03a with (nolock)
--	where
--	cd55crdno = @CardNumber and CD55CRDST  = 'Y' and
--	cd55ccqdn = cp55ccqdn and
--	cd55crdno = cp55crdno
--    COMMIT TRANSACTION;
--	select @Message = 'Added quick entry card information.'
--	exec proddta.uspLogCard	 @CardNumber, 'I', @Message
--END TRY
--BEGIN CATCH
--    -- Rollback any active or uncommittable transactions before
--    -- inserting information in the ErrorLog
--    IF XACT_STATE() <> 0
--    BEGIN
--        ROLLBACK TRANSACTION
--		select @Message = 'Rolling back transaction'
--		exec proddta.uspLogCard	 @CardNumber, 'W', @Message
--    END
--	select @ErrorFlag = 1
--END CATCH; 
----
----  Load it into the real thing and check for errors.
----
--if (@ErrorFlag = 0)
--BEGIN
--	exec proddta.uspEndCard	@CardNumber, @Count, @ErrorText output
--END
----
----  If all is well, make it so the data entry operator does not have to
----  review this card.  Mark as having been loaded.
----
--if ((@ErrorText = '') and (@ErrorFlag = 0)) 
--BEGIN
--	update SPAPPSPROD.dbo.HonorMemorialCards1 set Status = 'Y' where
--           CardDocumentNumber = @CardNumber and
--           Status = 'H'
--	update SPAPPSPROD.dbo.HonorMemorialCards1 set Status = 'D' where
--           CardDocumentNumber = @CardNumber and
--           ToCountry <> 'USA' and
--		   ToCountry <> 'CANADA' and
--           Status = 'Y'
--	update proddta.f55de03a set CD55CRDST  = 'P' where 
--			cd55crdno = @CardNumber 
--END

set nocount off
END
go


