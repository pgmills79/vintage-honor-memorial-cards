-- Something I saved in HonorMemorialCard

use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspAddHonorMemorialCardWithUserDonorStudio' AND type = 'P')
    DROP PROCEDURE dbo.uspAddHonorMemorialCardWithUserDonorStudio
GO
Create Procedure dbo.uspAddHonorMemorialCardWithUserDonorStudio
		@DocumentNumber bigint,
		@CardNumber bigint output,
		@UserName varchar(30),
		@Status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
	declare @AccountNumber bigint
	declare @ErrorText varchar(80)
	declare @AccountName varchar(80)
	declare @WebName varchar(80)
	declare @DonorStudioRecordID bigint

	select @Status = 0
	select @ErrorMessage = ''

	select @AccountNumber = 0
	select @AccountNumber = AccountNumber from SPDSPROD.dbo.T01_TransactionMaster where Documentnumber = @DocumentNumber
	if (@AccountNumber = 0 or @AccountNumber is null)
	BEGIN
		select @Status = 1
		select @ErrorMessage = 'Document number does not exist.'
	END		
	if (@Status = 0)
	BEGIN
		if (coalesce(@UserName, '') <> '')
			select @WebName = left('View Program - ' + replace(replace(@UserName, 'Samaritan\', ''), 'BG\', ''), 30)
		else
			select @WebName = 'View Program'
			
		select @AccountName = SPAPPSPROD.dbo.udfFormatAccountName (@AccountNumber, 'ALL')
		exec SPAPPSPROD.dbo.uspStartCardVersion2
					 @DocumentNumber,
					 @AccountName,
					 'HCC',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'N',
					@WebName,
					@CardNumber output,
					@ErrorText output
		if (@ErrorText <> '')
		BEGIN
			select @Status = 1
			select @ErrorMessage = @ErrorText
		END
	END
	if (@Status = 0)
	BEGIN
		exec  SPAPPSPROD.dbo.uspEndCardDonorStudio
					 @CardNumber,
					 0,
					 @DonorStudioRecordID output,
				     @ErrorText output
		select @CardNumber = @DonorStudioRecordID		-- Return the Donor Studio number not the traditional card number.

		-----------------------------------------------------------------------------------------------------------
		-- Check for error.
		-----------------------------------------------------------------------------------------------------------

		if (@ErrorText <> '')
		BEGIN
			select @Status = 1
			select @ErrorMessage = @ErrorText
		END
		-----------------------------------------------------------------------------------------------------------
		-- Add auditing information
		-----------------------------------------------------------------------------------------------------------
		exec dbo.uspAddCardAudit
			@CardNumber,
			@UserName,
			'C',
			'A',
			@Status output,
			@ErrorMessage output
		-----------------------------------------------------------------------------------------------------------
		-- Update Status
		-----------------------------------------------------------------------------------------------------------
		update SPDSPROD.dbo.SP02_CardMaster set printstatus = 'I' where RecordId = @CardNumber
	END
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspAddHonorMemorialCardWithUserDonorStudio, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH
