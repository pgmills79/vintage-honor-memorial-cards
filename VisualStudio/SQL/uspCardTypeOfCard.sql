IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspGetTypeOfCard' AND type = 'P')
    DROP PROCEDURE dbo.uspGetTypeOfCard
GO
Create Procedure dbo.uspGetTypeOfCard
		@CardID int,
		@CardType char(1) output,
		@InMemorialOf varchar(50) output,
		@ReturnToSender char(1) output,
		@Status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
	select @status = 0
	select @ErrorMessage = 0
	select @CardType = left(lettercode, 1) from SPAPPSPROD.DBO.Honormemorialcards1 where CardDocumentNumber = @CardID
	select @InmemorialOf = MemorialName from SPAPPSPROD.DBO.Honormemorialcards1 where CardDocumentNumber = @CardID
	select @ReturnToSender = ReturnToSender from SPAPPSPROD.DBO.Honormemorialcards1 where CardDocumentNumber = @CardID
	if (@CardType not in ('H', 'M')) select @CardType = 'H'
	if (@InMemorialOf is null) select @InMemorialOf = ''
	if (@ReturnTOSender not in ('Y', 'N')) select @ReturnToSender = 'Y'
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspCardTypeOfCard, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH

-- select * from SPAPPSPROD.dbo.HonorMemorialCards1 
