IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspHonorMemorialCombineDup' AND type = 'P')
    DROP PROCEDURE dbo.uspHonorMemorialCombineDup
GO
Create Procedure dbo.uspHonorMemorialCombineDup
						@Status as int output,
						@ErrorMessage as varchar(255) output
as
set nocount on
BEGIN TRY
select @status = 0
select @ErrorMessage = ''
--
--  Combine gift cards with exact names.
--
select
min(CardDocumentNumber) as KeepID, AccountNumber, DocumentNumber, FromName, MemorialName, TransactionDate, LetterCode, ToName, ToAddress1, ToAddress2, ToAddress3, ToCity, ToState, ToZip, ToCountry, count(*) as NumCards
into #Keep from SPAPPSPROD.dbo.HonorMemorialCards1
where -- TransactionDate >= 
[Status] in ('Y', 'H', 'D') and AccountNumber > 0 and DocumentNumber > 0
group by
AccountNumber, DocumentNumber, FromName, MemorialName, TransactionDate, LetterCode, ToName, ToAddress1, ToAddress2, ToAddress3, ToCity, ToState, ToZip, ToCountry
having count(*) > 1
-- List of duplicate cards
select a.CardDocumentNumber as DontKeep, b.keepid into #keep2 from 
SPAPPSPROD.dbo.HonorMemorialCards1 a, #keep b where
a.AccountNumber = b.AccountNumber and
a.DocumentNumber = b.DocumentNumber and 
isnull(a.FromName, '') = isnull(b.FromName, '') and
isnull(a.MemorialName, '') = isnull(b.MemorialName, '') and
a.TransactionDate = b.TransactionDate and
isnull(a.LetterCode, '') = isnull(b.LetterCode, '') and
isnull(a.ToName, '') = isnull(b.ToName, '') and
isnull(a.ToAddress1, '') = isnull(b.ToAddress1, '') and
isnull(a.ToAddress2, '') = isnull(b.ToAddress2, '') and
isnull(a.ToAddress3, '') = isnull(b.ToAddress3, '') and
isnull(a.ToCity, '') = isnull(b.ToCity, '') and
isnull(a.ToState, '') = isnull(b.ToState, '') and
isnull(a.ToZip, '') = isnull(b.ToZip, '') and
isnull(a.ToCountry, '') = isnull(b.ToCountry, '') and
CardDocumentNumber <> keepid

-- Get all projects to load onto the card we are going to keep.
select distinct CardDocumentNumber as dcno, Project as proj into #allproj from #keep2, SPAPPSPROD.dbo.HonorMemorialCards2 where
keepid = CardDocumentNumber
union
select keepid as dcno, Project as proj from #keep2, SPAPPSPROD.dbo.HonorMemorialCards2 where
dontkeep = CardDocumentNumber
-- Insert the gifts onto this card.
	BEGIN TRANSACTION
	insert into SPAPPSPROD.dbo.HonorMemorialCards2
	(CardDocumentNumber, Project, AddDate)
	select dcno, proj,  getdate() from #allproj where 
	dcno not in (select CardDocumentNumber from SPAPPSPROD.dbo.HonorMemorialCards2 where dcno = CardDocumentNumber and proj = Project)

-- Get rid of the other cards
	delete SPAPPSPROD.dbo.HonorMemorialCards1 from SPAPPSPROD.dbo.HonorMemorialCards1, #keep2 where
	CardDocumentNumber = dontkeep
	delete SPAPPSPROD.dbo.HonorMemorialCards2 from SPAPPSPROD.dbo.HonorMemorialCards2, #keep2 where
	CardDocumentNumber = dontkeep
	update webdonationcards set cardnumber = keepid from #keep2, webdonationcards where
	cardnumber = dontkeep
	COMMIT TRANSACTION
-- Get the gift count right
update SPAPPSPROD.dbo.HonorMemorialCards1 
set NumberOfGifts = (select count(*) from SPAPPSPROD.dbo.HonorMemorialCards2 HM2
                     where HM1.CardDocumentNumber = HM2.CardDocumentNumber)
from SPAPPSPROD.dbo.HonorMemorialCards1 HM1
where HM1.NumberOfGifts <> (select count(*) from SPAPPSPROD.dbo.HonorMemorialCards2 HM2
                     where HM1.CardDocumentNumber = HM2.CardDocumentNumber) and
HM1.CardDocumentNumber in (select keepid from #keep2 where keepid = HM1.CardDocumentNumber)
-- Get the status right
update SPAPPSPROD.dbo.HonorMemorialCards1 set [Status] = 'D' where
       NumberOfGifts > 8 and [Status] <> 'D' and
CardDocumentNumber in (select keepid from #keep2 where keepid = CardDocumentNumber)
drop table #keep
drop table #keep2
END TRY
BEGIN CATCH	
	if (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
	select @status = 1 
	select @ErrorMessage = substring('Error in  = ' + ERROR_PROCEDURE() + ', procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255)
END CATCH
