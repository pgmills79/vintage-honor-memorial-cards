IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspGridCardPrintGroups' AND type = 'P')
    DROP PROCEDURE dbo.uspGridCardPrintGroups
GO
Create Procedure dbo.uspGridCardPrintGroups
		@status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
select @status = 0
select @ErrorMessage = ''
select PrintGroup, max(DateGenerated) as PrintDate, count(*)  as NumberOfCards
	   from dbo.HonorCardFormatting 
	   group by [PrintGroup]
	   order by [PrintGroup] Desc
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspGridCardPrintGroups, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH