use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspUpdateHonorMemorialCardsDonorStudio' AND type = 'P')
    DROP PROCEDURE dbo.uspUpdateHonorMemorialCardsDonorStudio
GO
Create Procedure dbo.uspUpdateHonorMemorialCardsDonorStudio
		@CardID int,
		@FromName varchar(50),
		@ToName varchar(50),
		@MemorialName varchar(50),
		@ToAddress1 varchar(70),
		@ToAddress2 varchar(70),
		@ToAddress3 varchar(70),
		@ToCity varchar(40),
		@ToState varchar(10),
		@ToZip varchar(15),
		@ToCountry varchar(10),
		@NewStatus char(1),
		@UserName varchar(50),
		@UpdatedStatus varchar(10) output,
		@RecordUpdated int output,
		@NewChangedName varchar(50) output,
		@NewChangedDate varchar(20) output,
		@Status int output,
		@WarningMessage varchar(256) output,
		@ErrorMessage varchar(256) output
as
set nocount on
BEGIN TRY
declare @StatusToChangeTo char(1)
declare @NumberOfGifts int
select @UpdatedStatus = ''
select @status = 0
select @WarningMessage = ''
select @ErrorMessage = ''
select @RecordUpdated = 0
select @NewChangedDate = ''
select @NewChangedName = ''
--
-- Did we pass along the correct status?
--
if (@NewStatus not in ('I', 'P', 'Y', 'N', 'D', 'H', 'X', 'T', ''))
BEGIN
	select @WarningMessage = 'You tried to change to an invalid status.  Valid are D-Donor Ministry, H - Hold, N - Do not print, Y - Ready to print, T - Test, X - Delete.  Record was not updated.  Status: ' + @NewStatus + '.'
	return
END
--
-- Modify status to what we want to change to.
--
if (@NewStatus is null) set @NewStatus = ''
if (@NewStatus <> '') 
	select @StatusToChangeTo = @NewStatus
else
	select @StatusToChangeTo = [PrintStatus] from SPDSPROD.dbo.SP02_CardMaster where RecordId = @CardID
	


--
-- Update number of gifts.
--
select @NumberOfGifts = 0
select @NumberOfGifts = count(*) from SPDSPROD.dbo.SP01_CardDetails where CardRecordId = @CardID
--
--  Check for changing status.
--
if (@StatusToChangeTO = 'T') and (@NumberOfGifts > 8)
BEGIN
	select @WarningMessage = 'You cannot create a test account with more than eight gifts.'
	return
END

if (@StatusToChangeTo = 'Y')
BEGIN
	if (@NumberOfGifts > 8)
	BEGIN
		select @WarningMessage = 'There were more than eight gifts so status will be changed to Donor Minsitry'
		select @StatusToChangeTo = 'D'
	END
	if exists (select * from  SPDSPROD.dbo.SP01_CardDetails HMC2 where CardRecordId = @CardID and ProjectCode not in
			   (select project  from SPAPPSPROD.dbo.christmasCatalogDescriptions ccd where ccd.project = hmc2.ProjectCode))
	BEGIN
		select @WarningMessage = 'Some projects on the card are not defined so status will be changed to Donor Ministry'
		select @StatusToChangeTo = 'D'
	END
END
--
-- Get current status if blank
--
if ((@StatusToChangeTo = '') or (@Status is null))
BEGIN
	select @StatusToChangeTo = [PrintStatus] from SPDSPROD.dbo.SP02_CardMaster 
    where Recordid = @CardID
END
--
-- Update the record
--
update SPDSPROD.dbo.SP02_CardMaster set 
	FromName = @FromName,
	Memorial = @MemorialName,
	ToName = @ToName,
	Address1 = @ToAddress1,
	Address2 = @ToAddress2,
	Address3 = @ToAddress3,
	City = @ToCity,
	State = @ToState,
	ZipPostal = @ToZip,
	Country = @ToCountry,
	[PrintStatus] = @StatusToChangeTo
    where RecordId = @CardID
	select @RecordUpdated = 1

--
-- Add audit
--
	exec dbo.uspAddCardAudit
		@CardID,
		@UserName,
		'C',
		'C',
		@Status output,
		@ErrorMessage output
--
-- Give back the expanded status.
--
	select @UpdatedStatus = [PrintStatus] from SPDSPROD.dbo.SP02_CardMaster 
    where RecordId = @CardID
    
    select @NewChangedName = @UserName
    select @NewChangedDate = GETDATE()

	--select @UpdatedStatus = case when @StatusToChangeTo = 'N' then 'No Print' 
 --                                when @StatusToChangeTo = 'P' then 'Printed' 
 --                                when @StatusToChangeTo = 'Y' then 'Ready' 
 --                                when @StatusToChangeTo = 'H' then 'Hold' 
 --                                when @StatusToChangeTo = 'D' then 'DM Dept'  
 --                                else 'Unknown' 
 --                            end
	select @UpdatedStatus = @StatusToChangeTo
	if (@UpdatedStatus is null) select @UpdatedStatus = ''
--
-- Get change date and time
--
IF (@NewChangedName is null) select @NewChangedName = ''
if (@NewChangedDate is null) select @NewChangedDate = ''
END TRY
BEGIN CATCH
	select @status = 1 
	select @RecordUpdated = 0
	select @ErrorMessage = substring('Error in  = uspUpdateHonorMemorialCardsDonorStudio, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) + ' '
	
END CATCH

-- select * from SPDSPROD.dbo.SP02_CardMaster 
