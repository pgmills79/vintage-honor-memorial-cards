USE [SPDSPROD]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  trigger [dbo].[SP01_CardDetails_update]
	on [dbo].[SP01_CardDetails] for update
	as
      Begin
		begin try
			DECLARE @MasterrecordID bigint
			DECLARE @ItemrecordID bigint
			DECLARE insert_cursor CURSOR FAST_FORWARD FOR
			select CardRecordID, RecordID from inserted
			OPEN insert_cursor
			WHILE (1 = 1) 
			BEGIN
				FETCH NEXT FROM insert_cursor into @MasterrecordID, @ItemrecordID
				if (@@FETCH_STATUS != 0) break
				exec SPAPPSPROD.dbo.uspHonorCardInSyncQueue @MasterrecordID, @ItemrecordID
			END
			CLOSE insert_cursor
			DEALLOCATE insert_cursor
		End try
		begin catch
			declare @i int
			select @i = 1
		end catch
		set NOCOUNT OFF
end

