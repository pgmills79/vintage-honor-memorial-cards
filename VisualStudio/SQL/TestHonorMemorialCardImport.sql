use SPAPPSPROD
go
update WebDonationNames set loadedtoimport = 0 from dbo.WebDonationNames n, dbo.WebDonationCards c where
n.ID = c.ID and
n.DataSource = c.DataSource and
BankDate = '2011-01-25 00:00:00.000'

update SPDSPROD.dbo.Q04_ImportDetails set status = 'P' where status <> 'P'

select * from SPDSPROD.dbo.Q04_ImportDetails Q04, SPDSPROD.dbo.A01_AccountMaster A01 where
Q04.AccountNumber = A01.accountnumber and
A01.status = 'D'


declare 	@status int
declare		@ErrorMessage varchar(255)
exec dbo.uspLoadWebDonationsToImport '', @Status output, @ErrorMessage output
select @status, @ErrorMessage

select * from SPDSPROD.dbo.QSP01_ImportCardDetails QSP01, SPDSPROD.dbo.QSP02_ImportCardMaster QSP02 where
QSP01.CardImportRecordId = QSP02.RecordId

