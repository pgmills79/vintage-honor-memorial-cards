IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspCommitGiftCardProject' AND type = 'P')
    DROP PROCEDURE dbo.uspCommitGiftCardProject
GO
Create Procedure dbo.uspCommitGiftCardProject
		@CardID int,
		@Guid varchar(200),
		@GiftLine varchar(2048) output,
		@Status int output,
		@ErrorMessage varchar(256) output
as
set nocount on
BEGIN TRY
	select @GiftLine = ''
	select @Status = 0
	select @ErrorMessage = ''

	insert into SPAPPSPROD.dbo.HonorMemorialCards2
	(CardDocumentNumber, Project, AddDate)
	select CardID, Project, getdate() from SPAPPSPROD.dbo.HonorMemorialItemTemp t where
				   [Guid] = @Guid and
				   [CardID] = @Cardid and
				   [Project] not in (select Project from SPAPPSPROD.dbo.HonorMemorialCards2 HMC2 where
                                     HMC2.CardDocumentNumber = t.CardID and
                                     HMC2.Project = t.Project)

	delete SPAPPSPROD.dbo.HonorMemorialCards2 from SPAPPSPROD.dbo.HonorMemorialCards2 HMC2 where [Project] not in
				(select [Project] from SPAPPSPROD.dbo.HonorMemorialItemTemp t where
						HMC2.CardDocumentNumber = t.CardID and
                        HMC2.Project = t.Project and
						t.[Guid] = @Guid and
						t.[CardID] = @Cardid) and
			CardDocumentNumber = @CardID

	select @GiftLine = SPAPPSPROD.dbo.usfGetCardGifts(@CardID)
	delete from SPAPPSPROD.dbo.HonorMemorialItemTemp where [Guid] = @Guid
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspCommitGiftCardProject, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH
