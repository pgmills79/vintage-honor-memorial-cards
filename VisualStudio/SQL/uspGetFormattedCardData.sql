use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspGetFormattedCardData' AND type = 'P')
    DROP PROCEDURE dbo.uspGetFormattedCardData
GO
Create Procedure dbo.uspGetFormattedCardData
		@PrintGroup int,
		@status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
declare @iCount int
select @status = 0
select @ErrorMessage = ''
select 
PrintGroup, 
CardID,
DocumentNumber, 
AccountNumber, 
[Type], 
COALESCE([FileName], '') as FileName,
DateGenerated, 
COALESCE(FromName, '') as FromName, 
COALESCE(MemorialName, '') as MemorialName, 
COALESCE(ToName, '') as ToName, 
COALESCE(StdAddress1, '') as  ToAddress1, 
COALESCE(StdAddress2, '') as  ToAddress2, 
COALESCE(StdAddress3, '') as  ToAddress3, 
COALESCE(StdCity, '')  + ', ' + COALESCE(StdState, '')  + '  ' + COALESCE(StdZip, '') as  ToAddress4,
replace(COALESCE(ToCountry, ''), 'USA', '') as  ToAddress5, 
COALESCE(StdError, '') as StdError, 
COALESCE(Project01, '') as Project01, replace(COALESCE(ShortDescription01, ''), '\n', CHAR(13)+CHAR(10)) as ShortDescription01, replace(COALESCE(LongDescription01, ''), '\n', CHAR(13)+CHAR(10)) as LongDescription01, 
COALESCE(Project02, '') as Project02, replace(COALESCE(ShortDescription02, ''), '\n', CHAR(13)+CHAR(10)) as ShortDescription02, replace(COALESCE(LongDescription02, ''), '\n', CHAR(13)+CHAR(10)) as LongDescription02, 
COALESCE(Project03, '') as Project03, replace(COALESCE(ShortDescription03, ''), '\n', CHAR(13)+CHAR(10)) as ShortDescription03, replace(COALESCE(LongDescription03, ''), '\n', CHAR(13)+CHAR(10)) as LongDescription03, 
COALESCE(Project04, '') as Project04, replace(COALESCE(ShortDescription04, ''), '\n', CHAR(13)+CHAR(10)) as ShortDescription04, replace(COALESCE(LongDescription04, ''), '\n', CHAR(13)+CHAR(10)) as LongDescription04, 
COALESCE(Project05, '') as Project05, replace(COALESCE(ShortDescription05, ''), '\n', CHAR(13)+CHAR(10)) as ShortDescription05, replace(COALESCE(LongDescription05, ''), '\n', CHAR(13)+CHAR(10)) as LongDescription05, 
COALESCE(Project06, '') as Project06, replace(COALESCE(ShortDescription06, ''), '\n', CHAR(13)+CHAR(10)) as ShortDescription06, replace(COALESCE(LongDescription06, ''), '\n', CHAR(13)+CHAR(10)) as LongDescription06, 
COALESCE(Project07, '') as Project07, replace(COALESCE(ShortDescription07, ''), '\n', CHAR(13)+CHAR(10)) as ShortDescription07, replace(COALESCE(LongDescription07, ''), '\n', CHAR(13)+CHAR(10)) as LongDescription07, 
COALESCE(Project08, '') as Project08, replace(COALESCE(ShortDescription08, ''), '\n', CHAR(13)+CHAR(10)) as ShortDescription08, replace(COALESCE(LongDescription08, ''), '\n', CHAR(13)+CHAR(10)) as LongDescription08, 
COALESCE(Project09, '') as Project09, replace(COALESCE(ShortDescription09, ''), '\n', CHAR(13)+CHAR(10)) as ShortDescription09, replace(COALESCE(LongDescription09, ''), '\n', CHAR(13)+CHAR(10)) as LongDescription09, 
COALESCE(Project10, '') as Project10, replace(COALESCE(ShortDescription10, ''), '\n', CHAR(13)+CHAR(10)) as ShortDescription10, replace(COALESCE(LongDescription10, ''), '\n', CHAR(13)+CHAR(10)) as LongDescription10,
0 AS CardNumber,
0 AS OfNumber,
SPACE(20) AS CardTag
into #xx from dbo.HonorCardFormatting where PrintGroup = @PrintGroup
select @iCount = 5
while (@iCount > 0)
BEGIN
	update #xx set ToAddress1 = ToAddress2, ToAddress2=ToAddress3, ToAddress3=ToAddress4, ToAddress4 = ToAddress5, ToAddress5 = '' where ToAddress1 = ''
	update #xx set ToAddress2=ToAddress3, ToAddress3=ToAddress4, ToAddress4 = ToAddress5, ToAddress5 = '' where ToAddress2 = ''
	update #xx set ToAddress3=ToAddress4, ToAddress4 = ToAddress5, ToAddress5 = '' where ToAddress3 = ''
	update #xx set ToAddress4 = ToAddress5, ToAddress5 = '' where ToAddress4 = ''
	select @iCount = @iCount - 1
END
-----------------------------------------------------------------------------------------------------
--  Add the count number
------------------------------------------------------------------------------------------------------
DECLARE @CurrentPrintGroup INT
DECLARE @CardID BIGINT
DECLARE @Type INT
DECLARE @FileName VARCHAR(50)
DECLARE @Tag VARCHAR(50)
DECLARE @LastFileName VARCHAR(50) = ''
DECLARE @iCnt INT = 0
DECLARE @iTotal INT = 0

DECLARE CardCursor CURSOR FOR
SELECT PrintGroup, CardID, Type, FileName from #xx ORDER BY printgroup, Type, accountNumber, cardid
OPEN CardCursor
WHILE (1 = 1)
BEGIN
      FETCH NEXT FROM CardCursor into @CurrentPrintGroup, @CardID, @Type, @FileName
      if (@@FETCH_STATUS != 0) break
	  IF (@FileName <> @LastFileName) SET @iCnt = 0
	  SET @iCnt = @iCnt + 1
	  SELECT @iTotal = COUNT(*) FROM #xx WHERE FILENAME = @Filename
	  SET @Tag = CONVERT(VARCHAR(10), (@CurrentPrintGroup % 10000)) + '-' + RIGHT('000' + CONVERT(varchar(10), @Type), 3) + ':' +
	             CONVERT(VARCHAR(10), @iCnt)  + '/' + CONVERT(VARCHAR(10), @iTotal)
	  UPDATE #xx SET Cardnumber = @iCnt,
	                 OfNumber = @iTotal,
					 CardTag = @Tag WHERE cardid = @CardID

	  SET @LastFileName = @FileName
END
CLOSE CardCursor
DEALLOCATE CardCursor

SELECT * FROM #xx ORDER BY printgroup, Type, AccountNumber, cardid

drop table #xx
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspGetFormattedCardData, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH