use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspReportCardSegments2' AND type = 'P')
    DROP PROCEDURE dbo.uspReportCardSegments2
GO
Create Procedure dbo.uspReportCardSegments2
		@PrintGroup int
as
set nocount on
BEGIN TRY
SELECT 
(type / 100) * 100 AS type,
SPACE(80) AS Folder,
[FileName] + space(80) as FileName, count(*) as NumberInFile
	   into #x from dbo.HonorCardFormatting where PrintGroup = @PrintGroup 
	   group by (type / 100) * 100 , [FileName]
	   order by [FileName]
update #x set FileName = rtrim(ltrim(FileName))
UPDATE #x SET Folder = '000 - Catalog Cards' WHERE type between 0 AND 99
UPDATE #x SET Folder = '100 - Acknowledgement Cards' WHERE type  BETWEEN 100 AND 199
UPDATE #x SET Folder = '200 - OHOP Cards' WHERE type between 200 AND 299
UPDATE #x SET Folder = '300 - Mother''s Day Cards' WHERE type between 300 AND 399

--INSERT into #x values ('01-Honor-Not306-NotReturn-3OrUnder', 0)
--insert into #x values ('02-Honor-Not306-NotReturn-Over3', 0)
--insert into #x values ('03-Honor-Not306-Return-3OrUnder', 0)
--insert into #x values ('04-Honor-Not306-Return-Over3', 0)
--insert into #x values ('05-Honor-306-NotReturn-3OrUnder', 0)
--insert into #x values ('06-Honor-306-NotReturn-Over3', 0)
--insert into #x values ('07-Honor-306-Return-3OrUnder', 0)
--insert into #x values ('08-Honor-306-Return-Over3', 0)
--insert into #x values ('09-Memorial-Not306-NotReturn-3OrUnder', 0)
--insert into #x values ('10-Memorial-Not306-NotReturn-Over3', 0)
--insert into #x values ('11-Memorial-Not306-Return-3OrUnder', 0)
--insert into #x values ('12-Memorial-Not306-Return-Over3', 0)
--insert into #x values ('13-Memorial-306-NotReturn-3OrUnder', 0)
--insert into #x values ('14-Memorial-306-NotReturn-Over3', 0)
--insert into #x values ('15-Memorial-306-Return-3OrUnder', 0)
--insert into #x values ('16-Memorial-306-Return-Over3', 0)
select [Folder], [FileName] + case 
                    when (replace([FileName], '-Return-', '') <> [FileName]) then ' ---Label'
                    else ''
                    end as FileName,
					0 AS RowColor,
sum(NumberInFile) as NumberInFile INTO #y from #x
group BY [Folder],  [FileName] + case 
                    when (replace([FileName], '-Return-', '') <> [FileName]) then ' ---Label'
                    else ''
                    end
order BY [Folder],  [FileName] + case 
                    when (replace([FileName], '-Return-', '') <> [FileName]) then ' ---Label'
                    else ''
                    end
IF EXISTS (SELECT * FROM #y WHERE Folder = '100 - Acknowledgement Cards')
BEGIN
	UPDATE #y SET RowColor = 1 WHERE Folder = '100 - Acknowledgement Cards'
END
ELSE
BEGIN
	UPDATE #y SET RowColor = 1 WHERE Folder = '200 - OHOP Cards'
END
SELECT * FROM #y ORDER BY folder, FileName
END TRY
BEGIN CATCH
	select 'Error' as FileName, 0 as NumberInFile

END CATCH
