use SPAPPSPROD
go
drop function usfGetCardGiftsDonorStudio
go
CREATE function usfGetCardGiftsDonorStudio(@CardID int)
returns varchar(2048)
begin
	declare @Gifts varchar(2048)
	declare @Project varchar(10)
	declare @OneWord varchar(20)
	select @Gifts = ''
	select @Project = ''
	select @OneWord = ''

	DECLARE gift_cursor CURSOR FAST_FORWARD FOR
	select distinct hmc2.ProjectCode as Project, ccd.SingleWord from SPDSPROD.dbo.SP01_CardDetails hmc2, SPAPPSPROD.dbo.christmascatalogdescriptions ccd where 
	hmc2.CardRecordId = @CardID and
	hmc2.ProjectCode = ccd.project	
	OPEN gift_cursor
	WHILE (1=1)
	BEGIN
		FETCH NEXT FROM gift_cursor into @Project, @OneWord
		if (@@FETCH_STATUS <> 0) break
		if (@Gifts != '') select @Gifts = @Gifts + ';  '
		select @Gifts = @Gifts + rtrim(ltrim(@Project)) + ' ' + rtrim(ltrim(@OneWord))
	END
	CLOSE gift_cursor
	DEALLOCATE gift_cursor
	return @Gifts
end
