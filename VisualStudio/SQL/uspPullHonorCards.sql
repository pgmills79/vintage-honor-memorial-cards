use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspPullHonorCards' AND type = 'P')
    DROP PROCEDURE dbo.uspPullHonorCards
GO
Create Procedure dbo.uspPullHonorCards
	    @pg float,
		@Type int,
		@status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
select @status = 0
select @ErrorMessage = ''
exec uspPullHonorCardsToPrintQueue
exec dbo.uspHonorMemorialCombineDup	@Status output,	@ErrorMessage output
if (@status <> 0) return
exec dbo.uspMarkHonorCards @pg output, @status output, @ErrorMessage output
if (@status > 0) return
select * from SPAPPSPROD.dbo.HonorMemorialCards1 c1, SPAPPSPROD.dbo.HonorMemorialCards2 c2 where 
c1.CardDocumentNumber = c2.CardDocumentNumber and Status = 'I'
END TRY

BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspPullHonorCards, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
END CATCH