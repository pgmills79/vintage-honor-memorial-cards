-- =============================================
-- Author:		Dave Sable
-- Create date: 8/13/2007
-- Description:	Give address information for
--              honor/memorial card.
-- =============================================
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspApertaCardItems' AND type = 'P')
    DROP PROCEDURE dbo.uspApertaCardItems
GO
CREATE PROCEDURE dbo.uspApertaCardItems
				 @CardNumber float,
				 @GiftItem char(10),
				 @Count float output,
                 @ErrorText char(80) output
AS
BEGIN
SET NOCOUNT ON
declare @CardID int
declare @Message char(255)
declare @Done as int
declare @DocumentNumber as float
declare @error as int
--
--  Init
--
select @ErrorText = ''
select @CardID = @CardNumber
select @Done = 0
--
--  Check to see if transaction and account exists.
--
select @DocumentNumber = DocumentNumber from SPAPPSPROD.dbo.HonorMemorialCards1Temp where CardDocumentNumber = @CardNumber
if (@@RowCount = 0) and (@Done = 0)
BEGIN
	select @ErrorText = 'Card Information Not Submitted.'
	select @Message = 'CardInformation Not Submitted: '  + 
                      '|' + rtrim(ltrim(@GiftItem))
	exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'E', @Message
	select @Done = 1
END
--
--  Make sure product exists.
--
--
--  Set up date and time
--
if (@Done = 0)
BEGIN
	if not exists (select * from SPAPPSPROD.dbo.HonorMemorialCards2Temp where
                   CardDocumentNumber = @CardNumber and
                   Project = @GiftItem)
	BEGIN
		select @error = 0
		BEGIN TRY
			insert into SPAPPSPROD.dbo.HonorMemorialCards2Temp
			(CardDocumentNumber, Project, AddDate)
			values
			(@CardNumber, @GiftItem, getdate())
		END TRY
		BEGIN CATCH
			select @error = 1
			select @Message = 'Gift item Error - ' + convert(char(20), @@Error)
			exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'I', @Message
		END CATCH
		if @error = 0
		BEGIN
			select @Count = @Count + 1
			select @Message = 'Added gift item - ' + rtrim(ltrim(@GiftItem))
			exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'I', @Message
		END
	END
	ELSE
	BEGIN
		select @Message = 'Duplicate Gift Item - ' + rtrim(ltrim(@GiftItem))
		exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'W', @Message
	END		
END
set nocount off
END
go
