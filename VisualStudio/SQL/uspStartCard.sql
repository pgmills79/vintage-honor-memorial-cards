-- =============================================
-- Author:		Dave Sable
-- Create date: 8/13/2007
-- Description:	Give address information for
--              honor/memorial card.
-- =============================================
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspStartCard' AND type = 'P')
    DROP PROCEDURE dbo.uspStartCard
GO
CREATE PROCEDURE dbo.uspStartCard
				 @DocumentNumber float,
				 @FromName char(50),
			     @LetterCode char(10),
				 @ToName char(50),
				 @DeceasedName char(50),
                 @AddressLine1 char(70),
                 @AddressLine2 char(70),
                 @AddressLine3 char(70),
                 @City char(40),
                 @State char(10),
                 @Zip char(15),
                 @ScreenName char(30),
                 @CardNumber float output,
                 @ErrorText char(80) output
AS
BEGIN
SET NOCOUNT ON
--
-- Kept in place for Tradeweb interface.  However, I had to add the Return to Sender field so logic moved to version 2 stored procedure.
--
exec SPAPPSPROD.dbo.uspStartCardVersion2
				 @DocumentNumber,
				 @FromName,
			     @LetterCode,
				 @ToName,
				 @DeceasedName,
                 @AddressLine1,
                 @AddressLine2,
                 @AddressLine3,
                 @City,
                 @State,
                 @Zip,
			     'N',
                 @ScreenName,
                 @CardNumber output,
                 @ErrorText output
END
go
