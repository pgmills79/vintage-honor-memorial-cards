use SPAPPSPROD
go
IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspHonorMemorialCardsByCardID' AND type = 'P')
    DROP PROCEDURE dbo.uspHonorMemorialCardsByCardID
GO
Create Procedure dbo.uspHonorMemorialCardsByCardID
		@CardID bigint,
		@Status int output,
		@ErrorMessage varchar(3000) output
as
set nocount on
BEGIN TRY
declare @SQL varchar(2048)
declare @Fields varchar(2048)
select @SQL = ''
select @status = 0
select @ErrorMessage = ''
--
-- Make sure all is loaded.
--
--exec dbo.uspLoadHonorMemorialCards	@status output,	@ErrorMessage output
--if (@status <> 0) return
--
--
-- Fields
--
select @Fields = 
	'CardDocumentNumber, ' +
	'AccountNumber, ' +
	'SPAPPSDEV.[dbo].[udfFormatAccountName] (AccountNumber, ''ALL'') as AccountName, ' + 
	'isnull(DocumentNumber, 0) as DocumentNumber, ' +
	'rtrim(isnull(FromName, '''')) as FromName, ' +
	'rtrim(isnull(MemorialName, '''')) as MemorialName, ' +
	'convert(varchar(20), TransactionDate, 101) as TransactionDate, ' +
	'case when left(ltrim(LetterCode), 1) = ''H'' then ''Honor'' when left(ltrim(LetterCode), 1) = ''M'' then ''Memorial'' else LetterCode end as LetterCode , ' +
	'case when Status=''N'' then ''No Print'' when status = ''P'' then ''Printed'' when status = ''Y'' then ''Ready'' when status = ''H'' then ''Hold'' when status = ''I'' then ''Entry In Progress'' when status = ''D'' then ''Donor Ministry'' when status = ''X'' then ''Delete'' when status = ''T'' then ''Test''  else status end as status, ' +
	'PrintGroup, ' +
	'rtrim(isnull(ToName, '''')) as ToName, ' +
	'rtrim(isnull(ToAddress1, '''')) as ToAddress1, ' +
	'rtrim(isnull(ToAddress2, '''')) as ToAddress2, ' +
	'rtrim(isnull(ToAddress3, '''')) as ToAddress3, ' +
	'rtrim(isnull(ToCity, '''')) as ToCity, ' +
	'rtrim(isnull(ToState, '''')) as ToState, ' +
	'rtrim(isnull(ToZip, '''')) as ToZip, ' +
	'rtrim(isnull(ToCountry, '''')) as ToCountry, ' +
	'NumberOfGifts, ' +
	'isnull(WebPageName, '''') as WebPageName, ' +
	'convert(varchar(20), AddDate, 100) as AddDate, ' +
	'isnull(ChangeUser, '''') as ChangeUser, ' +
	'isnull(convert(varchar(20), ChangeDate, 100), '''') as ChangeDate, ' +
	'case when ReturnToSender = ''Y''  then ''Yes'' when ReturnToSender= ''N'' then ''No'' else ReturnToSender end as ReturnToSender, ' +
    'dbo.usfGetCardGifts(CardDocumentNumber) as Gifts, ' +
	''''' as NewStatus '
--
-- Main query
--
set @SQL = 'select ' + @Fields + ' from SPAPPSDEV.dbo.HonorMemorialCards1 where '
--
-- Card ID
--
if (@CardID > 0)
	set @SQL = @SQL + 'CardDocumentNumber = ' + rtrim(ltrim(convert(varchar(15), @CardID)))
--
-- Order
--
select @SQL = @SQL + ' order by TransactionDate, AccountNumber, CardDocumentNumber '
--select @SQL
exec (@SQL)
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspHonorMemorialCardsByCardID, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) + ' ' + @SQL
	
END CATCH

-- select * from SPAPPSDEV.dbo.HonorMemorialCards1 
