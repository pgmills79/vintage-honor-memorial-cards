IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspHonorCardInSyncQueue' AND type = 'P')
    DROP PROCEDURE uspHonorCardInSyncQueue
GO
Create Procedure uspHonorCardInSyncQueue
		@CardMasterRecordID bigint,
		@CardItemRecordID bigint
as
set nocount on
declare @Status int
BEGIN TRY
	select @status = 0
	INSERT INTO [SPAPPSPROD].[dbo].[HonorMemorialSyncFromDS]
           ([CardMasterRecordID]
           ,[CardItemRecordID]
           ,[QueuedDate]
           ,[AddedToViewDate]
           ,[CardID])
     VALUES
           (@CardMasterRecordID,
           @CardItemRecordID,
           getdate(),
           Null,
           0)
END TRY
BEGIN CATCH
	select @status = 1
END CATCH