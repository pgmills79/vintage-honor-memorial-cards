IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspAddHonorMemorialCard' AND type = 'P')
    DROP PROCEDURE dbo.uspAddHonorMemorialCard
GO
Create Procedure dbo.uspAddHonorMemorialCard
		@DocumentNumber bigint,
		@CardNumber bigint output,
		@Status int output,
		@ErrorMessage varchar(255) output
as
set nocount on
BEGIN TRY
	declare @AccountNumber bigint
	declare @ErrorText varchar(80)
	declare @AccountName varchar(80)
	select @Status = 0
	select @ErrorMessage = ''

	select @AccountNumber = 0
	select @AccountNumber = AccountNumber from SPDSPROD.dbo.T01_TransactionMaster where Documentnumber = @DocumentNumber
	if (@AccountNumber = 0 or @AccountNumber is null)
	BEGIN
		select @Status = 1
		select @ErrorMessage = 'Document number does not exist.'
	END		
	if (@Status = 0)
	BEGIN
		select @AccountName = SPAPPSPROD.dbo.udfFormatAccountName (@AccountNumber, 'ALL')
		exec SPAPPSPROD.dbo.uspStartCardVersion2
					 @DocumentNumber,
					 @AccountName,
					 'HCC',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'N',
					'HMPROGRAM',
					@CardNumber output,
					@ErrorText output
		if (@ErrorText <> '')
		BEGIN
			select @Status = 1
			select @ErrorMessage = @ErrorText
		END
	END
	if (@Status = 0)
	BEGIN
		exec  SPAPPSPROD.dbo.uspEndCard
					 @CardNumber,
					 0,
				     @ErrorText output
		if (@ErrorText <> '')
		BEGIN
			select @Status = 1
			select @ErrorMessage = @ErrorText
		END
		update SPAPPSPROD.dbo.HonorMemorialCards1 set status = 'I' where CardDocumentNumber = @CardNumber
	END
END TRY
BEGIN CATCH
	select @status = 1 
	select @ErrorMessage = substring('Error in  = uspCardTypeOfCard, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH
