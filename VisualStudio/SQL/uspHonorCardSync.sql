IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspHonorCardSync' AND type = 'P')
    DROP PROCEDURE uspHonorCardSync
GO
Create Procedure uspHonorCardSync
as
set nocount on
declare @Status int
declare @CardID bigint
declare @MasterrecordID bigint
declare @ItemrecordID bigint
declare @id bigint
declare @CountedCards int
BEGIN TRY
DECLARE sync_cursor CURSOR FAST_FORWARD FOR
select [id], CardMasterRecordID, CardItemRecordID from SPAPPSPROD.dbo.HonorMemorialSyncFromDS
where cardid = 0
order by CardMasterRecordID, CardItemRecordID
OPEN sync_cursor
WHILE (1 = 1) 
BEGIN
	FETCH NEXT FROM sync_cursor into @ID, @MasterrecordID, @ItemrecordID
	if (@@FETCH_STATUS != 0) break
	print convert(varchar(20), @MasterRecordID) + ' ' + convert(varchar(20), @ItemRecordID)
	--
	-- Get the card ID if it exists
	--
	select @CardID = 0
	if exists (select CardID from SPAPPSPROD.dbo.HonorMemorialSyncFromDS where CardMasterRecordID = @MasterrecordID and CardItemRecordID = 0 and CardID > 0)
	BEGIN
		select @CardID = min(CardID) from SPAPPSPROD.dbo.HonorMemorialSyncFromDS where CardMasterRecordID = @MasterrecordID and CardItemRecordID = 0 and CardID > 0
		if (@CardID is null) select @CardID = 0
	END
	--
	-- if we are doing master record and it already exist.
	--
	if ((@ItemRecordID = 0) and (@CardID > 0))
	BEGIN
		UPDATE [SPAPPSPROD].[dbo].[HonorMemorialCards1]
		   SET [DocumentNumber] = coalesce(SP02.DocumentNumber, 0),
			  [FromName] = left(coalesce(SP02.FromName, ''), 50),
			  [MemorialName] =  left(coalesce(SP02.Memorial, ''), 50),
			  [LetterCode] = case when rtrim(coalesce(SP02.Memorial, '')) = '' then 'HCC' else 'MCC' end,
			  [ToName] = left(coalesce(SP02.ToName, ''), 50),
			  [ToAddress1] = left(coalesce(SP02.Address1, ''), 70),
			  [ToAddress2] = left(coalesce(SP02.Address2, ''), 70),
			  [ToAddress3] = left(coalesce(SP02.Address3, ''), 70),
			  [ToCity] = left(coalesce(SP02.City, ''), 40),
			  [ToState] = left(coalesce(SP02.[State], ''), 10),
			  [ToZip] = left(coalesce(SP02.ZipPostal, ''), 15),
			  [ToCountry] = left(coalesce(SP02.Country, ''), 10),
			  [ChangeUser] =  'DSSCREEN',
			  [ChangeDate] = getdate(),
			  [ReturnToSender] = case when coalesce(SP02.ReturnToSender, 0) = 1 then 'Y' else 'N' END
		 FROM
		 SPDSPROD.dbo.SP02_CardMaster SP02, [SPAPPSPROD].[dbo].[HonorMemorialCards1] HM1, [SPAPPSPROD].dbo.HonorMemorialSyncFromDS SYNC
		 WHERE SP02.recordid = SYNC.CardMasterRecordID and
		       SP02.recordid = @MasterrecordID and
		       HM1.CardDocumentNumber = SYNC.CardID and
		       HM1.CardDocumentNumber = @CardID and
		       CardItemRecordID = @ItemRecordID and
		       HM1.[Status] <> 'P'
	END
	--
	-- if we are doing master record and it does not already exist.
	--
	if ((@ItemRecordID = 0) and (@CardID = 0))
	BEGIN
		exec  uspGetNextHonorCardNumber @CardID OUTPUT
		INSERT INTO [SPAPPSPROD].[dbo].[HonorMemorialCards1]
           ([CardDocumentNumber]
           ,[AccountNumber]
           ,[DocumentNumber]
           ,[FromName]
           ,[MemorialName]
           ,[TransactionDate]
           ,[LetterCode]
           ,[Status]
           ,[Occurrance]
           ,[PrintGroup]
           ,[ToName]
           ,[ToAddress1]
           ,[ToAddress2]
           ,[ToAddress3]
           ,[ToCity]
           ,[ToState]
           ,[ToZip]
           ,[ToCountry]
           ,[NumberOfGifts]
           ,[WebPageName]
           ,[AddDate]
           ,[ChangeUser]
           ,[ChangeDate]
           ,[ReturnToSender])
     Select
           @CardID,
           coalesce(AccountNumber, 0),
           coalesce(DocumentNumber, 0),
           left(coalesce(FromName, ''), 50),
           left(coalesce(Memorial, ''), 50),
           coalesce((select [date] from SPDSPROD.dbo.T01_TransactionMaster T01 where T01.DocumentNumber = SP02.DocumentNumber and SP02.DocumentNumber is not null), getdate()) ,
           case when rtrim(coalesce(Memorial, '')) = '' then 'HCC' else 'MCC' end,
           '?',
           ((@CardID % 5) + 1),
           0,
           left(coalesce(ToName, ''), 50),
           left(coalesce(Address1, ''), 70),
           left(coalesce(Address2, ''), 70),
           left(coalesce(Address3, ''), 70),
           left(coalesce(City, ''), 40),
           left(coalesce([State], ''), 10),
           left(coalesce(ZipPostal, ''), 15),
           left(coalesce(Country, ''), 10),
           0,
           'DSSCREEN',
           getdate(),
           '',
           Null,
           case when coalesce(ReturnToSender, 0) = 1 then 'Y' else 'N' END
           from SPDSPROD.dbo.SP02_CardMaster SP02 where recordid = @MasterrecordID
	END
	--
	-- if we are doing item record and header record exists
	--
	if ((@ItemRecordID > 0) and (@CardID > 0))
	BEGIN
		declare @dt datetime
		select @dt = getdate()
		INSERT INTO [SPAPPSPROD].[dbo].[HonorMemorialCards2]
           ([CardDocumentNumber]
           ,[Project]
           ,[AddDate])
		   select distinct @CardID, coalesce(ProjectCode, ''), @dt from SPDSPROD.dbo.SP01_CardDetails SP01 
		   where cardrecordid = @MasterrecordID and 
		   coalesce(ProjectCode, '') not in
		   (select Project from [SPAPPSPROD].[dbo].[HonorMemorialCards2] HM2 where 
		    HM2.Project = coalesce(SP01.ProjectCode, '') and HM2.CardDocumentNumber = @CardID)
		 --
		 delete [SPAPPSPROD].[dbo].[HonorMemorialCards2]
		 from [SPAPPSPROD].[dbo].[HonorMemorialCards2] HM2 where
		 CardDocumentNumber = @CardID and
		 Project not in
		 (select ProjectCode from SPDSPROD.dbo.SP01_CardDetails SP01 where
		  HM2.Project = coalesce(SP01.ProjectCode, '') and
		  SP01.cardrecordid = @MasterrecordID)
	END
	--
	-- if we are doing item record and header record does not exists
	--
	if ((@ItemRecordID > 0) and (@CardID = 0))
	BEGIN
		print 'Skip gifts - no header'
	END
	--
	-- Update the appropriate status if it is not yet printed.
	--
	if (@CardID > 0)
	BEGIN
		-- Card ID to sync table
		update dbo.HonorMemorialSyncFromDS set Cardid = @CardID where id = @id
		-- Find the account
		update SPAPPSPROD.dbo.HonorMemorialCards1 set AccountNumber = T01.AccountNumber
		from SPAPPSPROD.dbo.HonorMemorialCards1 HM1, SPDSPROD.dbo.T01_TransactionMaster T01 where
		T01.DocumentNumber = HM1.DocumentNumber and
		HM1.CardDocumentNumber = @CardID
		--
		-- Figure count
		--
		declare @CardStatus char(1)
		select @CardStatus = 'Y'
		select @CountedCards = count(*) from SPAPPSPROD.dbo.HonorMemorialCards2 where CardDocumentNumber = @CardID
		update SPAPPSPROD.dbo.HonorMemorialCards1 set NumberOfGifts = @CountedCards where CardDocumentNumber = @CardID
		-- Can't fit on card.  Donor Ministries to the rescue.
		if (@CountedCards > 8)  set @CardStatus = 'D' 
		--  
		if exists (select * from SPAPPSPROD.dbo.HonorMemorialCards1 where
		           CardDocumentNumber = @CardID and
		           (AccountNumber= 0 or
		            DocumentNumber = 0 or
		            rtrim(FromName) = '' or
		            rtrim(LetterCode) = '' or
		            rtrim(ToName) = '' or
		            rtrim(ToAddress1) = '' or
		            rtrim(ToCity) = '' or
		            rtrim(ToState) = '' or
		            rtrim(ToZip) = '' or
		            NumberOfGifts <= 0)) set @CardStatus = 'H'
		if exists (select * from SPAPPSPROD.dbo.HonorMemorialCards2	HM2 where project not in
					(select ProjectCode from SPDSPROD.dbo.Y01_ProjectMaster Y01 where Y01.ProjectCode = HM2.Project) and
					CardDocumentNumber = @CardID) set @CardStatus = 'H'
		update SPAPPSPROD.dbo.HonorMemorialCards1 set [status] = @CardStatus where
		status in ('Y', 'D', '?', 'H') and CardDocumentNumber = @CardID
		update SPAPPSPROD.dbo.HonorMemorialCards1 set [status] = @CardStatus where
		status is null and CardDocumentNumber = @CardID             
	END
END
CLOSE sync_cursor
DEALLOCATE sync_cursor
END TRY
BEGIN CATCH
	IF (SELECT CURSOR_STATUS('local','sync_cursor')) >=0 
	BEGIN
		DEALLOCATE sync_cursor
	END
	declare @ErrorMessage varchar(255)
	select @ErrorMessage = substring('Error in  = uspHonorCardSync, procedure line ' + 
												   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
												   ERROR_MESSAGE(), 1, 255)
	print @ErrorMessage
	exec SPAPPSPROD.dbo.uspLogCard	 @CardID, 'D', @ErrorMessage
END CATCH

select * from SPDSPROD.dbo.SP02_CardMaster where
len(memorial) > 50