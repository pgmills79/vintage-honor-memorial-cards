IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspMarkHonorCardAsDeleted' AND type = 'P')
    DROP PROCEDURE dbo.uspMarkHonorCardAsDeleted
GO
Create Procedure dbo.uspMarkHonorCardAsDeleted
as
set nocount on
BEGIN TRY
update SPAPPSPROD.dbo.HonorMemorialCards1 set status = 'Z' where status = 'X'
END TRY
BEGIN CATCH
	select  substring('Error in  = uspMarkHonorCardAsDeleted, procedure line ' + 
                                   rtrim(convert(char(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	
END CATCH
 
